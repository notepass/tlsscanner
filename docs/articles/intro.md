---
uid: tls-scanner-net-docs-articles-intro
authors: Florian Hiensch
creationDate: 28.12.2019
---

# Projektübersicht

Innerhalb dieses Projektes wurde eine Anwendung implementiert, welche die technische Richtlinie "TR-02102-2" des BSI prüft.

Die Umsetzung wurde in Teilen zunächst in den Programmiersprachen C++ sowie C# und schließlich Java umgesetzt. Die finale Implementierung erfolgte in Java. Die Hintergründe dieser Entscheidung werden im weiteren Verlauf des Dokuments noch näher erläutert.

<div class="mermaid">
graph TD
CC[CliClient]
TS[TlsScanner]
SC[TlsScannerEventCallback]
SR[TlsScannerReport]
PC[ProtocolCommunicator]
SF[SocketFactory]
CC-->TS
TS-.->SF
TS-.->PC
TS-->SC
TS-->SR
TS --> T{Tests}
T -->|1/4| D[fa:fa-tasks Protocol Checks]
T -->|2/4| E[fa:fa-lock TLS-Version Checks]
T -->|3/4| F[fa:fa-key Cipher Checks]
T -->|4/4| G[fa:fa-warning Attack Vulnerabilities]
</div>

## Auswahl der Programmiersprache

In diesem Kapitel werden die in Betracht gezogenen Programmiersprachen kurz beschrieben. Zudem werden die in den
Programmiersprachen gemachten Aufwände zur Umsetzung beschrieben.

### C++ und LibreSSL

Ein erster Versuch der Umsetzung fand mit der empfohlenen Kombination von C++ und LibreSSL statt.
Im Projektteam waren aber leider nur wenige Vorkenntnisse zu C++ und den zugehörigen Build- und Integrationsprozessen vorhanden, weshalb hier viel Zeit aufgewendet wurde, um LibreSSL unter Linux und Windows zu kompilieren um es nutzen zu können.

Nach dem zunächst erfolgreichen Kompilieren der Bibliothek entstanden im Anschluss aber große Schwierigkeiten das kompilierte Paket innerhalb einer neuen C++ Anwendung einzubinden, bzw. von dieser aus zur Laufzeit auf die Bibliothek zuzugreifen.
Da nicht mehr die ausreichende Zeit bestand, sich tiefergehend sowohl in die Programmiersprache als auch die LibreSSL-Bibliothek einzulesen, wurde der Versuch C++ zu nutzen abgebrochen.

### C#

Aufgrund dieser Erfahrung erfolgte ein zweiter Versuch der Umsetzung mit C#.
Da aber zunächst nicht sicher gesagt werden konnte, ob mit  den Bordmitteln des .NET Framework alle funktionalen Anforderungen der Anwendung umgesetzt werden konnten, wurde die Erstellung eines funktionalen Prototypen beschlossen, bei dem Einfachheit und Funktionalität im Vordergrund stehen sollten.

Um die Anwendung plattformübergreifend betreiben zu können, wurde zunächst .NET Core 2.2 als Zielframework gewählt und bereits viele Funktionen damit umgesetzt.
Leider enthielt diese Version von .NET Core aber zwei fehlende Funktionen, die eine relativ hohe Hürde darstellten:

- TLS 1.3 ist unter .NET Core 2.2 noch nicht verfügbar
- Beim Herstellen einer Verbindung über die integrierte SSL-Stream Klasse können keine Verbindungsparameter, wie beispielsweise die zu verwendenden Ciphers angegeben werden

Beide Funktionen wurden aber in .NET Core 3 ergänzt, weshalb das Projekt auf .NET Core 3.1 migriert wurde. Damit wurde dann auch der Test für TLS 1.3 und die Möglichkeit Ciphers zu testen integriert.

### Besonderheiten bei Verwendung von C#

Als eine Besonderheit der C# Implementierung kann genannt werden, dass die SSL/TLS Funktionalität auf den verschiedenen Plattformen unterschiedlich realisiert wird.

So wird unter Windows der in das Betriebssystem integrierte <abbr title="Security Support Provider Interface">Schannel</abbr> Provider genutzt. Unter Linux wird hingegen auf OpenSSL als Bibliothek zurückgegriffen, um die TLS und Crypto-Funktionen zu implementieren.

### Java

Während der Entwicklung des funktionalen Prototyps in C# wurde festgestellt, dass die aufgrund der unzureichenden Designphase entstandene Programmstruktur zu Problemen bei der Erweiterbarkeit führen könnte. Daher sollte ein <abbr title="Proof of Concept">POC</abbr> für die Restrukturierung der Anwendung erstellt werden.
Diese wurde zunächst auf Papier erarbeitet und später, zum Prüfen der Machbarkeit, in Java implementiert.

Nach kurzer Zeit befanden sich die C#- und Java-Version des Scanners auf derselben Ausbaustufe und der Fokus wurde, aufgrund der besseren Codestrukturierung, auf die Java-Version gelegt.

<!-- ### Entscheidung für Java
Die Java-Version der Anwendung war zunächst als POC zur strukturellen Verbesserung des bestehenden Codes gedacht.
Da diese sich jedoch schneller weiterentwickelte als die C#-Version, wurde der weitere Fokus auf Java gelegt.

Ein Vorteil von Java ist, dass die TLS-Kommunikation der Standardlibrary in Java implementiert wurde. Damit ist
die Anwendung unabhängig von installierten TLS/SSL-Bibliotheken auf dem Hostsystem. Zudem ist, zumindest theoretisch,
ein tieferer Zugriff auf die TLS Funktionalitäten möglich, da keine Aufrufe an Systembibliotheken delegiert werden. -->

### Besonderheiten bei Verwendung von Java

Im Gegensatz zu C# nutzt Java zur TLS-Kommunikation keine Systembibliotheken, sondern eine im <abbr title="Java Development Kit">JDK</abbr> enthaltene Implementierung.
Dadurch ist die Anwendung unabhängig von den installierten Systembibliotheken. Zudem ist, zumindest theoretisch,
ein tieferer Zugriff auf die TLS Funktionalitäten möglich, da keine Aufrufe an externe Bibliotheken delegiert werden.

# Implementierung
In diesem Kapitel wird das TLS-Protokoll, dessen Implementierung in Java und der Aufbau von [!include[prod](../prodlong.md)] beschrieben.

## Ablauf eines TLS-Handshakes

Um die Implementierung verstehen zu können, ist etwas Wissen über den Ablauf eines TLS-Handshakes nötig. Dieser ist wie folgt aufgebaut (vereinfachte Version):

<div class="mermaid">
sequenceDiagram
participant Server
participant Client
Client->>Server: ClientHello
Server->>Client: ServerHello
Server->>Client: Certificate
Server->>Client: ServerHelloDone
Client->>Server: ClientKeyExchange
Client->>Server: ChangeCipherSpec
Client->>Server: Finished
Server->>Client: ChangeCipherSpec
Server->>Client: Finished
</div>

Die Schritte werden nachfolgend erklärt:

- ClientHello: Initialisiert die Kommunikation. Der Client sendet die zu nutzende TLS-Version, eine Liste unterstützter Cipher und
eine Liste unterstützter TLS-Erweiterungen an den Server
- ServerHello: Der Server antwortet und sendet die zu nutzende TLS-Version, den zu nutzenden Cipher (aus der ClientHello-Liste ausgewählt) und die unterstützen TLS Erweiterungen (wiederum nur die aus der ClientHello-Nachricht) an den Client
- Certificate: Der Server sendet sein Zertifikat zur Validierung an den Client
- ServerHelloDone: Signal des Abschlusses der Server-Nachrichten, der Server wartet nun auf den Client
- ClientKeyExchange: Der Client sendet das Pre-Master Secret
- ChangeCipherSpec: Die erste verschlüsselte Nachricht - Mit dieser zeigt der Client, dass die Kommunikation ab jetzt nur
noch verschlüsselt erfolgt
- Finished: Client teilt mit, dass der Handshake von seiner Seite aus abgeschlossen ist
- ChangeCipherSpec: Server zeigt Client, dass auch dieser ab jetzt nur noch verschlüsselt kommuniziert
- Finished: Server teilt mit, dass der Handshake von seiner Seite aus abgeschlossen ist

[[Quelle 1](https://www.kryptowissen.de/transport-layer-security-tls.php)],
[[Quelle 2](https://www.cloudflare.com/learning/ssl/what-happens-in-a-tls-handshake/)]

Die meisten dieser Nachrichten sind für die Implementierung des TLS-Scanners nicht von Interesse. Lediglich die ClientHello
und ServerHello Nachrichten wurden implementiert. Entsprechend wurden auch einige Erweiterung zum Versand mit der ClientHello
Nachricht erstellt. Die weitere Dokumentation wird größtenteils nur auf diese Nachrichten, beziehungsweise deren Bestandteile
(Erweiterung, TLS_Versionen, Cipher-Sets, ...) eingehen.

## Implementierung von TLS-Verbindungen / Aufbau des TLS-Protokolls

Die TLS-Verbindungen wurden auf 2 Arten realisiert:

- Verbindung über die mit dem JDK gelieferte TLS-Bibliothek
- Kommunikation über eigens implementierte TLS Pakete (Client- und Server-Hello Pakete, keine Verschlüsselung o.ä.)
Die Standard-Bibliothek wird zum Prüfen der vom Host unterstützen Cipher-Suiten genutzt. Dadurch soll sichergestellt werden,
dass ein vollständiger Handshake, sowie eine verschlüsselte Verbindung möglich sind (durch senden eines HTTP-Headers).

Die Prüfung auf Verwundbarkeiten erfolgt mittels der eigens implementierten TLS-Nachrichten. Zudem kann die Unterstützung
von Cipher-Suiten über diese Nachrichten erfolgen, falls ein zu testender Cipher nicht vom Client unterstützt wird. Da
hier jedoch keine verschlüsselten Daten versandt werden, ist eine komplette Prüfung nicht möglich. Entsprechend muss dieser
Modus durch den Endanwender aktiv zugeschaltet werden.

Aus Zeitgründen wurden nur die Hello-Nachrichten für TLS v1.2 und TLS v1.3 realisiert. Diese wurden auf den entsprechenden
RFCs aufgebaut ([RFC-5246](https://tools.ietf.org/html/rfc5246) für TLS1.2 und [RFC-8446](https://tools.ietf.org/html/rfc8446) für TLS1.3).

> [!NOTE]
> Sofern möglich, werden für die Beschreibung des Aufbaus von TLS-Paketen und Erweiterungen die in den RFCs spezifizierten Feldnamen verwendet.

### Implementierung der TLS-Erweiterungen

Da die eigens implementierten TLS-Nachrichten zum Prüfen von Angriffsvektoren und Erweiterungen genutzt werden, mussten
auch die TLS-Erweiterungen implementiert werden. Dabei mussten zur Prüfung der "Technische(n) Richtlinie TR-02102-2" insgesamt 4 Erweiterungen implementiert werden:

- `encrypt_then_mac` ([Bestandteil von RFC-7366](https://tools.ietf.org/html/rfc7366#section-2))
- `extended_master_secret` ([RFC-7627](https://tools.ietf.org/html/rfc7627))
- `heartbeat` ([RFC-6520](https://tools.ietf.org/html/rfc6520))
- `truncated_hmac` ([RFC-6066](https://tools.ietf.org/html/rfc6066))

<br/>
> [!IMPORTANT]
> Es ist zu beachten, dass es sich bei der Umsetzung der Erweiterungen nicht um server- sonder clientseitige Implementierungen handelt, welche lediglich der Generierung einer Anfrage dienen ("Client fragt Server: *Wird diese Erweiterung unterstützt?*").

Für die generische Kommunikation über TLS mussten zudem noch 2 weitere Erweiterungen implementiert werden:

- `server_name` ([RFC-6066](https://tools.ietf.org/html/rfc6066))
- `supported_versions` ([RFC-8446](https://tools.ietf.org/html/rfc8446))

Zudem wurde noch die Heartbeat-Message implementiert ([Bestandteil von RFC-6520](https://tools.ietf.org/html/rfc6520#page-5)).

Diese funktionierte allerdings bis zum Abschluss der Arbeit nicht korrekt und werden daher nicht verwendet und als "Deprecated" markiert.

> [!NOTE]
> Eine Liste aller verfügbaren TLS-Erweiterungen kann der [IANA Webseite](https://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xhtml) entnommen werden.

### Aufbau einer TLS-Nachricht

In diesem Kapitel wird der Aufbau von TLS-Nachrichten näher erläutert. Zudem wird etwas auf die Java-Implementierung
eingegangen. Hierbei werden jedoch nicht alle Implementierungs-Details genannt, diese tiefergehenden Details können
der angehängten [API-Dokumentation](/api/), oder der Implementierung ([Gitlab-Link](https://gitlab.com/notepass/tlsscanner)) entnommen werden.

#### Generischer Aufbau

TLS-Nachrichten werden als Binärdaten über das Netzwerk übertragen. Das heißt, dass eine Darstellung als z.&nbsp;B. XML
oder JSON-Datei nicht ohne weitere Umwandlungen möglich ist. Auch das Mapping auf eine Objektstruktur benötigt zusätzliche
Logik.

Eine TLS Nachricht beginnt immer mit dem sogenannten **"Record Layer"**. Dieses hat eine Länge von 5 Bytes und enthält
folgende Informationen:

- `ContentType` (**1 byte**): Typ der Nachricht (*z.&nbsp;B. 22 für eine Handshake (=HELLO) Message*)b
- `ProtocolVersion` (**2 bytes**): Version des TLS-Protokolls - dieser Teil der Nachricht wird ab TLS 1.3 ignoriert (Nähere Informationen dazu im Kapitel zu [TLS 1.3](#tls-13))
- `length` (**2 bytes**): Gesamtlänge der verbleibenden Nachricht in Bytes (Ohne die Daten des Record-Layers)

<!--
Durch diese Informationen kann die Nachricht korrekt verarbeitet werden, da der Empfänger nun weiß, wie die Nachricht
zu parsen ist (=Typ der Nachricht) und wie viele Daten zu lesen sind (=Gesamtlänge der Verbleibenden Nachricht) -->

#### TLS 1.2

##### Client-HELLO

Eine Client-Hello-Nachricht ist die TLS-Nachricht welche der Client an einen Server sendet, um eine TLS-Verbindung zu initialisieren.
Diese enthält alle "Fähigkeiten" des Clients (TLS-Versionen, Cipher-Suiten, Erweiterungen, ...).

Eine TLS 1.2-Nachricht kann daran erkannt werden, dass das TLS-Version-Feld im Record-Layer den Wert `0x0303` enthält,
wobei der erste Teil `"03"` für die **Major-Version** (In diesem Fall 3, da TLS1 Version 1 und TLS2 Version 2 sind) und der zweite `"03"` Teil für die **Minor-Version** (01 = TLS 1.0, 02 = TLS 1.1, ...) steht.
[[Quelle](https://tools.ietf.org/html/rfc5246#section-6.2.1)]

Nach dem Record-Layer beinhaltet die Nachricht folgende Felder:

- `msg_type` (**1 byte**): Gibt die Art des Handshakes an, da sich Server- und Client-HELLO-Nachrichten die Basisstruktur teilen.
Der Wert 1 steht für ein Client-HELLO (Anfrage des Clients an den Server), der Wert 2 für ein Server-HELLO (Antwort des Servers auf die Client-Anfrage).
- `length` (**3 bytes**): Verbleibende Länge des Pakets nach diesem Feld (in Bytes)
- `client_version` (**2 bytes**): Die TLS-Version des Clients (*z.&nbsp;B. 0x0303 für TLS 1.2*)
- `gmt_unix_time` (**4 bytes**): Aktueller Zeitstempel des Clients (als Unix-timestmap, also Sekunden seit dem 01.01.1970). Wird oftmals durch Zufallsdaten substituiert
- `random_bytes` (**28 bytes**): 28 Bytes an Zufallsdaten, welche durch einen **sicheren** Zufallsdatengenerator zu generieren sind
- `session_id_lenght` (**1 byte**): Länge der TLS Session ID (in Bytes). Diese wird vom Server genutzt, um den Client zu identifizieren, sodass er weiß mit welchen Parametern (Cipher, Zufallszahlen, ...) er die Verbindung aufzubauen hat, falls es bereits eine Verbindung zwischen dem Client und Server gab
- `session_id` (**dynamische Länge**): Siehe vorheriger Punkt
- `cipher_suites_lenght` (**2 bytes**): Länge (in Bytes) des Cipher-Suites-"Arrays", welche der Client unterstützt
- `cipher_suites` (**dynamische Länge**): Eine Liste an 2-byte großen Einträgen, welche jeweils eine unterstütze Cipher-Suite
wiederspiegeln. Ein Mapping von IANA-Namen auf diese Werte kann z.&nbsp;B. der [Bouncycastle-Dokumentation](https://www.bouncycastle.org/docs/tlsdocs1.5on/constant-values.html#org.bouncycastle.tls.CipherSuite.DRAFT_TLS_DHE_PSK_WITH_AES_128_OCB) entnommen werden.
- `compression_methods_lenght` (**2 bytes**): Feldlänge für das Feld "Supported compression methods" in Bytes
- `compression_methods` (**dynamische Länge**): Eine Liste an 1-byte großen Einträgen, welche die vom Client unterstützten Komprimierungsmethoden darstellen. Derzeit sind hier nur die Werte `0` für `keine Komprimierung` und `1` für `DEFLATE Komprimierung` möglich
- `extensions_lenght` (**2 bytes**): Gibt die Länge des Feldes "extensions" in Byte an
- `extensions` (**dynamische Länge**): Enthält Informationen zu dem vom Client unterstützden TLS-Erweiterungen. Jede Erweiterung kann dabei mit einer anderen Anzahl an Bytes dargestellt werden. Daher ist hier keine Aussage der Länge pro Eintrag möglich. Weitere Informationen zum Aufbau und Parsing der Verschiedenen Erweiterungen sind im Kapitel "TLS-Erweiterungen (TODO)" zu finden.

##### Server-HELLO

Eine Server-HELLO-Nachricht ist die Nachricht, mittels welcher der Server auf einen Client-Hello antwortet. Dabei wird nur eine Server-Hello Nachricht versandt, wenn der Server mit den Fähigkeiten des Clients eine Verbindung aufbauen kann. Ansonsten wird eine Alert-Message versandt und der Verbindungsaufbau schlägt fehl.

Die Datenstruktur der Nachricht ist gleich mit der eines Client-HELLO. Allerdings ist für die Felder "Cipher-Suites" und "Supported compression methods" nur jeweils ein Eintrag erlaubt. Diese Einträge geben dann an, welche Cipher-Suite und
Kompression bei der Verbindung vom Client genutzt werden sollen. In diesen Feldern dürfen nur Werte vorkommen, welche der
Client zuvor in seinen Fähigkeiten spezifiziert hat.

Die Liste an TLS-Erweiterungen enthält alle Erweiterungen, welche der Server unterstützt und vom Client angefragt wurden.
Hierdurch signalisiert der Server dem Client welche Erweiterungen zu nutzen sind.

#### TLS 1.3

Eine TLS 1.3-Nachricht stimmt grundlegend mit einer TLS 1.2 Nachricht überein, mit Ausnahme der folgenden beiden Änderungen:

- Im Record-Layer wird die TLS-Version immer fest auf 1.2 (`0x0303`) gesetzt
- Es muss die TLS-Erweiterung `SupportedVersions` verwendet werden, in welcher TLS 1.3 als Option angegeben wird (zum erzwingen
einer TLS 1.3 Verbindung darf dabei nur TLS 1.3 als unterstützte Version versandt werden)

> [!NOTE]
> Erhält ein Server einen TLS 1.3 Request, so wird die TLS-Version im Record-Layer ignoriert und die in der "SupportedVersions"-Erweiterung spezifizierte Version genutzt.

Diese Änderung wurde durchgeführt, da viele Proxy-Server (*z.&nbsp;B. in Unternehmen*) Verbindungen ablehnen, wenn diese die dort spezifizierte TLS-Version nicht kennen.
Daher wurde das TLS-Version Feld im Record-Layer deprecated und die Aushandlung des TLS-Protokolls erfolgt über eine Erweiterung.

Dies sichert auch die Kompatibilität zu alten Systemen, da es sich hierbei nicht um einen "breaking change" im Protokoll handelt und auch nicht TLS 1.3-fähige Server einfach mit einem TLS 1.2 Server HELLO Antworten würden, auf welchen hin der Client die Verbindung abbrechen kann (falls TLS 1.3 forciert wird).

### Aufbau der TLS-Erweiterungen

Wie die HELLO-Nachrichten sind TLS-Erweiterungen auch in einem Binärformat aufgebaut. Hierbei sind die ersten
4 Bytes eines jeden TLS-Extension-Eintrags gleich:

- `ExtensionType` (**2 bytes**): Typ der Erweiterung. Eine Liste aller verfügbaren TLS-Erweiterungen und deren ExtensionType-Wert (Spalte "value" der IANA-Tabelle) kann der [IANA Webseite](https://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xhtml) entnommen werden
- `extension_data_lenght` (**2 bytes**): Länge des Eintrags in Bytes ohne diese Headerinformationen (wie viele weitere Bytes zum Parsen der Erweiterungsinformation gelesen werden müssen)

Alle nachfolgenden Bytes sind erweiterungsspezifisch zu betrachten und werden für alle implementierten Erweiterungen
in den nachfolgenden Kapiteln aufgezeigt.

#### supported_versions Erweiterung

Mittels der `supported_versions`-Erweiterung gibt ein Client an, welche TLS-Versionen dieser unterstützt. Ab
TLS v1.3 muss diese Erweiterung mitgesandt werden (weitere Informationen hierzu: Siehe Kapitel [TLS 1.3](#tls-13)). Die Aushandlung von älteren TLS-Versionen über diese Erweiterung ist theoretisch jedoch auch möglich.

Die Erweiterung ist wie folgt aufgebaut:

- `versions` (**dynamische Länge**): Ein Array bestehend aus 2-byte großen Einträgen, welche jeweils eine TLS-Version darstellen (*z.&nbsp;B.: 0x0303 für TLS v1.2 oder 0x0304 für TLS v1.3*)

#### truncated_hmac Erweiterung

Diese Erweiterung gibt an, dass ein Client gerne mit einer verkürzten HMAC-Komponente arbeiten möchte.
Dieser "Modus" war für Netzwerke mit geringer Durchsatzrate gedacht. Da die HMAC-Komponente jedoch der Integritäts- und Authentizitätsprüfung der Nachricht dient, wird durch eine Verkürzung dieser Komponente ein Angriff erleichtert.

Während ein Angriff auf Basis einer verkürzten HMAC-Komponente nicht direkt eine Entschlüsselung der Daten zur Folge hat, wird es für den Angreifer jedoch theoretisch einfacher gefälschte Pakete an den Client zu senden.
Dies basiert auf der Annahme, dass ein Angriff auf die HMAC-Komponente nur auf Basis einer Brute-Force-Attacke möglich ist (entsprechend gilt: mehr Daten = höherer Aufwand) [[Quelle](https://security.stackexchange.com/questions/93445/determining-strength-of-truncated-hmac)].

Die Erweiterung beinhaltet keine spezifischen Daten. Die Präsenz der 2 Bytes für extension_type (hier 4) und einer Länge von 0 ist ausreichend.

#### extended_master_secret Erweiterung

<!-- https://www.tripwire.com/state-of-security/security-data-protection/security-hardening/tls-extended-master-secret-extension-fixing-a-hole-in-tls/ -->
Die `extended_master_secret`-Erweiterung wurde entwickelt, um der Verwundbarkeit [3SHAKE](https://www.mitls.org/pages/attacks/3SHAKE) entgegenzuwirken. Bei 3SHAKE kann ein Server als <abbr title="Man-in-the-middle">MITM</abbr> den Traffic einer Verbindung mitlesen.
Diese Attacke wurde ähnlich schon 2009 bekannt, als eine logische Lücke im Renegotiation-Protokoll von TLS entdeckt wurde, die es einem Angreifer erlaubte, Pakete als authentifizierter User an den Server zu senden, während der "normale" Client die Verbindung selbst weiterhin nutzen konnte.
Auf Seiten des Servers sah die Verbindung wie von einer Person kommend aus. Dieser Fehler wurde behoben, indem ein Renegotiation-Handshake Informationen des initialen Handshakes enthalten muss.

Mittels 3SHAKE kann diese Absicherung umgangen werden. Hierzu muss sich ein Client mit einem böswilligen Server verbinden. Dieser kann sich dann wiederrum mit einem weiteren Server verbinden und die Nutzung des selben Master Secret zwischen den Verbindungen erzwingen.
Dies reicht jedoch nicht aus, um die 2009 entdeckte Attacke wiederum so durchzuführen, da die Renegotiation-Pakete mit Daten versehen sind, welche für beide Verbindungen (Client <-> Böswilliger Server und Böswilliger Server <-> Zielserver) unterschiedlich sind.
Um dies zu umgehen wird ein "Session Resumption" Handshake genutzt. Mittels diesem ist es möglich, eine zuvor aufgebaute Verbindung wiederherzustellen. Dabei werden nur die Werte für die Verschlüsselung erneuert, jedoch werden keine Zertifikate oder öffentliche Schlüssel ausgetauscht.
Durch diesen Trick sind die zusätzlichen Daten, welche die "alte" Attacke verhindern sollen nun für beide Verbindungen gleich. Damit kann die damals eingebaute Sicherung umgangen werden und der Exploit erneut genutzt werden.

Durch die Erweiterung "Extended Master Secret" werden nicht mehr nur einige Werte zur Absicherung des Renegotiation Handshakes verwendet (bisher wurde nur die Finish-Message verwendet), sondern Werte aus allen Handshake-Nachrichten.

Der Name "3SHAKE" kommt durch die 3 getätigten Handshakes zustande:
- Attacker mit Ziel-Server
- Client mit Attacker
- Renegotiation-Handshake zwischen Client und Attacker, sowie Attacker und Ziel-Server

[[Quelle 1](https://blog.cryptographyengineering.com/2014/04/24/attack-of-week-triple-handshakes-3shake/)] [[Quelle 2](https://www.mitls.org/pages/attacks/3SHAKE)] [[Quelle 3](https://www.tripwire.com/state-of-security/security-data-protection/security-hardening/tls-extended-master-secret-extension-fixing-a-hole-in-tls/)]

Auch diese Erweiterung enthält keine spezifischen Daten. Daher reicht die Angabe der generischen Daten (mit `ExtensionType` `0x17`)
um die Erweiterung korrekt zu definieren.

#### encrypt_then_mac Erweiterung

Bei einer normalen TLS-Verbindung wird zunächst der MAC (Message Authentication Code) berechnet und an die
Klartext-Nachricht angehängt. Danach erfolgt die Verschlüsselung.

Falls die `encrypt_then_mac` Erweiterung definiert ist, wird diese Reihenfolge jedoch wie folgt abgeändert: Zuerst wird der Inhalt
der Nachricht verschlüsselt. Danach wird der MAC berechnet und an die verschlüsselte Portion angehängt.

Dieses Vorgehen bietet einige Vorteile. Folgend die wichtigsten:

- Der MAC kann bereits vor der Entschlüsselung des Paketes geprüft werden. Dies spart Zeit bei der Analyse, ob ein Paket gefälscht
wurde beziehungsweise ungültig ist
- Da die MAC-Komponente der verschlüsselten Komponente angehangen wird, kann diese die Musterfindung schwieriger machen. Dies gilt
eher für den Fall, dass die Verschlüsselungsmethode "[formbar](https://en.wikipedia.org/wiki/Malleability_(cryptography))" ist.

[[Quelle](https://crypto.stackexchange.com/questions/202/should-we-mac-then-encrypt-or-encrypt-then-mac)]

<!-- Ein kurzer exkurs zu Formbarkeit:
Bei jeder Sprache sind die Buchstaben des zugehörigen Alphabetes bestimmt verteilt. In der deutschen Sprache sieht diese Verteilung zum Beispiel wie folgt aus (Relative Häufigkeit der Buchstaben, basierend auf deutschsprachigen texten) [Bildquelle](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Buchstabenh%C3%A4ufigkeit_Deutsch.svg/1920px-Buchstabenh%C3%A4ufigkeit_Deutsch.svg.png):
<div style="width: 100%">
![Relative Buchstabenverteilung Deutsche Sprache](https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Buchstabenh%C3%A4ufigkeit_Deutsch.svg/1920px-Buchstabenh%C3%A4ufigkeit_Deutsch.svg.png)
</div>

Wenn wir jetzt einen verschlüsselten Text haben, dessen Verschlüsselung formbar ist, können wir mittels Mustererkennung versuchen den Text zu entschlüsseln ohne die Ver/Entschlüsselungsfunktion zu kennen. Dies liegt daran, dass wir theoretisch davon ausgehen können, dass das am häufigsten auftretende Muster vermutlich ein E darstellt, dass zweithäufigste ein I, und so weiter.
Durch das Hinzufügen einer HMAC-Komponente am Ende des verschlüsselten Textes, ist es schwieriger diese Muster zu erkennen, da nicht nur die durch die Verschlüsselungsfunktion verschlüsselte Daten enthalten sind, sondern auch durch die MAC eine anderweitig "verschlüsselte" (bzw eher gehashte) Datenmenge existiert.
-->
#### heartbeat Erweiterung

Mittels der Heartbeat-Erweiterung kann ein Client / Server kommunizieren, dass dieser das TLS Heartbeat Protokoll unterstützt.
Das Heartbeat-Protokoll erlaubt das Versenden von Heartbeat-Paketen, mittels welcher ein Client oder ein Server signalisieren kann,
dass er, trotz einiger Zeit ohne gesendeter Pakete, noch aktiv ist und die Verbindung aufrecht halten möchte.

Die Erweiterung, beziehungsweise das Protokoll erhielt durch die Heartbleed Lücke im Jahr 2014 größere Aufmerksamkeit. Mittels dieser ist es möglich Daten aus dem verschlüsselten Speicherbereich eines Servers auszulesen. Diese Lücke ist jedoch kein Problem im TLS-Protokoll (wie bei vorherigen Erweiterungen beschrieben), sondern ein Fehler in der Implementierung der OpenSSL-Bibliothek. Entsprechend sind nur einige Server mit betroffenen Versionen dieser Bibliothek angreifbar und nicht alle Server mit Unterstützung des TLS-Heartbeat-Protokolls.

Um die Lücke im Heartbeat-Protokoll ausnutzen zu können, reicht eine einfache Anfrage eines Client an einen Server mit einem fehlerhaften Heartbeat-Paket.
Ein solches Paket enthält (nach [RFC-6520](https://tools.ietf.org/html/rfc6520#page-5)) nur 4 Datensätze:
- `type` (**1 byte**): Typ der Anfrage (1 = Request, 2 = Response)
- `payloadLength` (**2 byte**): Anzahl der bytes, welche in `payload` hinterlegt sind
- `payload` (**dynamisch**): Irgendwelche Daten, diese werden wieder an den Client zurückgesendet
- `padding` (**dynamisch, min 16 bytes**): Benötigt um Mindestgrößen für bestimmte Pakete einzuhalten, wird vom Server ignoriert

Wenn nun der Wert in payloadLength größer ist als die mitgegebenen Daten, versucht der Server nicht nur die mitgesendeten Daten zu lesen,
sondern noch weitere hinter diesen im Speicher. Da die Daten im von OpenSSL verschlüsselten Speicher abgelegt wurden, werden hier also
empfindliche Daten (*z.&nbsp;B. Nutzernamen, Passwörter, Zertifikate oder was auch immer zuletzt genutzt wurde*) mitgeliefert.

Der Fehler ist zwar behoben, trotzdem wird aus Sicherheitsgründen weiterhin empfohlen diese Erweiterung nur zu Nutzen, wenn die Funktionalität wirklich benötigt wird.

Die Heartbeat Erweiterung enthält neben den generischen Daten nur einen 1 byte großen Datensatz, welcher den Modus aushandelt.
Es gibt 2 Modi:

- `1`: Nur der Client darf Heartbeat-Anfragen senden. Der Server muss diese beantworten
- `2`: Client und Server dürfen Heartbeat-Anfragen senden. Beide Antworten entsprechend

#### server_name Erweiterung

Da das TLS-Protokoll über dem Anwendungsprotokoll arbeitet, ist es nicht direkt möglich bei einem Handshake festzustellen
für welchen Host die Verbindung aufgebaut werden soll, da nur die Ziel-IP aber nicht die (DNS)-Domain bekannt ist. Dies führt zu dem
Problem, dass der Server nicht weiß, mit welchem Zertifikat dieser sich ausweisen soll, da auf einem (beispielsweise) Webserver mehrere Webseiten mit unterschiedlichen Domains gehostet werden können, welche dementsprechend verschiedene Zertifikate nutzen.

Um dieses Problem zu lösen, kann ein Client mit der server_name Erweiterung spezifizieren, welcher Hostname/Domainname angefragt wurde.
Die Erweiterung enthält, neben den generischen Daten, folgende spezifische Daten:
- `entry_size` (**2 byte**): Länge des Eintrags
- `name_type` (**1 byte**): Der Typ des namens. Derzeit ist nur der Wert 1 als "Host name" zugewiesen
- `name` (**dynamisch**): Der Hostname

Dabei können von diesen Datentupeln mehrere hintereinander vorkommen, um so eine Liste an angefragten hosts (oder anderen Datentypen, wenn diese im RFC definiert wurden) zu konfigurieren.

## Java-Implementierung

In diesem Kapitel werden genauere Informationen zur Strukturierung der Java-Implementierung genannt.
Hierbei wird nur auf die High-Level Konzepte eingegangen. Die Beschreibung der Low-Level-Komponenten kann der dieser Dokumentation angehängten API-Dokumentation (Javadoc) entnommen werden.

### Scan-Ablauf
Das Scannen eines Hosts erfolgt derzeit in 4 Schritten. Diese sind:

- **Schritt 1.** Protokoll-Prüfungen:
In diesem Schritt werden die eigentlich Ziel-URLs für eine gesicherte und ungesicherte Verbindung abgerufen (*z.&nbsp;B. durch Auflösung von HTTP-Weiterleitungen*). Nach diesem Schritt, werden generische Protokollprüfungen durchgeführt. Da das Hauptprogramm Protokollagnostisch aufgebaut ist (siehe [Überblick Strukturierung](#überblick-strukturierung)), werden
diese Prüfungen an die Implementation zur Protokollkommunikation delegiert. In diesem Schritt findet derzeit auch die Zertifikatsprüfung statt

- **Schritt 2.** Generische TLS-Versionsprüfungen:
In diesem Schritt werden 4 Verbindungen zur verschlüsselten Zieladresse des Hosts aufgebaut.
Jede dieser Verbindungen forciert eine andere TLS-Version. Diese Prüfung erfolgt rein mittels der TLS/SSL-Library der Java-Standardbibliothek.
Dadurch wird geprüft, ob der Host generell die folgenden TLS-Protokollversionen unterstützt:

  - TLS v1.0
  - TLS v1.1
  - TLS v1.2
  - TLS v1.3

- **Schritt 3.** Prüfen von TLS-Protokollversionen mit spezifischen Cipher Suites:
In diesem Schritt werden Anfragen mit spezifische Kombinationen aus Ciphern und TLS-Versionen an den Zielserver gesendet, um zu überprüfen ob diese Versionen unterstützt werden. Welche Kombinationen genau versandt werden, kann über die Konfigurationsdatei (siehe [Konfiguration](#konfiguration)) definiert werden. Falls möglich werden für diese Prüfungen Abfragen über die Java Standard-Bibliothek zu TLS/SSL-Kommunikation versandt. Falls jedoch einer der konfigurierten Cipher nicht von dieser unterstützt wird, wird die Handshake-Anfrage über die Custom-Klassen generiert und ausgewertet (siehe [Cipher Suite Mocking](/articles/tests/ciphers.html#cipher-suite-mocking))

- **Schritt 4.** Prüfen von möglichen Sicherheitsmängeln durch fehlerhafte Konfiguration:
In diesem Schritt werden Prüfungen durchgeführt, welche Exploits durch das Nutzen von fehlerhaften Konfigurationen oder der Nutzung von veralteter Software aufdecken sollen. Hierzu wird meist überprüft, ob bestimmte TLS-Erweiterungen aktiv sind, welche bekannte Sicherheitsprobleme lösen. Je nach Art der zu prüfenden Lücke werden auch weitere Metriken, wie z.&nbsp;B. Versionen von Softwarebibliotheken miteinbezogen

> [!IMPORTANT]
> Bei der Prüfung der Angriffsvektoren werden keine Angriffe durchgeführt. Es wird lediglich die Konfiguration untersucht und eine Auswertung über die möglichen Angriffe ausgegeben

<!--  -->

> [!NOTE]
> Genauere Informationen zu den fachlichen Prüfungen jedes Schrittes können den dazugehörigen Kapiteln entnommen werden.

### Überblick Strukturierung

Bei der Planung der Anwendung wurde darauf geachtet den Scanner möglichst protokoll-agnostisch zu halten.
Dadurch soll in Zukunft erlaubt werden andere Protokolle, welche mittels TLS Verschlüsselt werden (*z.&nbsp;B.
SSH oder direkt TCP over TLS verbindungen*) überprüfen zu können. Im Rahmen des Projektes wurde eine
HTTP-Spezifische Implementierung zur Kommunikation mit HTTP-Servern implementiert. Weitere Protokolle sind
jedoch theoretisch ebenfalls einbindbar.  
Da es jedoch noch keine solchen Versuche gegeben hat, fehlt dafür vermutlich dennoch noch notwendige Code-Infrastruktur. Speziell im Hinblick auf beidseitig authentifizierte Verbindungen oder Verbindungen welche Pre-Shared-Keys nutzen. Diese werden in der aktuellen Version noch nicht unterstützt.

Der eigentliche TLS-Scanner wurde in einer Art umgesetzt, welche die Nutzung als Library zulässt, um diesen so in
ein größeres Programm einbinden zu können. Die aktuelle Implementierung ermöglicht die Ausführung als CLI in einem Terminal, welche mittels Kommandozeilenparametern konfiguriert wird und alle Ausgaben auf der Konsole ausgibt.
Neben dem existierenden Konsoleninterface sollte es relativ einfach sein, weitere Benutzeroberflächen wie GUIs, TUIs oder auch Web-Interfaces zur Steuerung einzusetzen.

Die hauptsächliche Konfiguration des Scanners erfolgt über eine XML-Datei. In dieser können die 4 Hauptprüfungsschritte konfiguriert werden.

Zusätzlich wurde eine "Reporting Engine" erstellt. Diese legt die Ergebnisse eines Laufs im Arbeitsspeicher ab, sodass
diese auch außerhalb der Laufzeit weiterverarbeitet werden können.

Um den Scanner asynchron ausführen zu können und trotzdem Daten direkt an eine Frontend-Implementierung weitergeben zu können, wurde ein Callback-System implementiert, welches vor und nach jedem der 4 Hauptschritte, sowie vor und nach jedem einzelnen Test aufgerufen wird.  
So können Ergebnisse während der Laufzeit angezeigt werden, anstatt den Abschluss des Scanvorgangs abwarten zu müssen.  
Falls dem Nutzer der Bibliothek jedoch ein nachgelagerter Report ausreicht, besteht auch die Möglichkeit
den Scanner synchron laufen zu lassen und im Nachhinein den generierten Report auszuwerten.

### Konfiguration

Die Konfiguration des TLSScanner erfolgt durch XML-Dateien. Im Rahmen des Projektes wurde eine Konfigurationsdatei für
die [Technische Richtlinie TR-02102-2](https://gitlab.com/notepass/tlsscanner/raw/master/src/main/resources/cipher_catalogs/BSI_cipher_catalog.xml) angelegt.
Diese wird immer mit dem Programm als eingebettete Konfigurationsdatei ausgeliefert. Der genaue Aufbau der Konfigurations-XML ist über eine [XSD-Datei](https://gitlab.com/notepass/tlsscanner/raw/master/src/main/resources/schema/cipher_catalog/cipher_catalog.xsd) definiert.
Innerhalb der XML-Datei können folgende Einstellungen getätigt werden:
- Name des Katalogs (*z.&nbsp;B. Technische Richtlinie TR-02102-2 [...]*)
- URL für weitere Informationen
- Gültigkeitsdauer
- Konfiguration der Java Virtual Machine (Derzeit nur System-Properties). Dies wird zum Beispiel zur Konfiguration der zu nutzenden Diffie-Hellman-Gruppen für TLS-Verbindungen genutzt oder um als unsicher geltende Cipher Suites wieder zur Nutzung zu aktivieren
- Konfiguration der Laufzeitparameter für die Anwendung:

  - Hier kann ein "SocketProducer" hinterlegt werden, welcher die Socket-Objekte zur Kommunikation mit dem Host erstellt
  - Zudem wird hier die Klasse der "ProtocolCommunicator" hinterlegt, welcher die Kommunikation über das High-Level-Protokoll (*z.&nbsp;B. HTTP*) übernimmt und die protokollspezifischen Prüfungen durchführt)

- Welche Angriffsvektoren geprüft werden sollen
- Auf welche Erweiterungen geprüft werden soll
- Eine Liste an TLS-Versions und Cipher Suites, welche in Schritt 3 (Prüfen von TLS-Protokollversionen mit spezifischen Ciphern)
geprüft werden sollen.

Genaue Informationen zur Konfigurationsdatei können der [XSD-Datei](https://gitlab.com/notepass/tlsscanner/raw/master/src/main/resources/schema/cipher_catalog/cipher_catalog.xsd)
beziehungsweise der [bestehenden XML-Konfiguration](https://gitlab.com/notepass/tlsscanner/raw/master/src/main/resources/cipher_catalogs/BSI_cipher_catalog.xml) entnommen werden.

### Ausblick

Während das Tool bereits viele Funktionalitäten beinhaltet, gibt es derzeit bereits Ideen zur Erweiterung und Verbesserung der Anwendung:

- Erweiterung der Reporting-Engine zur Beinhaltung von Informationen, wie Probleme auf beliebten Webservern (Apache, nginx) behoben werden können, sodass diese Hinweise von jedem angeschlossenen UI angezeigt werden können
- Support für TLS 1.0, 1.1 und verschiedene SSL-Versionen in den eigens implementierten TLS-Handshake Klassen
- Möglichkeit des Hinzufügens von Custom-Steps in den TLS-Scanner (Rework der Klasse TlsScanner und des TlsScannerReport benötigt)
- Erweiterung der Konfigurationsdatei, sodass Schritt 2 (Generische TLS-Versionsprüfungen) konfiguriert werden kann
- Erstellung eines GUIs (*z.&nbsp;B. mittels JavaFX oder als <abbr title="Single Page Application (webbasierte Browseranwendung)">SPA</abbr>*)
- Erstellung von "ReportFormattern", welche es erlauben eine Ausgabedatei aus dem Report-Objekt (*z.&nbsp;B. XML zur automatisierten Weiterverarbeitung
oder HTML/DOCX zur menschlichen Auswertung*) zu erzeugen.
