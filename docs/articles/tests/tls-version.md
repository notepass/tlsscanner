---
uid: tls-scanner-net-docs-tests-tls-version
authors: Florian Hiensch
creationDate: 28.12.2019
---

# Prüfung der verwendeten TLS/SSL-Version

## Überprüfte Kriterien

|Fall     |Eigenschaften|Werte    |Prüfergebnis    |
|:-------:|:-----------:|:-------:|:--------------:|
|1.       |TLS-Version  |1.0      |:warning: Warnung |
|2.       |TLS-Version  |1.1      |:warning: Warnung |
|3.       |TLS-Version  |1.2      |:heavy_check_mark: Erfolg |
|4.       |TLS-Version  |1.3      |:heavy_check_mark: Erfolg |

> [!NOTE]
> Es wird **keine Prüfung** durchgeführt ob das veraltete **<abbr title="Secure Socket Layer">SSL</abbr>-Protokoll** verwendet wird, da dieses als überholt gilt und von den meisten Frameworks und Anwendungen bereits nicht mehr implementiert bzw. verwendet wird

Ziel dieses Tests ist es herauszufinden, welche Verschlüsselungsverfahren für die Kommunikation von der Gegenseite angeboten werden.

Hierzu wird wiederholt eine Verbindung mit der Gegenseite hergestellt, jeweils unter Verwendung eines unterschiedlichen Verschlüsselungsverfahrens.
