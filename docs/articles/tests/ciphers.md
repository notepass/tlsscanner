---
uid: tls-scanner-net-docs-tests-certificate
authors: Florian Hiensch
creationDate: 28.12.2019
---

# Prüfung der serverseitig unterstützten Cipher Suites

Bei Cypher Suites handelt es sich um Sammlungen von kryptografischen Verfahren, die im TLS-Protokoll die zur Verschlüsselung verwendeten Algorithmen festlegen.

Diese legen dabei jeweils vier Komponenten der Verschlüsselung fest:

- das **Schlüsselaustauschverfahren**, z.&nbsp;B. RSA, Diffie-Hellmann, Pre-Shared-Key, o. A.
- das **Authentifizierungsverfahren**, z.&nbsp;B. DSA, ECDSA, o. A.
- das **Verschlüsselungsverfahren**, z.&nbsp;B. DES, 3DES, AES, o.A.
- die **Hashfunktion**, z.&nbsp;B. MD5 oder SHA

Neben der Kombination der verschiedenen Verfahren untereinander, enthalten die Cipher Suites zudem die jeweils verwendeten Schlüssellängen.
Jede Cipher Suite ist zudem für die Verwendung unter einer bestimmten Version des TLS-Protokolls vorgesehen und ist einem eindeutigen Zahlenwert zugeordnet (*z.&nbsp;B.* `{ 0xC0, 0x13 }`).

## Sicherheit der Cipher Suites

Ein Faktor, welcher bei der Bewertung von Cipher Suites eine Rolle spielt, ist die Sicherheit der verwendeten Algorithmen zum Zeitpunkt der Verwendung.

Denn für viele Algorithmen wurden im Laufe der Zeit und ihrer Nutzung Mängel oder Sicherheitslücken festgestellt, welche die Integrität der TLS-Verbindung gefährenden können, sollten diese dafür eingesetzt werden.

Deshalb können Cipher Suites generell in 4 Kategorien eingeteilt werden: <span style="color:limegreen">Empfohlen</span>, <span style="color:dodgerblue">Sicher</span>, <span style="color:orange">Schwach</span> und <span style="color:red">Unsicher</span>

- **Unsichere** Cipher Suites sollten auf **keinen Fall** mehr verwendet werden.  
- **Schwache** Cipher Suites sollten **für neue Systeme nicht mehr eingesetzt** und bei bestehenden Systemen auch **ausgetauscht werden**. Der Einsatz von schwachen Cipher Suites sollte sich auf **Sonderfälle** konzentrieren.  
- **Sichere** Cipher Suites werden als ***"state-of-the-art"*** angesehen und sollten statt unsicherer oder schwacher Cipher Suites verwendet werden. Eine Verwendung sollte auch auf den meisten älteren Systemen möglich sein.  
- **Empfohlene** Cipher Suites gelten **ebenfalls als sicher**, sind darüber hinaus aber für **Perfect Forward Secrecy (PFS)** geeignet. Dies bedeutet, dass Algorithmen verwendet wurden, welche es nicht ermöglichen im Nachhinein einen zuvor gespeicherten, gesicherten Datenverkehr zu entschlüsseln, sollten die Langzeitschlüssel, welche als Basis für die verwendeten Sitzungsschlüssel dienen, wie z.&nbsp;B. der private Schlüssel eines Zertifikats bekannt werden.

Eine Cipher Suite kann z.&nbsp;B. als unsicher angesehen werden, wenn diese eine unsichere Verschlüsselung (wie der Data Encryption Standard DES), eine von Gremien wie dem IETF für die Nutzung in TLS verbotene (wie Rivest Cipher 4 RC4) oder gleich gar keine Verschlüsselung besitzen.

## Verwendung von Cipher Suites in TLS

Die zur Verfügung stehenden Cipher Suites ist unter anderem auch von der verwendeten TLS Version abhängig. Wie zuvor erwähnt, sind bestimmte Algorithmen für die Verwendung unter TLS untersagt und nicht in den verabschiedeten Standard mitaufgenommen.  

So wurde für die TLS Version 1.3 Perfect Forward Secrecy als verpflichtendes Merkmal eines Algorithmus definiert.  
Dies hat zur Folge, dass beispielweise die Nutzung statischer Schlüsselgenerierungs- oder <wbr>-austauschverfahren wie RSA, Diffie-Hellman oder Elliptic-Curve Diffie-Hellman untersagt wurde.

Die letzteren beiden Verfahren wurde durch eine sog. "flüchtige" Variante ersetzt, dem sogenannten Diffie-Hellman Ephemermal (DHE) respektive Elliptic-Curve Diffie-Hellman Ephemeral (ECDHE). Bei dieser Variante des Algorithmus wird der für den Schlüsselaustausch verwendete Parameter für jede TLS-Session verändert, um Forward Secrecy zu ermöglichen.

Weitere Algorithmen, die unter TLS 1.3 nicht mehr verwendet werden können sind z.&nbsp;B.:

- RC4 Steam Cipher
- RSA Key Transport
- SHA-1 Hash Function
- CBC Mode Ciphers
- MD5 Algorithm
- Various Diffie-Hellman groups
- EXPORT-strength ciphers
- DES
- 3DES

Für TLS 1.3 stehen nun noch folgende Cipher Suites zur Verfügung:

| Name                         | Value          |
|:-----------------------------|:---------------|
| TLS_AES_128_GCM_SHA256       | { 0x13, 0x01 } |
| TLS_AES_256_GCM_SHA384       | { 0x13, 0x02 } |
| TLS_CHACHA20_POLY1305_SHA256 | { 0x13, 0x03 } |
| TLS_AES_128_CCM_SHA256       | { 0x13, 0x04 } |
| TLS_AES_128_CCM_8_SHA256     | { 0x13, 0x05 } |

> [!NOTE]
> Unter TLS 1.3 werden nur noch das Verschlüsselungsverfahren und der Hashing-Algorithmus im Namen definiert  
> Dies liegt daran, dass als Schlüsselaustauschverfahren nur noch flüchtige Diffie-Hellman-basierte Algorithmen verwendet werden und der verwendete Authentifizierungsalgorithmus auf das verwendete Zertifikat definiert wird.

Als Algorithmen für die Authentifizierung, also die Erbringung des Beweises, dass die Gegenpartei im Besitz des privaten Schlüssels für das Zertifikat, mit dem sie sich ausweist, ist, können unter TLS 1.3 RSA, <abbr title="Elliptic Curve Digital Signature Algorithm">ECDSA</abbr> oder <abbr title="Edwards-Curve Digital Signature Algorithm">EdDSA</abbr> verwendet werden.

### `TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA`

Numerischer Wert: `{ 0xC0, 0x13 }`

Diese Cipher Suite  ist für **TLS 1.0 - 1.2** vorgesehen und definiert als Schlüsselaustauschverfahren Diffie-Hellmann auf einer Elliptischen-Kurve (EC: Elliptic Curve), RSA (Rivest Shamir Adleman) als Authentifizierungsverfahren und AES (Advanced Encryption Standard) mit einem 128-bit Schlüssel und dem Cipher Block Chaining Verfahren (CBC).  
Als Hashfunktion kommt SHA (Secure Hash Algorithm) zum Einsatz.  

Diese Cipher Suite ist ein gutes Beispiel für eine mögliche Angreifbarkeit, bei der Verwendung innerhalb von TLS.  
Bei dieser spezifischen Suite ist ein möglicher Wunder Punkt die Verwendung des Cipher Block Chainings. Dieses Betriebsart des AES ist anfällig für einen Klartextangriff, wenn er zusammen mit TLS 1.0 oder geringer verwendet wird (**BEAST-Angriff**).

Für die Verwendung unter TLS 1.2 wird ein als wirksam erachteter Fix empfohlen, welcher in der Verwendung des **GCM (Galois/Counter Mode)** statt CBC besteht.

Zudem gilt der verwendete **SHA1 Algorithmus seit 2017 als verwundbar**, nachdem ein erfolgreiche Kollisionsangriff durchgeführt werden konnte. Es sollte die Verwendung von **SHA-256 oder SHA-384** in Betracht gezogen werden.

***

### `TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384`

Numerischer Wert: `{ 0xC0, 0x30 }`

Diese Cipher Suite stellt eine empfohlene Alternative zur zuvor genannten dar. Statt AES 128 mit Cipher Block Chaining wird AES mit einem **256-bit Schlüssel und dem GCM Betriebsmodus** verwendet.  
Außerdem wird **SHA-384** anstatt SHA1 eingesetzt.

Die zuvor genannte Cipher Suite wird zwar noch als sicher angesehen, es wird jedoch **explizit empfohlen**, diese nicht zu nutzen, wenn dies nicht notwendig ist.

Die Notwendigkeit kann aus der verwendeten TLS Version entstehen, da die hier gezeigte Alternative unter anderem aufgrund des eingesetzten GCM Modus **erst ab TLS 1.2** verwendet werden kann

## Relevanz der Schlüssellänge

Ein weiterer Faktor, der in die Bewertung von Cipher Suites einfließt, ist die verwendete Schlüssellänge der Algorithmen.
Generell gelten Algorithmen mit größeren Schlüssellängen als sicherer, da die benötigte Rechenleistung um diese anzugreifen mit der Schlüssellänge wächst.

Bei den meisten für das TLS-Protokoll verwendeten Kryptographieverfahren handelt es sich um asymmetrische Verfahren, das heißt Verfahren die als berechtigte Partei leicht durchzuführen sind, aber nur mit hohem Rechenaufwand von einer dritten, unberechtigten Partei angegriffen werden können.

Da die Leistungs-/Preisverhältnis und auch die generelle Rechenleistung der hergestellten (Krypto-)Prozessoren stetig zunimmt, ist es wichtig die Komplexität der verwendeten Kryptographieverfahren ebenfalls zu steigern, um diesen Prozess auszugleichen.

Deshalb werden von verschiedenen Institutionen regelmäßig aktualisierte Verwendungsempfehlungen herausgegeben, welche Cipher Suites noch bis zu welchem Zeitpunkt als sicher betrachtet werden können.

## Umsetzung in der Anwendung

Im Report des BSI sind ebenfalls Angaben hierzu zu finden.

Diese wurden in folgender Struktur ebenfalls in die Konfigurationsdatei aufgenommen:

```xml
[...]
    <CipherSet>
        <Protocol>
            <Name>TLSv1.3</Name>
            <IsSecureTill>2026-01-01</IsSecureTill>
        </Protocol>
        <Cipher>
            <Name>TLS_AES_128_GCM_SHA256</Name>
            <IsSecureTill>2026-01-01</IsSecureTill>
        </Cipher>
        [...]
    </CipherSet>
[...]
```

Neben der Zielversion des TLS-Protokolls für die entsprechende Menge an Cipher Suites ist für jede Suite neben ihrem Namen auch ihr aktuell vorgesehenes Mindestgültigkeitsdatum angegeben.

Auf Basis der Information aus dem BSI-Report wurden nur die dort empfohlenen Cipher Suites mit in die Konfigurationsdatei aufgenommen. Es wurde keine Klassifizierung in 4 Kategorien, wie sie oben beschrieben wurde, durchgeführt.

### Prüfung der unterstützten Ciphers

Um zu prüfen, ob der angegebene Remotehost eine Cipher Suite unterstützt, wird pro getesteter Suite eine TLS-Verbindung zum Server aufgebaut.
Geprüft werden hierbei nicht nur die in der Konfiguration vorhandenen, sondern alle Cipher Suites, welche von Java bereitgestellt werden.

#### Cipher Suite Mocking

Da nicht alle in Java zur Verfügung stehenden Cipher Suites auch auf jedem Zielsystem, welches durch die Java Laufzeitumgebung unterstützt wird, zur Verfügung stehen, ist es möglich diese von der Anwendung mocken zu lassen, um ihre Verwendbarkeit dennoch zu prüfen.

Hierbei wird kein vollständiger Handshake mit dem Zielsystem aufgebaut, sondern nur ein Client-Hello durchgeführt, um zu prüfen, ob der Server die Verbindung mit der vorgeschlagenen Cipher Suite eingeht oder diese ablehnt.

Dieses Verhalten kann mithilfe des Optionsschalters `--allow-mock-client` beeinflusst werden.

#### Nicht geprüfte Cipher Suites

> [!IMPORTANT]
> Aus technischen Gründen werden nicht alle möglichen Cipher Suites getestet.

Der Test über die Verfügbarkeit einer Cipher Suite wird nur für bestimmte Kombinationen durchgeführt.

Grundsätzlich müssen Cipher Suites, die geprüft werden sollen in der verwendete Java Umgebung zur Verfügung stehen. Dabei ist die zur Verfügung stehende Java Version entscheidend, da diese die darunterliegende Java TLS-Implementierung festlegt.

Ist das Mocking von Cipher Suites aktiviert, können auch nur die Suites getestet werden, welche in der externen Bibliothek BouncyCastle Version unterstützt werden, da diese zur Erstellung der gemockten ClientHello-Messages verwendet wird.

Cipher Suites die generell nicht geprüft werden, sind solche, die im Rahmen eines Tests mit [!include[prod](../../prodlong.md)] nicht geprüft werden können, wie beispielsweise Verbindungen mit zuvor vereinbartem Schlüssel (Pre-Shared-Key/PSK).

Die mitgelieferte Konfigurationsdatei "BSI_cipher_catalog" hat eine Prüfung der Cipher Suites zur Folge, welche im BSI Report "TR-02102-2" festgehalten wurden und ebenfalls die oben genannten Kriterien erfüllen.

[[Quelle 1](https://tools.ietf.org/html/rfc5246)]<!-- TLS 1.2 -->
[[Quelle 2](https://tools.ietf.org/html/rfc8446)] <!-- TLS 1.3 -->
[[Quelle 3](https://ciphersuite.info/page/faq/)]
[[Quelle 4](https://www.thesslstore.com/blog/cipher-suites-algorithms-security-settings/)]
[[Quelle 5](https://www.thesslstore.com/blog/tls-1-3-approved/)]
[[Quelle 6](https://www.iana.org/assignments/tls-parameters/tls-parameters.xhtml)]
