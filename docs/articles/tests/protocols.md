---
uid: tls-scanner-net-docs-tests-protocols
authors: Florian Hiensch
creationDate: 24.01.2020
---

# Prüfung der unterstützen Protokolle

## Überprüfte Kriterien

|Fall     |Eigenschaften    |Werte              |Prüfergebnis    |
|:-------:|:---------------:|:-----------------:|:--------------:|
|1.       |HTTP &rarr; HTTPS|Keine Weiterleitung|:warning: Warnung |
|2.       |HTTP &rarr; HTTPS|Weiterleitung      |:heavy_check_mark: Erfolg |
|3.       |HTTP &rarr; HTTPS|Gleiches Ziel      |:heavy_check_mark: Erfolg |
|4.       |HTTP &rarr; HTTPS|Abweichendes Ziel  |:heavy_check_mark: Erfolg |

Im ersten Schritt wird geprüft, ob die Website sowohl das HTTP- als auch HTTPS-Protokoll unterstützt.

Hierbei wird auch eine möglicherweise vorhandene Weiterleitungskette aufgelöst, indem den Weiterleitungen gefolgt wird (`HTTP 3**`), bis diese ausbleiben (`HTTP 2**`) oder ein Protokollwechsel stattfindet.

Diese Protokollwechsel werden ebenfalls registriert, um zu prüfen ob eine Weiterleitung dafür eingerichtet wurde. Generell wird empfohlen jeglichen HTTP Verkehr, wenn möglich, automatisch auf HTTPS umzuleiten.
