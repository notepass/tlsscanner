$docsPath = '..\target\apidocs\generated-files'

Write-Output 'TlsScanner Documentation BuildScript'

Write-Output 'Copying javadoc documentation'

if(!(Test-Path($docsPath))){
   Write-Error -Message "Path ..\target\apidocs\generated-files does not exist. Please make sure you have built the solution using maven (e.g. mvn clean install)"
}

Copy-Item -Path "$($docsPath)\*" -Destination '.\api' -Force

Write-Output 'Building docfx documentation'

dotnet build