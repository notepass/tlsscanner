---
uid: tls-scanner-net-docs-api-index
authors: Florian Hiensch
creationDate: 28.12.2019
---

# TLS Scanner - Objektreferenz

Verwenden Sie diese Objektreferenz um tiefere Kenntnisse über die Funktionsweise von [!include[prod](../prodlong.md)] zu erhalten, welche ihnen ermöglichen Fehlerbehebungen oder Erweiterungen der Software durchzuführen.

Mithilfe dieser Dokumentation können Sie sich mit den intern verwendeten Strukturen der Anwendung vertraut machen sowie sich über mögliche technologische Fallen informieren.

## Erste Schritte

Neu in der Entwicklung von [!include[prod](../prodlong.md)]? Zunächst erfahren Sie mehr über die intern verwendeten Strukturen und anschließend über die Funktionsweise der durchgeführten Prüfungen:

<h1> LINK ZU STRUKTUR </h1>
<h1> LINK ZU PRÜFUNGEN </h1>
