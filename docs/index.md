---
uid: tls-scanner-net-docs-index
authors: Florian Hiensch
creationDate: 28.12.2019
title: TLS Scanner Dokumentation
---

<h1 class="index-heading">
    <img alt="TLS Scanner Logo" src="media/logo.png" class="logo">
    [!include[prod](prodlong.md)] Dokumentation
</h1>

Die Dokumentation von [!include[prod](prodlong.md)] dient als zentrale Informationsquelle, die Nutzern und Entwicklern des Produkts hilft, die Funktionalitäten besser verstehen und gegebenenfalls verändern oder ergänzen zu können.

Hierzu ist die Dokumentation in verschiedene Bereiche aufgegliedert:

- **Technische Dokumentation**  
  Enthält allgemeine Informationen zum Anwendungsdesign und den verwendeten Produkten und Verfahren

- **Referenzdokumentation**  
  Die Referenzdokumentation umfasst eine detaillierte API-Referenz der Anwendung sowie die entsprechende technische Anwendungsdokumentation

<br/>
<br/>

## Verwendung der Anwendung

```powershell
TlsScanner
    -h <String>
    -i <String>
    [--allow-mock-client]
```

## Parameter

### `--host`

Gibt den Host an, der für die Durchführung der Prüfung verwendet werden soll. Das erwartete Format ist eine gültige URL mit Angabe des Protokolls und Zielports.
***
Typ:  String
***
Alias: h
***
Position: Benannt
***
Verpflichtend: Ja

### `--config-file`

Gibt den Pfad zur Konfigurationsdatei an, welche für die Durchführung der Prüfungen verwendet werden soll. Das erwartete Format ist ein gültiger Dateisystempfad.  
**Ist nicht kompatibel mit** `--internal-config-file`
***
Typ:  String
***
Alias: c
***
Position: Benannt
***
Verpflichtend: Nein

### `--internal-config-file`

Gibt den Namen einer internen Konfigurationsdatei an, welche für die Durchführung der Prüfungen verwendet werden soll. Das erwartete Format ist ein gültiger Name.  
**Ist nicht kompatibel mit** `--config-file`
***
Typ:  String
***
Alias: i
***
Position: Benannt
***
Verpflichtend: Nein

### `--allow-mock-client`

Gibt an, ob zur Prüfung von vom Clientsystem nicht unterstützen Cipher Suites eine Prüfung per Client-Hello anstatt eines Handshakes durchgeführt werden soll. Wird diese nicht durchgeführt, wird die Überprüfung von nicht unterstützten Cipher Suites übersprungen.
***
Typ:  Optionsschalter
***
Alias: m
***
Position: Benannt
***
Standardwert: false
***
Verpflichtend: Nein

## Beispiele

```powershell
tlsscanner -h http://google.de:80/ -i BSI_cipher_catalog.xml --allow-mock-client true
```

In diesem Beispiel wird ein Test für die Seite `google.de` ausgeführt. Dazu wird die interne Konfiguration `BSI_cipher_catalog.xml` verwendet und das Mocking von Cipher Suites ist aktiviert.
