# TlsScanner
This project aims to provide a library which can scan a server for possible TLS-Vulnerabilities
and can give Information about the security of the configured TLS-Versions and cipher sets.

## Building from scratch
### Perequirements
- At least a Java 11 JDK (Java 8 might also work, but wasn't tested sufficiently), OpenJDK and OracleJDK should work
- Maven

### Building
A normal build can be obtained by running `mvn clean install package`. 
If you want to have javadoc, you will need to run  `mvn clean install package site`.
If you want to build the in-depth documentation you will also need the following prerequirements:
- A locally compiled version of [docfx-doclet](https://github.com/docascode/docfx-doclet) (Build with `mvn -Dmaven.test-skip=true clean install`
- An installation of the [dotnet core SDK](https://dotnet.microsoft.com/download)
- An installation of [docfx](https://github.com/dotnet/docfx/releases)
- Knowledge of the german language

When everything is installed (and added to the Path variable), run the command `mvn -Pdocfx clean install`.
After the command finishes, go into the docs folder and run `make.bat`. This will build the documentation and
bring up a local server on port `8123` where it can be accessed. To get the whole documentation you can open
the file `/export.html`.

## Running
A default build will bring a CLI-Interface with it. To run it, use:  
`java -jar target/TlsScanner-1.0-SNAPSHOT-jar-with-dependencies.jar`  
This will print a help prtompt containing (hopefully) everything you need to know.  

An example command line for running the tool is: `java -jar TlsScanner-1.0-SNAPSHOT-jar-with-dependencies.jar --allow-mock-client true --host http://bachgold.de --internal-config-file /cipher_catalogs/BSI_cipher_catalog.xml`  

Please note: In some java version / JDK implementations the following errors can show up. These have to do with used libraries which may be not completly compatible with the mixture of
java version and JDK implementation you are using. But, they are known and have been worked around, so it should be safe to just ignore them.

`WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by com.sun.xml.bind.v2.runtime.reflect.opt.Injector (file:[...]) to method java.lang.ClassLoader.defineClass(java.lang.String,byte[],int,int)
WARNING: Please consider reporting this to the maintainers of com.sun.xml.bind.v2.runtime.reflect.opt.Injector
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release`  

`WARNING: sun.reflect.Reflection.getCallerClass is not supported. This will impact performance.`

The programm will ned a configuration file to run. One is already build into it, testing the proposals made
by the [BSI](https://en.wikipedia.org/wiki/Federal_Office_for_Information_Security) in their 
[technical guideline TR-02102-2](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR02102/BSI-TR-02102-2.html)

## Configuration
In-depth information about configuring the can be found in the wiki (Currently WIP, there are no entries)

## Using
In-depth information about using the API found in the wiki (Currently WIP, there are no entries)