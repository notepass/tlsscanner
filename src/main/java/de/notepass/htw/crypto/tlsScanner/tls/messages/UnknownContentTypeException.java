package de.notepass.htw.crypto.tlsScanner.tls.messages;

/**
 * Will be thrown if the content type of a TLS record layer is unsupported by the parsing implementation<br>
 * Will also be thrown if the parsing implementation finds something fishy
 */
public class UnknownContentTypeException extends Exception {
    public UnknownContentTypeException() {
        super();
    }

    public UnknownContentTypeException(String message) {
        super(message);
    }

    public UnknownContentTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
