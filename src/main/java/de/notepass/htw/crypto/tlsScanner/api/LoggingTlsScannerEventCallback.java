package de.notepass.htw.crypto.tlsScanner.api;

import de.notepass.htw.crypto.tlsScanner.definition.IProtocolCommunicator;
import de.notepass.htw.crypto.tlsScanner.definition.ISocketFactory;
import de.notepass.htw.crypto.tlsScanner.impl.HttpProtocolCommunicator;
import de.notepass.htw.crypto.tlsScanner.reporting.AttackVector;
import de.notepass.htw.crypto.tlsScanner.reporting.TlsRequestSupportDataContainer;
import de.notepass.htw.crypto.tlsScanner.reporting.TlsScannerReport;
import de.notepass.htw.crypto.tlsScanner.reporting.TlsVersionAndCipherContainer;
import de.notepass.htw.crypto.tlsScanner.util.SSLCertificateInfo;
import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoggingTlsScannerEventCallback implements TlsScannerEventCallback {
    private static final Logger LOGGER = LogManager.getLogger(LoggingTlsScannerEventCallback.class);
    private URL effectivePlainUrl = null;
    private URL effectiveSecuredUrl = null;

    @Override
    public void beforeSetJvmProperty(String name, String currentValue, String newValue, TlsScannerReport report) {
        LOGGER.debug("Setting JVM property: {}={}", name, newValue);
    }

    @Override
    public void afterSetJvmProperty(String name, String oldValue, String currentValue, TlsScannerReport report) {

    }

    @Override
    public void onListSupportedCiphers(String[] supportedCiphers, TlsScannerReport report) {
        LOGGER.debug("Locally supported ciphers:");
        for (String cipher : supportedCiphers) {
            LOGGER.debug("Supported cipher: " + cipher);
        }
    }

    @Override
    public void beforeCreateSocketFactory(String className, TlsScannerReport report) {
        LOGGER.debug("Trying to create SocketFactory from class {}", className);
    }

    @Override
    public void afterCreateSocketFactory(ISocketFactory socketFactory, TlsScannerReport report) {
        LOGGER.debug("SocketFactory created");
    }

    @Override
    public void beforeCreateProtocolCommunicator(String className, TlsScannerReport report) {
        LOGGER.debug("Trying to create ProtocolCommunicator from class {}", className);
    }

    @Override
    public void afterCreateProtocolCommunicator(IProtocolCommunicator protocolCommunicator, TlsScannerReport report) {
        LOGGER.debug("ProtocolCommunicator created");
    }

    @Override
    public void beforeCalculateEffectivePlainUrl(String targetHost, TlsScannerReport report) {
        LOGGER.debug("Resolving plain URL redirection chain for URL " + targetHost);
    }

    @Override
    public void afterCalculateEffectivePlainUrl(URL plainUrl, TlsScannerReport report) {
        LOGGER.info("Last non-redirecting or plain-protocol endpoint for given URL is {}. This URL will be used for all plain-socket communication", plainUrl.toString());
        effectivePlainUrl = plainUrl;
    }

    @Override
    public void beforeCalculateEffectiveSecuredUrl(String targetHost, URL plainUrl, TlsScannerReport report) {
        LOGGER.debug("Resolving secured URL redirection chain for plain URL " + plainUrl.toString());
    }

    @Override
    public void afterCalculateEffectiveSecuredUrl(URL securedUrl, TlsScannerReport report) {
        LOGGER.info("Last non-redirecting or secured-protocol endpoint for given URL is {}. This URL will be used for all plain-socket communication", securedUrl.toString());
        effectiveSecuredUrl = securedUrl;
    }

    @Override
    public void beforeGenericProtocolCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[Start generic protocol checks]=======================================");
        LOGGER.info("Step 1 of 4: Doing protocol-checks");
    }

    @Override
    public void afterGenericProtocolCheckStepExecution(TlsScannerReport report) {
        if (effectivePlainUrl == null || effectiveSecuredUrl == null) {
            //Every check only makes sense if there is a unsecured and secured endpoint
            return;
        }

        Object httpsRedirectObj = report.getProtocolCheckMessages().get(HttpProtocolCommunicator.HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_HTTP_TO_HTTPS_REDIRECT);
        Boolean httpsRedirect = null;
        boolean filePortionUntouched = effectiveSecuredUrl.getFile() != null && effectiveSecuredUrl.getFile().equals(effectivePlainUrl.getFile());

        if (httpsRedirectObj instanceof Boolean) {
            httpsRedirect = (Boolean) httpsRedirectObj;
        }

        if (httpsRedirect != null && httpsRedirect) {
            LOGGER.info("Redirection from HTTP to HTTPS: Enforced");

            // Only check "file portion touched" if the redirect was enforced, elsewise this doesn't make much sense
            if (filePortionUntouched) {
                LOGGER.info("Redirection from HTTP to HTTPS: File portion of URL untouched");
            } else {
                LOGGER.warn("Redirection from HTTP to HTTPS: File portion of URL changed");
            }
        } else {
            LOGGER.warn("Redirection from HTTP to HTTPS: NOT Enforced");
        }
        LOGGER.info("=====================================[End generic protocol checks]=======================================");
    }

    @Override
    public void beforeGenericUnsecuredProtocolChecks(TlsScannerReport report) {

    }

    @Override
    public void afterGenericUnsecuredProtocolChecks(TlsScannerReport report) {

    }

    @Override
    public void onGenericUnsecuredProtocolChecksError(Exception e, TlsScannerReport report) {
        LOGGER.error("Error while doing unsecured protocol checks: " + e.getMessage(), e);
    }

    @Override
    public void beforeGenericSecuredProtocolChecks(TlsScannerReport report) {

    }

    @Override
    public void afterGenericSecuredProtocolChecks(TlsScannerReport report) {

    }

    @Override
    public void onGenericSecuredProtocolChecksError(Exception e, TlsScannerReport report) {
        LOGGER.error("Error while doing secured protocol checks: " + e.getMessage(), e);
    }

    @Override
    public void beforeGenericCertificateChecks(TlsScannerReport report) {
        LOGGER.info("Checking certificate properties");
    }

    @Override
    public void afterGenericCertificateChecks(TlsScannerReport report) {
        SSLCertificateInfo certInfo = report.getCertificateInfo();
        if (certInfo.getIsNotYetValid())
            LOGGER.info("The certificate is not yet valid, it's validity starts on " + certInfo.getNotBefore().toString());
        if (certInfo.getIsNotValidAnymore())
            LOGGER.warn("The certificate is not valid anymore, it was valid until " + certInfo.getNotAfter().toString());
        if (certInfo.getIsCertValid() == null)
            LOGGER.error("The certification chain could not be validated as no default trust provider was found");
        if (certInfo.getIsCertValid().equals(Boolean.FALSE))
            LOGGER.warn("The certificates certification chain was found to be invalid");
        if (certInfo.getIsCertValid().equals(Boolean.TRUE))
            LOGGER.info("The certification chain has been successfully validated");
        if (!certInfo.getIsSubjectValid())
            LOGGER.warn("The subject name does not match the certificate's subject DN neither one of the subject alternative names.");
        if (certInfo.getIsSubjectValid())
            LOGGER.info("The subject name is contained in the certificates subjects");
        if (!certInfo.getIsNotYetValid() && !certInfo.getIsNotValidAnymore() && certInfo.getIsCertValid().equals(Boolean.TRUE) && certInfo.getIsSubjectValid())
            LOGGER.info("The certificate is fully valid");
    }

    @Override
    public void onGenericCertificateChecksError(Exception e, TlsScannerReport report) {
        LOGGER.error("Error while doing certificate checks: " + e.getClass().getCanonicalName() + ": " + e.getMessage(), e);
    }

    @Override
    public void beforeGenericTlsSupportCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[Start generic TLS support checks]=======================================");
        LOGGER.info("Step 2 of 4: Checking TLS protocol support (any cipher) for " + Arrays.toString(Arrays.stream(TlsVersion.values()).map(TlsVersion::getReadableName).toArray(String[]::new)));
    }

    @Override
    public void afterGenericTlsSupportCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[End generic TLS support checks]=======================================");
    }

    @Override
    public void beforeGenericTlsCheck(TlsVersion tlsVersion, URL securedUrl, TlsScannerReport report) {

    }

    @Override
    public void afterGenericTlsCheck(TlsVersion tlsVersion, TlsRequestSupportDataContainer dataContainer, URL securedUrl, TlsScannerReport report) {
        switch (dataContainer.getFailReason()) {
            case NO_SERVER_SUPPORT:
                LOGGER.warn("Host does not support " + tlsVersion.getReadableName());
                break;
            case NO_CLIENT_SUPPORT:
                LOGGER.error("Client doesn't support " + tlsVersion.getReadableName());
                break;
            case NONE:
                LOGGER.info("Host supports " + tlsVersion.getReadableName());
                break;
            default:
            case UNKNOWN:
                LOGGER.error("Unknown error while checking support for " + tlsVersion.getReadableName() + ": " + dataContainer.getAdditionalInformation().getMessage(), dataContainer.getAdditionalInformation());
                break;
        }
    }

    @Override
    public void beforeSpecificTlsSupportCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[Start specific protocol cipher checks]=======================================");
        LOGGER.info("Step 3 of 4: Doing cipher checks");
    }

    @Override
    public void afterSpecificTlsSupportCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[End specific protocol cipher checks]=======================================");
    }

    @Override
    public void beforeSpecificTlsSupportGroupCheckStepExecution(TlsVersion version, TlsScannerReport report) {
        LOGGER.info("=====================================[Start check for protocol: {}]=======================================", version.getReadableName());
    }

    @Override
    public void afterSpecificTlsSupportGroupCheckStepExecution(TlsVersion version, TlsScannerReport report) {
        LOGGER.info("=====================================[End check for protocol: {}]=======================================", version.getReadableName());
    }

    @Override
    public void beforeSpecificTlsCheck(TlsVersionAndCipherContainer versionAndCipherContainer, URL securedUrl, TlsScannerReport report) {

    }

    @Override
    public void afterSpecificTlsCheck(TlsVersionAndCipherContainer versionAndCipherContainer, TlsRequestSupportDataContainer dataContainer, boolean madeByUsingMockClient, URL securedUrl, TlsScannerReport report) {
        switch (dataContainer.getFailReason()) {
            case NONE:
                LOGGER.info("Host supports {} with cipher {}", versionAndCipherContainer.getTlsVersion().getReadableName(), versionAndCipherContainer.getIanaCipherName());
                break;
            case NO_CLIENT_SUPPORT:
                LOGGER.error("Client doesn't support {} with cipher {}", versionAndCipherContainer.getTlsVersion().getReadableName(), versionAndCipherContainer.getIanaCipherName());
                break;
            case NO_SERVER_SUPPORT:
                LOGGER.warn("Host doesn't support {} with cipher {}", versionAndCipherContainer.getTlsVersion().getReadableName(), versionAndCipherContainer.getIanaCipherName());
                break;
            default:
            case UNKNOWN:
                LOGGER.error("Unknown error while checking support for {} with cipher {}: {}", versionAndCipherContainer.getTlsVersion().getReadableName(), versionAndCipherContainer.getIanaCipherName(), dataContainer.getAdditionalInformation().getMessage(), dataContainer.getAdditionalInformation());
                break;
        }
    }

    @Override
    public void beforeAttackVectorCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[Start attack vector checks]=======================================");
        LOGGER.info("Step 4 of 4: Doing possible attack vector checks");
    }

    @Override
    public void afterAttackVectorCheckStepExecution(TlsScannerReport report) {
        LOGGER.info("=====================================[End attack vector checks]=======================================");
    }

    @Override
    public void beforeAttackVectorCheck(AttackVector attackVector, TlsScannerReport report) {

    }

    @Override
    public void afterAttackVectorCheck(AttackVector attackVector, boolean isSecureAgainst, TlsScannerReport report) {
        switch (attackVector) {
            case HEARTBLEED:
                if (isSecureAgainst) {
                    LOGGER.info("Heartbeat extension: Disabled");
                } else {
                    //Do additional checks: OpenSSL version in server header, if given, to make sure that the version is attackable
                    boolean isHearthbleedOpenSslVersion = false;
                    boolean couldCheckifUsingopenSsl = false;

                    Object httpHeadersObj = report.getProtocolCheckMessages().get(HttpProtocolCommunicator.HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_LAST_HTTP_HEADERS);
                    if (httpHeadersObj instanceof Map) {
                        Map<String, String> httpHeaders = (Map<String, String>) httpHeadersObj;

                        String serverHeaderValue = null;
                        if (httpHeaders.containsKey("server")) {
                            serverHeaderValue = httpHeaders.get("server");
                        } else if (httpHeaders.containsKey("Server")) {
                            serverHeaderValue = httpHeaders.get("Server");
                        }

                        if (serverHeaderValue != null) {
                            if (serverHeaderValue.toLowerCase().contains("openssl")) {
                                couldCheckifUsingopenSsl = true;

                                String[] parts = serverHeaderValue.split(Pattern.quote(" "));
                                for (String part : parts) {
                                    if (part.toLowerCase().trim().startsWith("openssl")) {
                                        //Versions 1.0.1 to 1.0.1g are vulnerable to hearthbleed
                                        isHearthbleedOpenSslVersion =
                                                part.toLowerCase().equals("openssl/1.0.1")
                                                        || part.toLowerCase().matches("openssl/1\\.0\\.1[a-g]+");
                                    }
                                }
                            }
                        }
                    }

                    if (!couldCheckifUsingopenSsl) {
                        LOGGER.warn("Heartbeat extension: Enabled. Heartbleed may be possible (SSL-Library and version could not be checked)");
                    } else {
                        if (isHearthbleedOpenSslVersion) {
                            LOGGER.warn("Heartbeat extension: Enabled. !! Used OpenSSL-Version IS VULNERABLE to Hearthbleed !!");
                        } else {
                            LOGGER.info("Heartbeat extension: Enabled. Used OpenSSL-Version is not vulnerable to Hearthbleed");
                        }
                    }
                }
                break;
            case TRUNCATED_HMAC:
                if (isSecureAgainst) {
                    LOGGER.info("Truncated HMAC extension: Disabled");
                } else {
                    LOGGER.warn("Truncated HMAC extension: Enabled. This may ease forgery attacks");
                }
                break;
            case TLS_COMPRESSION:
                if (isSecureAgainst) {
                    LOGGER.info("TLS compression: Disabled");
                } else {
                    LOGGER.warn("TLS compression: Enabled. CRIME attack may be possible");
                }
                break;
            case ENCRYPT_THAN_HMAC:
                if (isSecureAgainst) {
                    LOGGER.warn("Encrypt-then-MAC extension: Enabled");
                } else {
                    LOGGER.warn("Encrypt-then-MAC extension: Disabled. Enabling this extension can \"buff out\" problems in encryption (See https://crypto.stackexchange.com/questions/202/should-we-mac-then-encrypt-or-encrypt-then-mac)");
                }
                break;
            case EXTENDED_MASTER_SECRET:
                if (isSecureAgainst) {
                    LOGGER.info("Extended master secret extension: Enabled");
                } else {
                    LOGGER.warn("Extended master secret extension: Disabled. 3SHAKE-Attack might be possible if the connection uses SSL-based client-authentication (e.g. PEAP, SASL)");
                }
                break;
            case PROTOCOL_COMPRESSION:
                if (isSecureAgainst) {
                    LOGGER.info("Protocol (e.g. HTTP) compression: Disabled");
                } else {
                    LOGGER.warn("Protocol (e.g. HTTP) compression: Enabled. CRIME attack may be possible");
                }
                break;
            case SESSION_RENEGOTIATION:
                if (isSecureAgainst) {
                    LOGGER.info("Client side [secured] session renegotiation: Disabled");
                } else {
                    LOGGER.warn("Client side [secured] session renegotiation: Enabled. May cause session splicing/hijacking/Demotion to insecure cipher (CVE-2009-3555)");
                }
                break;
            default:
                if (isSecureAgainst) {
                    LOGGER.info("Host is secured against " + attackVector.name());
                } else {
                    LOGGER.warn("Host is not secured against " + attackVector.name());
                }
                break;
        }
    }

    @Override
    public void onAttackVectorCheckError(AttackVector attackVector, Exception error, TlsScannerReport report) {
        String readableName;

        switch (attackVector) {
            case SESSION_RENEGOTIATION:
                readableName = "if client side [secured] session renegotiation is supported";
                break;
            case ENCRYPT_THAN_HMAC:
                readableName = "for encrypt than MAC extension";
                break;
            case TLS_COMPRESSION:
                readableName = "for tls compression support";
                break;
            case TRUNCATED_HMAC:
                readableName = "for truncated HMAC extension";
                break;
            case HEARTBLEED:
                readableName = "for hearthbleed vulnerability";
                break;
            case EXTENDED_MASTER_SECRET:
                readableName = "for extended master sexcret extension";
                break;
            case PROTOCOL_COMPRESSION:
                readableName = "for protocol compression support";
                break;
            default:
                readableName = attackVector.name();
                break;
        }

        LOGGER.error("Error while checking {}: " + error.getMessage(), readableName, error);
    }

    @Override
    public void onFatalError(Exception e, TlsScannerReport report) {
        LOGGER.fatal("Fatal error while running TLSScanner: " + e.getMessage(), e);
    }
}
