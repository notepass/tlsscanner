package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

//List of extensions: https://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xhtml#tls-extensiontype-values-1
public abstract class TlsExtension {
    protected short type = 0;
    protected short length = 0;

    public static TlsExtension parseFromByteRepresentation(byte[] data) {
        short extensionType = deserializeShort(data[0], data[1]);
        TlsExtension e;

        switch (extensionType) {
            case 15:
                e = new TlsHeartbeatExtension((byte) 0);
                break;
            case 22:
                e = new TlsEncryptThenMacExtension();
                break;
            case 0:
                e = new TlsServerNameExtension("");
                break;
            default:
                return null;
        }

        if (e != null) {
            e.deserialize(data);
        }

        return e;
    }

    public abstract byte[] serialize();
    public abstract void deserialize(byte[] data);
}
