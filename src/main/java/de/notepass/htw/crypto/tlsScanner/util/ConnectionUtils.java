package de.notepass.htw.crypto.tlsScanner.util;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * utility-class for URL-based connection stuff
 */
public class ConnectionUtils {
    private static final Map<String, Integer> wellKnownPorts = new HashMap<>();

    /**
     * Returns the well-known port for a given protocol (Based on IANA list)
     * @param protocol protocol to get the well known port for
     * @return
     */
    public static int getPortFor(String protocol) {
        return wellKnownPorts.get(protocol);
    }

    /**
     * Returns the well-known port for the protocol in the given URL (Based on IANA list)
     * @param url URL to get the well known port for
     * @return
     */
    public static int getPortFor(URL url) {
        return getPortFor(url.getProtocol());
    }

    /**
     * Returns the well-known port for the protocol in the given URL or the port set in the URL, if any (Based on IANA list)
     * @param url URL to get the well known or configured port for
     * @return
     */
    public static int getPortOrDefault(URL url) {
        if (url.getPort() > 0) {
            return url.getPort();
        }

        return getPortFor(url);
    }

    static {
        Properties properties = new Properties();
        try {
            properties.load(ConnectionUtils.class.getResourceAsStream("/defaultPorts.properties"));
        } catch (IOException e) {
            System.err.println("Could not load well known ports. Substituting with some data.");
            wellKnownPorts.put("http", 80);
            wellKnownPorts.put("https", 443);
        }
        for (Object o:properties.keySet()) {
            String s = o.toString();
            if (s != null && s.startsWith("iana.standard.port.")) {
                s = s.substring("iana.standard.port.".length());
                String value = properties.get(o).toString();
                if (value.contains("-")) {
                    int start = Integer.parseInt(value.substring(0, value.indexOf('-')));
                    wellKnownPorts.put(s, start);
                } else {
                    wellKnownPorts.put(s, Integer.parseInt(properties.getProperty(o.toString())));
                }
            }
        }
    }
}
