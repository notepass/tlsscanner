package de.notepass.htw.crypto.tlsScanner.reporting;

import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;

public class TlsRequestSupportDataContainer {
    private boolean supported;
    private TlsConnectionFailReason failReason;
    private Exception additionalInformation = null;

    public TlsRequestSupportDataContainer(boolean supported) {
        this.supported = supported;
        if (supported) {
            this.failReason = TlsConnectionFailReason.NONE;
        } else {
            this.failReason = TlsConnectionFailReason.UNKNOWN;
        }
    }

    public TlsRequestSupportDataContainer(boolean supported, TlsConnectionFailReason failReason) {
        this.supported = supported;
        this.failReason = failReason;
    }

    public TlsRequestSupportDataContainer(boolean supported, TlsConnectionFailReason failReason, Exception additionalInformation) {
        this.supported = supported;
        this.failReason = failReason;
        this.additionalInformation = additionalInformation;
    }

    public boolean isSupported() {
        return supported;
    }

    public TlsConnectionFailReason getFailReason() {
        return failReason;
    }

    public Exception getAdditionalInformation() {
        return additionalInformation;
    }
}
