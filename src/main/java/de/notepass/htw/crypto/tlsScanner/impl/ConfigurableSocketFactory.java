package de.notepass.htw.crypto.tlsScanner.impl;

import com.sun.xml.bind.v2.runtime.output.StAXExStreamWriterOutput;
import de.notepass.htw.crypto.tlsScanner.util.ConnectionUtils;
import de.notepass.htw.crypto.tlsScanner.util.SSLCertificateInfo;
import de.notepass.htw.crypto.tlsScanner.util.SSLConnection;
import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.asn1.*;

import javax.net.ssl.*;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.*;
import java.util.*;
import java.util.function.Function;

/**
 * Default implementation for connecting to a secured and unsecured target with advanced TLS/SSL configuration options
 */
public class ConfigurableSocketFactory extends BaseSocketFactoryImplementation {
    private static final Logger LOGGER = LogManager.getLogger(ConfigurableSocketFactory.class);

    private TlsVersion[] allowedProtocols = null;
    private String[] allowedCiphers = null;

    /**
     * Returns the contents of the subject alternative names extensions of the given certificate
     *
     * @param certificate The certificate to get the subject alternative names for
     * @return The contents of the subject alternative names extensions of the given certificate
     */
    private static List<String> getSubjectAlternativeNames(X509Certificate certificate) {
        List<String> identities = new ArrayList<String>();
        try {
            Collection<List<?>> altNames = certificate.getSubjectAlternativeNames();
            if (altNames == null)
                return Collections.emptyList();
            for (List item : altNames) {
                Integer type = (Integer) item.get(0);
                if (type == 0 || type == 2) {
                    try {
                        ASN1InputStream decoder = null;
                        if (item.toArray()[1] instanceof byte[])
                            decoder = new ASN1InputStream((byte[]) item.toArray()[1]);
                        else if (item.toArray()[1] instanceof String)
                            identities.add((String) item.toArray()[1]);
                        if (decoder == null) continue;
                        ASN1Object encoded = decoder.readObject();
                        encoded = (ASN1Object) ((DERSequence) encoded).getObjectAt(1);
                        encoded = ((DERTaggedObject) encoded).getObject();
                        encoded = ((DERTaggedObject) encoded).getObject();
                        String identity = ((DERUTF8String) encoded).getString();
                        identities.add(identity);
                    } catch (UnsupportedEncodingException e) {
                        LOGGER.error("Error decoding subjectAltName" + e.getLocalizedMessage(), e);
                    } catch (Exception e) {
                        LOGGER.error("Error decoding subjectAltName" + e.getLocalizedMessage(), e);
                    }
                } else {
                    LOGGER.warn("SubjectAltName of invalid type found: " + certificate);
                }
            }
        } catch (CertificateParsingException e) {
            LOGGER.error("Error parsing SubjectAltName in certificate: " + certificate + "\r\nerror:" + e.getLocalizedMessage(), e);
        }
        return identities;
    }

    @Override
    public SSLSocket createSecuredSocket(URL targetAddress) throws Exception {

        // Set up a custom trust manager for handling server trust and analyzing the certificate
        TrustManager[] customTrustManagers = new TrustManager[]{new X509TrustManager() {

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        }};

        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, customTrustManagers, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier((string, sslS) -> {
            // Deactivate certificate validation to be able to display errors, if present
            return true;
        });

        SSLSocket socket = (SSLSocket) HttpsURLConnection.getDefaultSSLSocketFactory().createSocket(targetAddress.getHost(), ConnectionUtils.getPortOrDefault(targetAddress));

        if (allowedProtocols != null) {
            socket.setEnabledProtocols(Arrays.stream(allowedProtocols).map((Function<TlsVersion, Object>) TlsVersion::getTechnicalName).toArray(String[]::new));
        }

        if (allowedCiphers != null) {
            socket.setEnabledCipherSuites(allowedCiphers);
        }

        socket.startHandshake();

        return socket;
    }

    @Override
    public SSLConnection createSecuredConnection(URL targetAddress) throws Exception {

        final List<String>[] altNames = new List[]{new ArrayList<>()};
        final Date[] dateList = new Date[2];
        final Boolean[] validValues = new Boolean[]{null, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE};


        // Set up a custom trust manager for handling server trust and analyzing the certificate
        TrustManager[] customTrustManagers = new TrustManager[]{new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                try {
                    altNames[0] = getSubjectAlternativeNames(certs[0]);
                    validValues[1] = altNames[0].contains(targetAddress.getHost()) ||
                            certs[0].getSubjectDN().getName() == "CN=" + targetAddress.getHost();

                    dateList[0] = certs[0].getNotBefore();
                    dateList[1] = certs[0].getNotAfter();

                    // Check certificate for validity on default trust manager
                    // Get TrustManagerFactory which can load the default certificate key store
                    TrustManagerFactory trustManagerFactory =
                            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                    trustManagerFactory.init((KeyStore) null);

                    TrustManager tms[] = trustManagerFactory.getTrustManagers();

                    X509TrustManager tm = null;

                    // Iterate over the returned trust managers, look for an instance of X509TrustManager.
                    // If found, use that as our "default" trust manager.
                    for (int i = 0; i < tms.length; i++) {
                        if (tms[i] instanceof X509TrustManager) {
                            tm = (X509TrustManager) tms[i];
                            break;
                        }
                    }

                    if (tm == null) {
                        // Failed to resolve default trust manager
                        validValues[0] = null;
                        LOGGER.info("Failed to retrieve default trust manager. Could not validate certificate chain.");
                    } else {
                        try {
                            // Redirect call to default trust manager
                            tm.checkServerTrusted(certs, authType);
                            validValues[0] = Boolean.TRUE;
                        } catch (CertificateException cex) {
                            LOGGER.info("Certificate chain is invalid: " + cex.getMessage());
                            validValues[0] = Boolean.FALSE;
                        }
                    }

                    // Check certificate itself for validity
                    try {
                        certs[0].checkValidity();
                    } catch (CertificateNotYetValidException cnyve) {
                        validValues[2] = Boolean.TRUE;
                    } catch (CertificateExpiredException cee) {
                        validValues[3] = Boolean.TRUE;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }};

        SSLContext sc = SSLContext.getInstance("TLS");

        sc.init(null, customTrustManagers, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier((string, sslS) -> {
            // Deactivate certificate validation to be able to display errors, if present
            return true;
        });

        SSLSocket socket = (SSLSocket) HttpsURLConnection.getDefaultSSLSocketFactory().createSocket(targetAddress.getHost(), ConnectionUtils.getPortOrDefault(targetAddress));

        if (allowedProtocols != null) {
            socket.setEnabledProtocols(Arrays.stream(allowedProtocols).map((Function<TlsVersion, Object>) TlsVersion::getTechnicalName).toArray(String[]::new));
        }

        if (allowedCiphers != null) {
            socket.setEnabledCipherSuites(allowedCiphers);
        }

        socket.startHandshake();

        return new SSLConnection(socket, new SSLCertificateInfo(altNames[0], dateList[0], dateList[1], validValues[0], validValues[2], validValues[3], validValues[1]));
    }

    @Override
    public TlsVersion[] getAllowedProtocols() {
        return allowedProtocols;
    }

    @Override
    public void setAllowedProtocols(TlsVersion[] allowedProtocols) {
        this.allowedProtocols = allowedProtocols;
    }

    @Override
    public String[] getAllowedCiphers() {
        return allowedCiphers;
    }

    @Override
    public void setAllowedCiphers(String[] allowedCiphers) {
        this.allowedCiphers = allowedCiphers;
    }

    @Override
    public void resetConfiguration() {
        this.allowedCiphers = null;
        this.allowedProtocols = null;
    }
}
