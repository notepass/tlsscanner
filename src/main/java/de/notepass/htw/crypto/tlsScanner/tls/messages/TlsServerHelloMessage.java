package de.notepass.htw.crypto.tlsScanner.tls.messages;

import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtension;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls2.Tls12ServerHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls3.Tls13ServerHelloMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.deserializeShort;

//TODO: Maybe put TLS record layer stuff in the tlsmessage class?

/**
 * Base class for TLS server hello messages containing logic for the TLS record layer
 */
public abstract class TlsServerHelloMessage implements TlsHelloMessage {
    private static final Logger LOGGER = LogManager.getLogger(TlsServerHelloMessage.class);

    /**
     * Parses a TLS server hello messages with an intact record layer
     *
     * @param is
     * @return
     * @throws UnknownContentTypeException
     * @throws TlsAlertException
     * @throws IOException
     */
    public static TlsServerHelloMessage parse(InputStream is) throws UnknownContentTypeException, TlsAlertException, IOException {
        //Pass 1: If no data is available wait for 100ms
        if (is.available() < 5) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // Ignore
            }

        }

        //Pass 2: If no data is available wait for 1s
        if (is.available() < 5) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Ignore
            }
        }

        //Pass 3: Give up
        if (is.available() < 5) {
            throw new IOException("TLS response error: Server didn't send ServerHello after 1.1 seconds. Aborting!");
        }

        byte[] recordLayer = new byte[5];
        is.read(recordLayer);
        byte contentType = recordLayer[0];
        short version = deserializeShort(recordLayer[1], recordLayer[2]);
        short remainingLength = deserializeShort(recordLayer[3], recordLayer[4]);

        if (contentType == 21) {
            throw new TlsAlertException();
        } else if (contentType != 22) {
            throw new UnknownContentTypeException("Unknown content type: " + contentType);
        }

        byte[] handshakeData = new byte[remainingLength];
        is.read(handshakeData);

        if (version == (short) 0x0303) {
            return Tls12ServerHelloMessage.parse(handshakeData);
        } else if (version == (short) 0x0304) {
            return Tls13ServerHelloMessage.parse(handshakeData);
        } else {
            return null;
        }
    }

    /**
     * Returns the handshake type as defined by RFC-5246<br>
     * For a server hello this should always be 2
     *
     * @return 2, hopefully
     */
    public abstract byte getHandshakeType();

    /**
     * Returns the TLS version as defined in the according RFCs (for TLSv1.2 e.g. <a href="https://tools.ietf.org/html/rfc5246">tools.ietf.org/html/rfc5246</a>)<br>
     * Values:
     * <ul>
     * <li>TLSv1.0: 0x0301</li>
     * <li>TLSv1.1: 0x0302</li>
     * <li>TLSv1.2: 0x0303</li>
     * <li>TLSv1.3: 0x0304</li>
     * </ul>
     *
     * @return
     */
    public abstract short getTlsVersion();

    /**
     * Should be a timestamp of the server, doesn't has to be, and normally isn't
     *
     * @return
     */
    public abstract int getTimestamp();

    /**
     * Random data
     *
     * @return
     */
    public abstract byte[] getRandom();

    /**
     * Session id for the TLS connection (for the server to access the ciphers and co)
     *
     * @return
     */
    public abstract byte[] getSessionId();

    /**
     * The cipher suite the server send and which is to use<br>
     * Send as raw values, see <a href="https://www.iana.org/assignments/tls-parameters/tls-parameters.xml#tls-parameters-4">IANA TLS parameter list</a> for lookup list
     * or use {@link org.bouncycastle.tls.CipherSuite}
     *
     * @return
     */
    public abstract short getCipherSuite();

    /**
     * Returns the compression method to use. There are only 2 defined: 0 = No compression, 1 = DEFLATE compression
     *
     * @return
     */
    public abstract byte getCompressionMethod();

    /**
     * Returns the extensions we send and the server supports (In theory a server can also return unsupported extensions, but this is rarely done)
     *
     * @return
     */
    public abstract TlsExtension[] getExtensions();
}
