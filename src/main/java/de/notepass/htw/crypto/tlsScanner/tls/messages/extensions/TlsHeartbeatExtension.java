package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;
import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

/**
 * See: <a href="https://tools.ietf.org/html/rfc6520">RFC-6520</a>
 */
public class TlsHeartbeatExtension extends TlsExtension {
    public static final byte HEARTBEAT_MODE_ONLY_CLIENT_REQUEST = 1;
    public static final byte HEARTBEAT_MODE_SERVER_AND_CLIENT_REQUEST = 2;

    private byte heartbeatMode; //1 = only client can send heartbeat requests; 2 = Server can send request and client answers

    public TlsHeartbeatExtension(byte heartbeatMode) {
        this.type = 15;
        this.heartbeatMode = heartbeatMode;
        this.length = (short) sizeof(heartbeatMode);
    }

    @Override
    public byte[] serialize() {
        byte[] data = new byte[2 + 2 + 1];

        int pos = append(data, type, 0);
        pos = append(data, length, pos);
        pos = append(data, heartbeatMode, pos);

        return data;
    }

    @Override
    public void deserialize(byte[] data) {
        type = deserializeShort(data[0], data[1]);
        short length = deserializeShort(data[2], data[3]);
        if (length > 0) {
            heartbeatMode = data[4];
        }
    }
}
