package de.notepass.htw.crypto.tlsScanner.tls.messages.tls2;

import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsServerHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.UnknownContentTypeException;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtension;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

import java.util.ArrayList;
import java.util.List;

//See https://tools.ietf.org/html/rfc5246
public class Tls12ServerHelloMessage extends TlsServerHelloMessage {
    // ======== START Server Hello Message ========
    /**
     * Content layer - type of handshake - 2 for server hello
     */
    private byte handshakeType = 0;

    /**
     * @see Tls12ClientHelloMessage#tlsVersion
     */
    private short tlsVersion = 0; //0x0303 = TLSv1.2

    /**
     * @see Tls12ClientHelloMessage#timestamp
     */
    private int timestamp =0;

    /**
     * @see Tls12ClientHelloMessage#random
     */
    private byte[] random = new byte[28]; //28 bytes of random data

    /**
     * @see Tls12ClientHelloMessage#sessionId
     */
    private byte[] sessionId = new byte[0]; //Up to 32 bytes

    /**
     * Content layer - cipher to use for communication
     */
    private short cipherSuite = 0;

    /**
     * Content layer - compression method to use for communication (0=no compression, 1=DEFLATE compression)
     */
    private byte compressionMethod = 0; //0 to 255, 0 = No compression, 1 = DEFLATE
    //private short[] extensions = new short[0];

    /**
     * Content layer - Extensions supported by server
     */
    private TlsExtension[] extensions = new TlsExtension[0];
    // ======== END Server Hello Message ========

    public static Tls12ServerHelloMessage parse(byte[] handshakeData) throws UnknownContentTypeException {
        byte handshakeType = handshakeData[0];

        if (handshakeType != 2) {
            throw new UnknownContentTypeException("Unknown handshake type: "+handshakeType);
        }

        int remainingLength = deserializeInteger((byte) 0, handshakeData[1], handshakeData[2], handshakeData[3]);
        short tlsVersion = deserializeShort(handshakeData[4], handshakeData[5]);
        int timestamp = deserializeInteger(handshakeData[6], handshakeData[7], handshakeData[8], handshakeData[9]);
        byte[] randomData = new byte[28];
        System.arraycopy(handshakeData, 10, randomData, 0, randomData.length);
        byte sessionIdLength = handshakeData[38];
        byte[] sessionId = new byte[sessionIdLength];
        int dynamicPos = 39;
        for (int i=0;i<sessionId.length;i++) {
            sessionId[i] = handshakeData[i+dynamicPos];
        }
        dynamicPos += sessionIdLength;

        short cipherSuite = deserializeShort(handshakeData[dynamicPos], handshakeData[dynamicPos+1]);
        dynamicPos += 2;

        byte compressionMethod = handshakeData[dynamicPos];
        dynamicPos++;

        List<TlsExtension> extensions = new ArrayList<>(20);

        if (dynamicPos < handshakeData.length) { //Only parse extension section if it exists
            short overallExtensionSize = deserializeShort(handshakeData[dynamicPos], handshakeData[dynamicPos + 1]);
            dynamicPos += 2;

            byte[] allExtensionData = new byte[overallExtensionSize];
            System.arraycopy(handshakeData, dynamicPos, allExtensionData, 0, overallExtensionSize);

            int currentPos = 0;
            while (currentPos + 3 < allExtensionData.length) { // +2 => Needs to read at least 3 indexes over the current pos
                short size = deserializeShort(allExtensionData[currentPos + 2], allExtensionData[currentPos + 3]);
                byte[] extensionData = new byte[2 + 2 + size]; //2 = size of type info before length; and 2 for length info
                System.arraycopy(allExtensionData, currentPos, extensionData, 0, extensionData.length);
                currentPos += extensionData.length;
                extensions.add(TlsExtension.parseFromByteRepresentation(extensionData));
            }
        }


        Tls12ServerHelloMessage msg = new Tls12ServerHelloMessage();
        msg.cipherSuite = cipherSuite;
        msg.compressionMethod = compressionMethod;
        msg.handshakeType = handshakeType;
        msg.sessionId = sessionId;
        msg.timestamp = timestamp;
        msg.tlsVersion = tlsVersion;
        msg.random = randomData;
        msg.extensions = extensions.toArray(new TlsExtension[0]);

        return msg;
    }

    /**
     * @see #handshakeType
     * @return
     */
    public byte getHandshakeType() {
        return handshakeType;
    }

    /**
     * @see #tlsVersion
     * @return
     */
    public short getTlsVersion() {
        return tlsVersion;
    }

    /**
     * @see #timestamp
     * @return
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * @see #random
     * @return
     */
    public byte[] getRandom() {
        return random;
    }

    /**
     * @see #sessionId
     * @return
     */
    public byte[] getSessionId() {
        return sessionId;
    }

    /**
     * @see #cipherSuite
     * @return
     */
    public short getCipherSuite() {
        return cipherSuite;
    }

    /**
     * @see #compressionMethod
     * @return
     */
    public byte getCompressionMethod() {
        return compressionMethod;
    }

    /**
     * @see #extensions
     * @return
     */
    public TlsExtension[] getExtensions() {
        return extensions;
    }

    @Override
    public byte[] serialize() {
        return new byte[0];
    }
}
