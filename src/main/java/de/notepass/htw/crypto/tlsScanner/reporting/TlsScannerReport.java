package de.notepass.htw.crypto.tlsScanner.reporting;

import de.notepass.htw.crypto.tlsScanner.autogen.schema.cipherCatalog.CipherCatalog;
import de.notepass.htw.crypto.tlsScanner.util.SSLCertificateInfo;
import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;

import java.util.*;

public class TlsScannerReport {
    private String plainConnectionUrl = null;
    private String securedConnectionUrl = null;

    private SSLCertificateInfo certificateInfo = null;

    private Map<TlsVersion, TlsRequestSupportDataContainer> genericTlsVersionSupport = new HashMap<>();
    private Map<TlsVersionAndCipherContainer, TlsRequestSupportDataContainer> cipherSupport = new HashMap<>();

    private Map<AttackVector, Boolean> isSecureAgainst = new HashMap<>();
    private Map<AttackVector, Exception> attackVectorCheckErrors = new HashMap<>();

    private CipherCatalog scannerConfiguration = null;

    private Map<String, Object> protocolCheckMessages = new HashMap<>();

    public String getPlainConnectionUrl() {
        return plainConnectionUrl;
    }

    public void setPlainConnectionUrl(String plainConnectionUrl) {
        this.plainConnectionUrl = plainConnectionUrl;
    }

    public String getSecuredConnectionUrl() {
        return securedConnectionUrl;
    }

    public void setSecuredConnectionUrl(String securedConnectionUrl) {
        this.securedConnectionUrl = securedConnectionUrl;
    }

    public SSLCertificateInfo getCertificateInfo() {
        return certificateInfo;
    }

    public void setCertificateInfo(SSLCertificateInfo certificateInfo) {
        this.certificateInfo = certificateInfo;
    }

    public Map<TlsVersion, TlsRequestSupportDataContainer> getGenericTlsVersionSupport() {
        return genericTlsVersionSupport;
    }

    public Map<TlsVersionAndCipherContainer, TlsRequestSupportDataContainer> getCipherSupport() {
        return cipherSupport;
    }

    public Map<AttackVector, Boolean> getIsSecureAgainst() {
        return isSecureAgainst;
    }

    public CipherCatalog getScannerConfiguration() {
        return scannerConfiguration;
    }

    public void setScannerConfiguration(CipherCatalog scannerConfiguration) {
        this.scannerConfiguration = scannerConfiguration;
    }

    public Map<String, Object> getProtocolCheckMessages() {
        return protocolCheckMessages;
    }

    public void addGenericSupportEntry(TlsVersion version, TlsRequestSupportDataContainer dataContainer) {
        genericTlsVersionSupport.put(version, dataContainer);
    }

    public void addSpecificSupportEntry(TlsVersionAndCipherContainer versionAndCipherContainer, TlsRequestSupportDataContainer dataContainer) {
        cipherSupport.put(versionAndCipherContainer, dataContainer);
    }

    public void addAttackVectorCheckResult(AttackVector vector, boolean isSecureAgainst) {
        this.isSecureAgainst.put(vector, isSecureAgainst);
    }

    public Map<AttackVector, Exception> getAttackVectorCheckErrors() {
        return attackVectorCheckErrors;
    }

    public void addAttackVectorCheckError(AttackVector vector, Exception cause) {
        attackVectorCheckErrors.put(vector, cause);
    }
}
