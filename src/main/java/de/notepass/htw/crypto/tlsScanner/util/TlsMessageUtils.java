package de.notepass.htw.crypto.tlsScanner.util;

/**
 * utility methods for plugging together TLS messages (b/c sometimes you have to implement some C-methods
 * for doing nice byte-stuff)
 */
public class TlsMessageUtils {
    /**
     * Appends an byte to an byte array (doesn't resize the array, so it has to have at least a size of startPos + data
     * type to append size in bytes)
     * @param target The byte array to append the byte to
     * @param data The byte to append
     * @param startPos The position on which to append the byte (First free position in input target array)
     * @return new next free position in the byte array
     */
    public static int append(byte[] target, byte data, int startPos) {
        return append(target, convertToByteArray(data), startPos);
    }

    /**
     * Appends a short to an byte array (doesn't resize the array, so it has to have at least a size of startPos
     * + data type to append size in bytes)
     * @param target The byte array to append the short to
     * @param data The short to append
     * @param startPos The position on which to start appending the byte array (First free position in input target array)
     * @return new next free position in the byte array
     */
    public static int append(byte[] target, short data, int startPos) {
        return append(target, convertToByteArray(data), startPos);
    }

    /**
     * Appends an int to an byte array (doesn't resize the array, so it has to have at least a size of startPos
     * + data type to append size in bytes)
     * @param target The byte array to append the int to
     * @param data The int to append
     * @param startPos The position on which to start appending the int (First free position in input target array)
     * @return new next free position in the byte array
     */
    public static int append(byte[] target, int data, int startPos) {
        return append(target, convertToByteArray(data), startPos);
    }

    /**
     * Appends a flattened two-dimensional byte array to an byte array (doesn't resize the array, so it has to have at least a size of startPos
     * + data type to append size in bytes)
     * @param target The byte array to append the two-dimensional byte array to
     * @param data The two-dimensional byte array to append
     * @param startPos The position on which to start appending the two-dimensional byte array (First free position in input target array)
     * @return new next free position in the byte array
     */
    public static int append(byte[] target, byte[][] data, int startPos) {
        for (byte[] b:data) {
            startPos = append(target, b, startPos);
        }

        return startPos;
    }

    /**
     * Appends an byte array to an byte array (doesn't resize the array, so it has to have at least a size of startPos
     * + data type to append size in bytes)
     * @param target The byte array to append the byte array to
     * @param data The byte array to append
     * @param startPos The position on which to start appending the byte array (First free position in input target array)
     * @return new next free position in the byte array
     */
    public static int append(byte[] target, byte[] data, int startPos) {
        System.arraycopy(data, 0, target, startPos, data.length);

        return startPos + sizeof(data);
    }

    /**
     * Appends a short array to an byte array (doesn't resize the array, so it has to have at least a size of startPos
     * + data type to append size in bytes)
     * @param target The byte array to append the short array to
     * @param data The short array to append
     * @param startPos The position on which to start appending the short array (First free position in input target array)
     * @return new next free position in the byte array
     */
    public static int append(byte[] target, short[] data, int startPos) {
        for (int i = 0; i<data.length; i++) {
            startPos = append(target, convertToByteArray(data[i]), startPos);
        }

        return startPos;
    }

    /**
     * Converts a given byte into a byte array
     * @param data Data to convert into a byte array
     * @return Data as byte array
     */
    public static byte[] convertToByteArray(byte data) {
        return new byte[]{data};
    }

    /**
     * Converts a given short into a byte array
     * @param data Data to convert into a byte array
     * @return Data as byte array
     */
    public static byte[] convertToByteArray(short data) {
        byte[] result = new byte[sizeof(data)];

        for (int i = 0; i < sizeof(data); i++) {
            byte part = (byte) (data >> i * 8);

            //Write array in reversed order to not mess up most and less significant byte
            result[result.length - i - 1] = part;
        }

        return result;
    }

    /**
     * Converts a given int into a byte array
     * @param data Data to convert into a byte array
     * @return Data as byte array
     */
    public static byte[] convertToByteArray(int data) {
        byte[] result = new byte[sizeof(data)];

        for (int i = 0; i < sizeof(data); i++) {
            byte part = (byte) (data >> i * 8);

            //Write array in reversed order to not mess up most and less significant byte
            result[result.length - i - 1] = part;
        }

        return result;
    }

    /**
     * Converts a given byte array into a byte array<br>
     * (Yes, this only makes sense for syntax / readability)
     * @param data Data to convert into a byte array
     * @return Data as byte array
     */
    public static byte[] convertToByteArray(byte[] data) {
        return data;
    }

    /**
     * Return the size of an integer in bytes (= 4)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(int a) {
        return 4;
    }

    /**
     * Return the size of an short in bytes (= 2)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(short a) {
        return 2;
    }

    /**
     * Return the size of an byte in bytes (= 1)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(byte a) {
        return 1;
    }

    /**
     * Return the size of an integer array in bytes (= 4 * array length)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(int[] a) {
        return 4 * a.length;
    }

    /**
     * Return the size of an short array in bytes (= 2 * array length)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(short[] a) {
        return 2 * a.length;
    }

    /**
     * Return the size of an byte array in bytes (= array length)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(byte[] a) {
        return a.length;
    }

    /**
     * Return the size of an two dimensional byte array in bytes (= addition of the length of all second dimension arrays)
     * @param a Data type for which to get the byte size
     * @return Byte size of given data type
     */
    public static int sizeof(byte[][] a) {
        int size = 0;
        for (byte[] b:a) {
            size += b.length;
        }

        return size;
    }

    /**
     * Creates a short from two given bytes
     * @param mostSignificant Higher byte
     * @param leastSignificant Lower byte
     * @return corresponding, signed, short (b/c java only supports signed values)
     */
    public static short deserializeShort(byte mostSignificant, byte leastSignificant) {
        return (short) ((mostSignificant << 8) | (leastSignificant));
    }

    /**
     * Creates a int from four given bytes
     * @param mostSignificant Higher byte
     * @param byte2 next lower byte
     * @param byte3 next lower byte
     * @param leastSignificant Lowest byte
     * @return corresponding, signed, int (b/c java only supports signed values)
     */
    public static short deserializeInteger(byte mostSignificant, byte byte2, byte byte3, byte leastSignificant) {
        return (short) ((mostSignificant << 24) | (byte2 << 16) | (byte3 << 8) | (leastSignificant));
    }
}
