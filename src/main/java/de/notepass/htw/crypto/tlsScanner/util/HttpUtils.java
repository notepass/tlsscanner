package de.notepass.htw.crypto.tlsScanner.util;

import de.notepass.htw.crypto.tlsScanner.impl.ConfigurableSocketFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Utility class to work with the HTTP protocol (because I didn't want to introduce an external library for reading
 * HTTP-Headers)
 */
public class HttpUtils {
    private static final Logger LOGGER = LogManager.getLogger(HttpUtils.class);
    private static ConfigurableSocketFactory socketProducer = new ConfigurableSocketFactory();

    /**
     * Extremely basic HTTP header parser. Doesn't verify anything or does smart stuff
     *
     * @param headers
     * @return
     */
    public static Map<String, String> parseHeaders(String headers) {
        Map<String, String> parsedHeaders = new HashMap<>();

        for (String line : headers.split(System.lineSeparator())) {
            if (line.startsWith("HTTP/")) {
                String httpCode = line.substring(line.indexOf(' '));
                String httpMessage = httpCode.substring(httpCode.indexOf(' '));
                httpCode = httpCode.substring(0, httpCode.indexOf(' '));

                parsedHeaders.put("httpCode", httpCode.trim());
                parsedHeaders.put("httpMessage", httpMessage.trim());
            }

            if (line.contains(": ")) {
                String[] parts = line.split(Pattern.quote(": "));
                if (parts.length == 2) {
                    parsedHeaders.put(parts[0].trim(), parts[1].trim());
                }
            }
        }

        return parsedHeaders;
    }

    /**
     * Sends a new request to the server for any redirection code returned to resolve the redirection chain.
     * Will stop if a HTTP 200 is recived or the protocol is changed (If configured so)
     * @param baseUrl URL to start the resolution from
     * @param stopOnProtocolChange If set to true, the resolution will stop if the redirection target protocoll is different
     *                             from the protocol given in the baseUrl.<br>
     *                             If set to false the function will return only if a HTTP 200 was returned
     * @return Last non-redirecting URL. If stopOnProtocolChange is set to true, this may still be a redirecting URL, but
     * the target of that redirection would mean a protocol change
     * @throws Exception On communication / response parsing error
     */
    public static URL resolveRedirectionChain(URL baseUrl, boolean stopOnProtocolChange) throws Exception {
        String oriProto = baseUrl.getProtocol();
        String locationHeaderValue;
        URL locationHeaderUrl = baseUrl;
        Map<String, String> headers;
        //Socket socket = null;
        URL previousLocationHeaderUrl;

        do {
            previousLocationHeaderUrl = locationHeaderUrl;
            try (Socket socket = (locationHeaderUrl.getProtocol().equals("https") ? socketProducer.createSecuredSocket(locationHeaderUrl) : socketProducer.createUnsecuredSocket(locationHeaderUrl)))
            {
                headers = doHttpGetRequest(socket, locationHeaderUrl);
                locationHeaderValue = headers.get("Location");
                locationHeaderUrl = null;

                if (locationHeaderValue != null) {
                    locationHeaderUrl = URI.create(locationHeaderValue).toURL();
                }

                LOGGER.debug(previousLocationHeaderUrl + " -> " + locationHeaderUrl);
            } catch (Exception e) {
                throw e;
            }
        } while ((!previousLocationHeaderUrl.equals(locationHeaderUrl) /* prevents infinite redirection loop */) && locationHeaderValue != null && (!stopOnProtocolChange || oriProto.equals(locationHeaderUrl.getProtocol())));

        return previousLocationHeaderUrl;
    }

    /**
     * Does a simple HTTP GET request and parses the resulting headers from the server into a map to access
     * @param socket Already connected socket to send the request over
     * @param target target-URL (used for setting the host-header)
     * @return
     * @throws IOException
     */
    public static Map<String, String> doHttpGetRequest(Socket socket, URL target) throws IOException {
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            String httpRequest = "GET / HTTP/1.1" + System.lineSeparator() +
                    "Host: " + target.getHost() + System.lineSeparator() +
                    "User-Agent: TLS-Scanner" + System.lineSeparator() + System.lineSeparator();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("=====================================================");
                LOGGER.debug("Sending HTTP request to " + target + ": ");
                for (String part : httpRequest.split(System.lineSeparator())) {
                    if (!part.trim().isEmpty()) {
                        LOGGER.debug(part);
                    }
                }
                LOGGER.debug("=====================================================");
            }

            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
            out.print(httpRequest);
            out.flush();

            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            LOGGER.debug("=====================================================");
            LOGGER.debug("Got HTTP response:");
            String inputLine;
            StringBuilder headerBuilder = new StringBuilder();
            boolean isHeader = true;
            while ((inputLine = in.readLine()) != null) {
                LOGGER.debug(inputLine);
                if (inputLine.isEmpty()) {
                    isHeader = false;
                    break; //Do not read the remaining part of the response, as it is only needed for debug logging from here on
                }

                if (isHeader) {
                    headerBuilder.append(inputLine).append(System.lineSeparator());
                }
            }
            LOGGER.debug("=====================================================");

            return HttpUtils.parseHeaders(headerBuilder.toString());
        } finally {
            try {
                if (in != null) in.close();
            } catch (Exception ignored) {

            }

            try {
                if (out != null) out.close();
            } catch (Exception ignored) {

            }
        }
    }
}
