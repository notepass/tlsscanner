package de.notepass.htw.crypto.tlsScanner.util;

import java.util.Date;
import java.util.List;

/**
 * Container for Certificate check results
 */
public class SSLCertificateInfo {

    private List<String> subjectAlternativeNames;
    private Date notBefore;
    private Date notAfter;
    private Boolean isCertValid;
    private boolean isNotYetValid;
    private boolean isNotValidAnymore;
    private boolean isSubjectValid;

    public SSLCertificateInfo(List<String> subjectAlternativeNames, Date notBefore, Date notAfter, Boolean isCertValid, boolean isNotYetValid, boolean isNotValidAnymore, boolean isSubjectValid) {
        this.subjectAlternativeNames = subjectAlternativeNames;
        this.notBefore = notBefore;
        this.notAfter = notAfter;
        this.isCertValid = isCertValid;
        this.isNotYetValid = isNotYetValid;
        this.isNotValidAnymore = isNotValidAnymore;
        this.isSubjectValid = isSubjectValid;
    }

    /**
     * Returns the values of the "subject alternative name" of the certificate
     * @return values of the "subject alternative name" of the certificate
     */
    public List<String> getSubjectAlternativeNames() {
        return subjectAlternativeNames;
    }

    /**
     * Returns whether the certificate is expired or not
     * @return true = certificate is valid; false = certificate is expired
     */
    public Boolean getIsCertValid() {
        return isCertValid;
    }

    /**
     * Returns the date the certificate is valid to
     * @return date the certificate is valid to
     */
    public Date getNotAfter() {
        return notAfter;
    }

    /**
     * Returns the date the certificate is valid from
     * @return date the certificate is valid from
     */
    public Date getNotBefore() {
        return notBefore;
    }

    /**
     * Returns whether the certificate is already valid
     * @return true = certificate is valid / false = certificate is invalid
     */
    public boolean getIsNotYetValid() {
        return isNotYetValid;
    }

    /**
     * Returns whether the certificate is still valid
     * @return true = certificate is still valid / false = certificate is expired
     */
    public boolean getIsNotValidAnymore() {
        return isNotValidAnymore;
    }

    /**
     * Returns whether the certificate is valid for the given host
     * @return
     */
    public boolean getIsSubjectValid() {
        return isSubjectValid;
    }
}
