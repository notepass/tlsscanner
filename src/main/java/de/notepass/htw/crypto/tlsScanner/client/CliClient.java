package de.notepass.htw.crypto.tlsScanner.client;

import de.notepass.htw.crypto.tlsScanner.api.InternalConfigFile;
import de.notepass.htw.crypto.tlsScanner.api.LoggingTlsScannerEventCallback;
import de.notepass.htw.crypto.tlsScanner.api.TlsScanner;
import de.notepass.htw.crypto.tlsScanner.api.TlsScannerFactory;
import de.notepass.htw.crypto.tlsScanner.reporting.TlsScannerReport;
import de.notepass.htw.crypto.tlsScanner.util.ConnectionUtils;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * CLI-based client which parses parameters to configure a tls-scanner and run it. It will use the logging callback to
 * ghenerate real-time console output
 */
public class CliClient {
    private static final Logger LOGGER = LogManager.getLogger(CliClient.class);

    public static void main(String[] args) throws Exception {
        Option hostOption = Option.builder()
                .argName("h")
                .longOpt("host")
                .desc("Host to scan in format http://<HOST>:<PORT> where HOST and PORT are required")
                .hasArg()
                .optionalArg(false)
                .type(String.class)
                .build();

        Option externalConfigFile = Option.builder()
                .argName("c")
                .longOpt("config-file")
                .desc("External configuration file to load (Incompatible with i/internal-config-file)")
                .hasArg()
                .type(String.class)
                .build();

        List<String> internalConfigFiles = Arrays.stream(TlsScannerFactory.getInternalConfigFiles()).map(InternalConfigFile::getLocation).collect(Collectors.toList());
        Option internalConfigFile = Option.builder()
                .argName("i")
                .longOpt("internal-config-file")
                .desc("Internal configuration file to load (Incompatible with c/config-file). Currently available: " + String.join(", ", internalConfigFiles))
                .hasArg()
                .type(String.class)
                .build();

        Option allowMockClient = Option.builder()
                .argName("m")
                .longOpt("allow-mock-client")
                .desc("Allows to send mocked client hello messages for client-unsupported ciphers/protocols. This will not emulate a handshake, bot no encrypted data can be send/received")
                .hasArg(true)
                .build();

        OptionGroup configFileGroup = new OptionGroup();
        configFileGroup.addOption(internalConfigFile);
        configFileGroup.addOption(externalConfigFile);
        configFileGroup.setRequired(true);

        Options options = new Options();
        options.addOption(hostOption);
        options.addOption(allowMockClient);
        options.addOptionGroup(configFileGroup);

        CommandLineParser parser = new DefaultParser();
        CommandLine line = null;

        boolean printHelp = false;

        try {
            line = parser.parse(options, args);
        } catch (Exception e) {
            System.out.println("Syntax error: " + e.getMessage());
            printHelp = true;
        }

        if (printHelp) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("tlsscanner", options);
            return;
        }

        if (line.hasOption("internal-config-file") && line.hasOption("config-file")) {
            System.out.println("internal-config-file and config-file are exclusive. Please only specify one!");
            return;
        }

        boolean allowCustomTlsImplementationHelloMessageForCipherTesting = false;
        if (line.hasOption("allow-mock-client")) {
            allowCustomTlsImplementationHelloMessageForCipherTesting = Boolean.parseBoolean(line.getOptionValue("allow-mock-client")) || line.getOptionValue("allow-mock-client") == null;
        }

        String targetHost = line.getOptionValue("host");
        InputStream cipherCatalogStream = null;
        if (line.hasOption("internal-config-file")) {
            cipherCatalogStream = CliClient.class.getResourceAsStream(line.getOptionValue("internal-config-file"));
        } else {
            cipherCatalogStream = new FileInputStream(line.getOptionValue("config-file"));
        }


        try {
            URL url = URI.create(targetHost).toURL();
            try (Socket s = new Socket(url.getHost(), ConnectionUtils.getPortOrDefault(url))) {
                if (!s.isConnected()) {
                    System.err.println("Could not connect to host \""+targetHost+"\". Reason: unknown. Aborting execution!");
                    return;
                }
            } catch (Exception e) {
                System.err.println("Error while trying to create test connection to host \""+targetHost+"\". Aborting execution! Reason: "+e.getClass()+": "+e.getMessage());
                e.printStackTrace();
                return;
            }
        } catch (Exception e) {
            System.err.println("Given host is invalid. Aborting execution! Reason: "+e.getClass()+": "+e.getMessage());
            e.printStackTrace();
            return;
        }

        LOGGER.info("TlsScanner - Starting up");

        TlsScanner scanner = TlsScannerFactory.createInstance(cipherCatalogStream, allowCustomTlsImplementationHelloMessageForCipherTesting);
        TlsScannerReport report = scanner.execute(new LoggingTlsScannerEventCallback(), targetHost);

        LOGGER.info("TlsScanner - Shutting down");
    }
}
