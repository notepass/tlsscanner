package de.notepass.htw.crypto.tlsScanner.impl;

import de.notepass.htw.crypto.tlsScanner.definition.IProtocolCommunicator;
import de.notepass.htw.crypto.tlsScanner.reporting.TlsScannerReport;
import de.notepass.htw.crypto.tlsScanner.util.HttpUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.SSLSocket;
import java.io.*;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;

/**
 * Implement HTTP protocol communications
 */
public class HttpProtocolCommunicator implements IProtocolCommunicator {
    private static final Logger LOGGER = LogManager.getLogger(HttpProtocolCommunicator.class);

    // long name bois
    public static final String HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_HTTP_TO_HTTPS_REDIRECT = "de.notepass.htw.crypto.tlsScanner.impl.HttpProtocolCommunicator#RedirectHttpToHttps";
    //public static final String HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_HTTP_TO_HTTPS_FILE_UNTOUCHED = "de.notepass.htw.crypto.tlsScanner.impl.HttpProtocolCommunicator#RedirectHttpToHttpsFileUntouched";
    public static final String HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_LAST_HTTP_HEADERS = "de.notepass.htw.crypto.tlsScanner.impl.HttpProtocolCommunicator#LastHttpdHeaders";

    @Override
    public boolean allowPlainCommunication() {
        return true;
    }

    @Override
    public URL getEffectivePlainUrl(String sourceUrl) throws MalformedURLException {
        try {
            return HttpUtils.resolveRedirectionChain(URI.create(sourceUrl).toURL(), true);
        } catch (IOException e) {
            LOGGER.error("Could not calculate effective plain URL. Falling back to given URL. Reason: " + e.getMessage(), e);
            return URI.create(sourceUrl).toURL();
        } catch (Exception e) {
            LOGGER.error("Could not create socket for calculating effective plain URL. Falling back to given URL. Reason:"+ e.getMessage(), e);
            return URI.create(sourceUrl).toURL();
        }
    }

    @Override
    public void checkOverPlainConnection(Socket plainSocket, URL plainUrl, TlsScannerReport report) {
        URL redirectTargetUrl;
        try {
            redirectTargetUrl = HttpUtils.resolveRedirectionChain(plainUrl, false);
        } catch (Exception e) {
            LOGGER.error("Could not check if HTTP-site redirects to HTTPS-site: "+e.getMessage(), e);
            return;
        }

        report.getProtocolCheckMessages().put(HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_HTTP_TO_HTTPS_REDIRECT, false);
        if (redirectTargetUrl != null) {
            if ("https".equalsIgnoreCase(redirectTargetUrl.getProtocol())) {
                report.getProtocolCheckMessages().put(HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_HTTP_TO_HTTPS_REDIRECT, true);
            }
        }
    }

    @Override
    public URL getEffectiveSecuredUrl(String sourceUrl, URL plainUrl) throws MalformedURLException {
        //TODO
        try {
            return HttpUtils.resolveRedirectionChain(new URL("https", plainUrl.getHost(), 443, plainUrl.getFile()), true);
        } catch (IOException e) {
            LOGGER.error("Could not calculate effective secured URL. Falling back to given URL. Reason: " + e.getMessage(), e);
            return URI.create(sourceUrl).toURL();
        } catch (Exception e) {
            LOGGER.error("Could not create socket for calculating effective secured URL. Falling back to given URL. Reason:"+ e.getMessage(), e);
            return URI.create(sourceUrl).toURL();
        }
    }

    @Override
    public void checkOverSecuredConnection(SSLSocket securedSocket, URL securedUrl, TlsScannerReport report) {
        //checkOverPlainConnection(securedSocket, securedUrl);
        try {
            report.getProtocolCheckMessages().put(HTTP_PROTOCOL_COMMUNICATOR_REPORT_KEY_LAST_HTTP_HEADERS, HttpUtils.doHttpGetRequest(securedSocket, securedUrl));
        } catch (IOException e) {
            LOGGER.warn("Could not fill HTTP header data cache for further analyses", e);
        }
    }
}
