package de.notepass.htw.crypto.tlsScanner.definition;

import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;

import javax.net.ssl.SSLSocket;
import java.net.Socket;
import java.net.URL;

//TODO: Throw custom exception

/**
 * The implementer of this interface implements a factory which can create unsecured and (D)TLS/SSL-secured sockets which
 * are already connected to a given URL
 */
public interface ISocketFactory {
    /**
     * Returns a (D)TLS (or SSL) secured socket in which a handshake already occurred
     *
     * @param targetAddress URL to which the socket should connect
     * @return A connected socket. The needed handshake for a connection has have to already taken place.
     * @throws Exception
     */
    public SSLSocket createSecuredSocket(URL targetAddress) throws Exception;


    /**
     * Returns a {@link SecureConnection} with a (D)TLS (or SSL) secured socket in which a handshake already occurred
     *
     * @param targetAddress URL to which the socket should connect
     * @return A {@link SecureConnection} with a connected socket. The needed handshake for a connection has have to already taken place.
     * @throws Exception
     */
    public SecureConnection createSecuredConnection(URL targetAddress) throws Exception;

    /**
     * Returns an unsecured socket connection to the given URL
     *
     * @param targetAddress URL to which the socket should connect
     * @return A connected socket without any data send
     * @throws Exception
     */
    public Socket createUnsecuredSocket(URL targetAddress) throws Exception;

    /**
     * Returns all protocols which can be used for a TLS/SSL connection<br>
     * (e.g. TLSv1.2, SSLv3)
     *
     * @return
     */
    public TlsVersion[] getAllowedProtocols();

    /**
     * Configures all protocols which can be used for a TLS/SSL connection<br>
     * (e.g. TLSv1.2, SSLv3)
     */
    public void setAllowedProtocols(TlsVersion[] allowedProtocols);

    /**
     * Returns the IANA names of the ciphers which are allowed for use in the secured connection (See <a href="https://www.iana.org/assignments/tls-parameters/tls-parameters.xml#tls-parameters-4">IANA TLS parameter list</a>)<br>
     * (example: TLS_DH_RSA_WITH_AES_256_CBC_SHA)
     *
     */
    public String[] getAllowedCiphers();

    /**
     * Sets the ciphers which are allowed for use in the secured connection. Please use the IANA names (See <a href="https://www.iana.org/assignments/tls-parameters/tls-parameters.xml#tls-parameters-4">IANA TLS parameter list</a>)<br>
     * (example: TLS_DH_RSA_WITH_AES_256_CBC_SHA)
     *
     * @param ianaCipherNames
     */
    public void setAllowedCiphers(String[] ianaCipherNames);

    /**
     * Resets the current configuration to the default one
     */
    public void resetConfiguration();

    /**
     * Returns a list of IANA cipher names. The list should contain all cipher names the underlying SSL-Implementation can handle
     * @return
     */
    public String[] getSupportedCipherSuites();
}
