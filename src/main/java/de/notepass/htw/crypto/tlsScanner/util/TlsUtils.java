package de.notepass.htw.crypto.tlsScanner.util;

import java.lang.reflect.Field;

/**
 * utility-methods to work with IANA cipher names
 */
public class TlsUtils {
    /**
     * Returns the 2-byte code for the given cipher, or -1 if no code can be calculated
     * @param cipher
     * @return
     */
    public static short getCipherCode(String cipher) {
        try {
            Field f = org.bouncycastle.tls.CipherSuite.class.getField(cipher.toUpperCase());
            if (f.getType() == int.class) {
                return (short) f.getInt(null);
            }

            return -1;
        } catch (Exception e) {
            return -1;
        }
    }
}
