/**
 * This package contains implementations for all extensions supported by the custom TLS client:
 * <ul>
 *     <li>{@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsEncryptThenMacExtension}</li>
 *     <li>{@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtendedMasterSecretExtension}</li>
 *     <li>{@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsHeartbeatExtension}</li>
 *     <li>{@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsServerNameExtension}</li>
 *     <li>{@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsSupportedVersionsExtension}</li>
 *     <li>{@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsTruncatedHmacExtension}</li>
 * </ul>
 */
package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;