package de.notepass.htw.crypto.tlsScanner.reporting.writer;

import de.notepass.htw.crypto.tlsScanner.reporting.TlsScannerReport;

import java.nio.file.Path;

public interface ReportTransformer {
    public byte[] transformReport(TlsScannerReport report);
}
