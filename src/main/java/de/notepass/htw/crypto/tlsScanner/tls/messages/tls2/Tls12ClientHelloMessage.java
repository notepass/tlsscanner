package de.notepass.htw.crypto.tlsScanner.tls.messages.tls2;

import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsClientHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtension;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * TLS client hello message as defined in <a href="https://tools.ietf.org/html/rfc5246">RFC-5246</a>
 */
public class Tls12ClientHelloMessage implements TlsClientHelloMessage {
    // ======== START "Record Layer" ========
    /**
     * TLS record layer - type of content - 22 for handshake
     */
    private byte contentType = 22; //22 = Handshake

    /**
     * TLS record layer - tls version - 0x0303 for TLSv1.2
     */
    private short tlsVersion = 0x0303; //0x0303 = TLSv1.2

    /**
     * TLS record layer - Overall size of the send package
     */
    private short totalLength = 0; //Size of the package to send
    // ======== END "Record Layer" ========

    // ======== START Client Hello Message ========
    /**
     * Content layer - type of handshake - 1 for client hello
     */
    private byte handshakeType = 1; //Client HELLO

    /**
     * Content layer - size of handshake
     */
    private byte[] handshakeLength = new byte[3]; //Size of the handshake portion of the package

    //TLS version again

    /**
     * Content layer - client timestamp (or other 4 random bytes)
     */
    private int timestamp = (int) (System.currentTimeMillis() / 1000L); //current time as unix timestamp, belongs to the random data section

    /**
     * Content layer - 25 bytes of random data
     */
    private byte[] random = new byte[28]; //28 bytes of random data

    /**
     * Content layer - length of the session id (in bytes)
     */
    private byte sessionIdLength = 0; //Empty, as this wont resume any session (Can be up to 32 bytes)

    /**
     * Content layer - session id
     */
    private byte[] sessionId = new byte[0]; //Up to 32 bytes

    /**
     * Content layer - size of the client supported cipher part (in bytes)
     */
    private short cipherSuitesLength = 0; //Number of bytes used in the session id

    /**
     * Content layer - list of supported ciphers
     */
    private short[] cipherSuites = new short[0];

    /**
     * Content layer - size of list of supported compression methods (in bytes)
     */
    private byte compressionMethodsLength = 0;

    /**
     * Content layer - supported compression methods - 0 for none, 1 for DEFLATE
     */
    private byte[] compressionMethods = new byte[]{0}; //0 to 255, 0 = No compression, 1 = DEFLATE

    /**
     * Content layer - length of extension part (in bytes)
     */
    private short extensionsLength = 0; //Number of bytes the extensions-array occupies
    //private short[] extensions = new short[0];

    /**
     * Content layer - supported extensions
     */
    private TlsExtension[] extensions = new TlsExtension[0];
    // ======== END Client Hello Message ========

    public Tls12ClientHelloMessage() {
        new SecureRandom().nextBytes(random);
        cipherSuites = new short[]{(short) 0xC030};
        //compressionMethods = new byte[]{1, 0};
    }

    @Override
    public byte[] serialize() {
        byte[][] serializedExtensions = new byte[extensions.length][];
        for (int i=0;i<extensions.length;i++) {
            serializedExtensions[i] = extensions[i].serialize();
        }

        //These fields have their own size counter as they are variable length.
        extensionsLength = (short) (sizeof(serializedExtensions));
        cipherSuitesLength = (short) (sizeof(cipherSuites));
        sessionIdLength = (byte) sizeof(sessionId);
        compressionMethodsLength = (byte) sizeof(compressionMethods);

        int helloMessageSize = sizeof(tlsVersion)
                + sizeof(timestamp)
                + sizeof(random)
                + sizeof(sessionIdLength)
                + sessionIdLength
                + sizeof(cipherSuitesLength)
                + cipherSuitesLength
                + sizeof(compressionMethodsLength)
                + compressionMethodsLength
                + sizeof(extensionsLength)
                + extensionsLength;

        totalLength = (short) (sizeof(handshakeType) + sizeof(handshakeLength) + helloMessageSize);

        byte[] helloMessageSizeBytes = new byte[sizeof(helloMessageSize)];

        //TODO: Generalize
        //for (int i=sizeof(helloMessageSize)-1;i>=0; i -= 1) {
        for (int i = 0; i < sizeof(helloMessageSize); i++) {
            byte part = (byte) (helloMessageSize >> i * 8);

            //Write array in reversed order to not mess up most and less significant byte
            helloMessageSizeBytes[helloMessageSizeBytes.length - i - 1] = part;
        }

        //Copy upper 3 bytes into handshakeLength
        System.arraycopy(helloMessageSizeBytes, helloMessageSizeBytes.length - sizeof(handshakeLength), handshakeLength, 0, sizeof(handshakeLength));

        //Add some "missing" sizes to the total length (As that only counts every field AFTER the length info)
        int completeMessageSize = totalLength + sizeof(tlsVersion) + sizeof(contentType) + sizeof(totalLength);

        byte[] message = new byte[completeMessageSize];
        int pos = append(message, contentType, 0);
        pos = append(message, tlsVersion, pos);
        pos = append(message, totalLength, pos);
        pos = append(message, handshakeType, pos);
        pos = append(message, handshakeLength, pos);
        pos = append(message, tlsVersion, pos);
        pos = append(message, timestamp, pos);
        pos = append(message, random, pos);
        pos = append(message, sessionIdLength, pos);
        pos = append(message, sessionId, pos);
        pos = append(message, cipherSuitesLength, pos);
        pos = append(message, cipherSuites, pos);
        pos = append(message, compressionMethodsLength, pos);
        pos = append(message, compressionMethods, pos);
        pos = append(message, extensionsLength, pos);
        pos = append(message, serializedExtensions, pos);

        return message;
    }

    /**
     * @see #timestamp
     * @return
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * @see #timestamp
     */
    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @see #random
     * @return
     */
    public byte[] getRandom() {
        return random;
    }

    /**
     * @see #random
     */
    public void setRandom(byte[] random) {
        this.random = random;
    }

    /**
     * @see #sessionId
     * @return
     */
    public byte[] getSessionId() {
        return sessionId;
    }

    /**
     * @see #sessionId
     */
    public void setSessionId(byte[] sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @see #cipherSuites
     * @return
     */
    public short[] getCipherSuites() {
        return cipherSuites;
    }

    /**
     * @see #cipherSuites
     */
    public void setCipherSuites(short[] cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

    /**
     * @see #compressionMethods
     */
    public byte[] getCompressionMethods() {
        return compressionMethods;
    }

    /**
     * @see #compressionMethods
     */
    public void setCompressionMethods(byte[] compressionMethods) {
        this.compressionMethods = compressionMethods;
    }

    /**
     * @see #extensions
     */
    public TlsExtension[] getExtensions() {
        return extensions;
    }

    /**
     * @see #extensions
     */
    public void setExtensions(TlsExtension[] extensions) {
        this.extensions = extensions;
    }

    @Override
    public void addExtension(TlsExtension... extensions) {
        List<TlsExtension> extensionList = new ArrayList<>(this.extensions.length + extensions.length);
        for (TlsExtension extension:this.extensions) {
            extensionList.add(extension);
        }

        for (TlsExtension extension:extensions) {
            extensionList.add(extension);
        }

        this.extensions = extensionList.toArray(new TlsExtension[0]);
    }

    @Override
    public void removeExtension(TlsExtension... extensions) {
        List<TlsExtension> extensionList = new ArrayList<>(this.extensions.length);
        for (TlsExtension extension:this.extensions) {
            extensionList.add(extension);
        }

        for (TlsExtension extension:extensions) {
            extensionList.remove(extension);
        }

        this.extensions = extensionList.toArray(new TlsExtension[0]);
    }

    /**
     * @see #contentType
     * @return
     */
    public byte getContentType() {
        return contentType;
    }

    /**
     * @see #tlsVersion
     * @return
     */
    public short getTlsVersion() {
        return tlsVersion;
    }

    /**
     * @see #totalLength
     * @return
     */
    public short getTotalLength() {
        return totalLength;
    }

    /**
     * @see #handshakeType
     * @return
     */
    public byte getHandshakeType() {
        return handshakeType;
    }

    /**
     * @see #handshakeLength
     * @return
     */
    public byte[] getHandshakeLength() {
        return handshakeLength;
    }

    /**
     * @see #sessionIdLength
     * @return
     */
    public byte getSessionIdLength() {
        return sessionIdLength;
    }

    /**
     * @see #cipherSuitesLength
     * @return
     */
    public short getCipherSuitesLength() {
        return cipherSuitesLength;
    }

    /**
     * @see #compressionMethodsLength
     * @return
     */
    public byte getCompressionMethodsLength() {
        return compressionMethodsLength;
    }

    /**
     * @see #extensionsLength
     * @return
     */
    public short getExtensionsLength() {
        return extensionsLength;
    }
}
