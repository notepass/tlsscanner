package de.notepass.htw.crypto.tlsScanner.reporting;

public enum AttackVector {
    SESSION_RENEGOTIATION,
    TLS_COMPRESSION,
    PROTOCOL_COMPRESSION,
    HEARTBLEED,
    TRUNCATED_HMAC,
    ENCRYPT_THAN_HMAC,
    EXTENDED_MASTER_SECRET
}
