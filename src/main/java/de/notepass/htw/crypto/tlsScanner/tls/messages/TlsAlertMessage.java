package de.notepass.htw.crypto.tlsScanner.tls.messages;

/**
 * Base interface for TLS alert messages (Currently not implemented, as alerts are handled as exceptions)
 */
public interface TlsAlertMessage extends TlsMessage {
}
