package de.notepass.htw.crypto.tlsScanner.tls.messages;

import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtension;
import de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils;

/**
 * Base interface for TLS client hello messages
 */
public interface TlsClientHelloMessage extends TlsHelloMessage {
    /**
     * Sets the cipher suites which the client will send as "supported"<br>
     * An entry consist of 2 bytes (=short) which indicate the AKS and Encryption Hash portions of the cipher.<br>
     * An exhaustive list of these cipher codes can be found at <a href="https://www.iana.org/assignments/tls-parameters/tls-parameters.xml#tls-parameters-4">IANA TLS parameter list</a>
     * (See column "Value". The short can be assembled via {@link TlsMessageUtils#deserializeShort(byte, byte)}.
     * Alternatively, the values in {@link org.bouncycastle.tls.CipherSuite} can be used. Or to convert labels to 2-byte codes the function {@link de.notepass.htw.crypto.tlsScanner.util.TlsUtils#getCipherCode(String)}}
     * can be used
     * @param ciphers
     */
    public void setCipherSuites(short[] ciphers);

    /**
     * Sets the extensions the client will transmit as supported. For a list of supported extensions please see {@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions}<br>
     * Please only use this if necessary, as it can break the TLSv1.3 client, as TLSv1.3 support is indicated via an extension. As an alternative use {@link #addExtension(TlsExtension...)} /  {@link #removeExtension(TlsExtension...)}
     * @param extensions
     */
    public void setExtensions(TlsExtension[] extensions);

    /**
     * Adds extensions the client will transmit as supported. For a list of supported extensions please see {@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions}
     * @param extensions
     */
    public void addExtension(TlsExtension ... extensions);

    /**
     * Removes extensions the client will transmit as supported. For a list of supported extensions please see {@link de.notepass.htw.crypto.tlsScanner.tls.messages.extensions}
     * @param extensions
     */
    public void removeExtension(TlsExtension ... extensions);

    /**
     * Sets the supported compression methods. There are onl 2 defined: 0 = No compression, 1 = DEFLATE compression
     * @param compressionMethods
     */
    public void setCompressionMethods(byte[] compressionMethods);
}
