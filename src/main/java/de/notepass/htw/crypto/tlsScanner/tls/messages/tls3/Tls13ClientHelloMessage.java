package de.notepass.htw.crypto.tlsScanner.tls.messages.tls3;

import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtension;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsSupportedVersionsExtension;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls2.Tls12ClientHelloMessage;

public class Tls13ClientHelloMessage extends Tls12ClientHelloMessage {
    public Tls13ClientHelloMessage() {
        super();

        TlsExtension[] extensions = getExtensions();
        TlsExtension[] newExtensions = new TlsExtension[extensions.length + 1];

        System.arraycopy(extensions, 0, newExtensions, 0, extensions.length);
        newExtensions[newExtensions.length - 1] = new TlsSupportedVersionsExtension((short) 0x0304); // 0x0304 = TlSv1.3

        setExtensions(newExtensions);
    }
}
