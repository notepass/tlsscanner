package de.notepass.htw.crypto.tlsScanner.impl;

import de.notepass.htw.crypto.tlsScanner.definition.ISocketFactory;
import de.notepass.htw.crypto.tlsScanner.util.ConnectionUtils;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * A base implementation of the socket factory interface which implements unsecured socket connections
 */
public abstract class BaseSocketFactoryImplementation implements ISocketFactory {
    @Override
    public Socket createUnsecuredSocket(URL targetAddress) throws Exception {
        Socket s;

        if (targetAddress.getPort() > 0) {
            s = new Socket(targetAddress.getHost(), targetAddress.getPort());
        } else {
            s = new Socket(targetAddress.getHost(), ConnectionUtils.getPortOrDefault(targetAddress));
        }

        return s;
    }

    @Override
    public String[] getSupportedCipherSuites() {
        try (SSLSocket s = (SSLSocket)SSLSocketFactory.getDefault().createSocket()) {
            return s.getSupportedCipherSuites();
        } catch (IOException e) {
            return new String[0];
        }
    }
}
