package de.notepass.htw.crypto.tlsScanner.definition;

import de.notepass.htw.crypto.tlsScanner.reporting.TlsScannerReport;

import javax.net.ssl.SSLSocket;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

/**
 * The implementor of this class implements a OSI level 7 protocol<br>
 * This can be used to check certain functions of the protocol before and after connecting to the target server via
 * a secure connection
 */
public interface IProtocolCommunicator {
    /**
     * Returns whether a protocol can communicate over a non-secured connection.<br>
     * While layer 7 protocols shouldn't care about if a secured connection is created, some applications don't expose
     * unsecured sockets. If this happens this method can return false, so that no unsecured connection attempt is made
     * @return
     */
    public boolean allowPlainCommunication();

    /**
     * This method should return a URL with which an unsecured socket connection can be established
     * Please note: The given string doesn't has to be a valid URL. It is the job of this method to convert it into
     * an usable URL or throw an {@link MalformedURLException} if this isn't possible
     * @param sourceUrl
     * @return
     * @throws MalformedURLException
     */
    public abstract URL getEffectivePlainUrl(String sourceUrl) throws MalformedURLException;


    public void checkOverPlainConnection(Socket plainSocket, URL plainUrl, TlsScannerReport report);

    /**
     * This method should return an SSL-accessible version of the plain URL. If the given URL is already SSL-compatibly
     * it can simply return the given URL<br>
     *
     * @param plainUrl URL returned by the  {@link #getEffectivePlainUrl(String)} method or null if {@link #allowPlainCommunication()} returns false
     * @param sourceUrl string given to the {@link #getEffectivePlainUrl(String)} method
     * @return
     * @throws MalformedURLException If no secured connection URL can be created
     */
    public abstract URL getEffectiveSecuredUrl(String sourceUrl, URL plainUrl) throws MalformedURLException;

    /**
     * This method communicates via its native protocol over the SSL socket after a secured connection was made.<br>
     * This can be used for additional checks which can only be done over a secured connection
     * @param securedSocket
     */
    public void checkOverSecuredConnection(SSLSocket securedSocket, URL securedUrl, TlsScannerReport report);
}
