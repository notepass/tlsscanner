package de.notepass.htw.crypto.tlsScanner.util;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Enum containing all currently known TLS-Versions
 */
public enum TlsVersion {
    TLS10("TLS 1.0", "TLSv1"),
    TLS11("TLS 1.1","TLSv1.1"),
    TLS12("TLS 1.2","TLSv1.2"),
    TLS13("TLS 1.3","TLSv1.3");

    private String readableName;
    private String technicalName;

    TlsVersion(String readableName, String technicalName) {
        this.readableName = readableName;
        this.technicalName = technicalName;
    }

    /**
     * Returns a human-readable name of the TLS version
     * @return human-readable name of the TLS version
     */
    public String getReadableName() {
        return readableName;
    }

    /**
     * Returns the IANA name of the TLS version
     * @return IANA name of the TLS version
     */
    public String getTechnicalName() {
        return technicalName;
    }

    /**
     * Maps a given readable name to a TLS-Version-Enum entry, or null if none can be found
     * @param readableName Name of the Version to get the Enum entry of
     * @return Enum-Entry, or null if none matches
     */
    public static TlsVersion fromReadableName(String readableName) {
        return fromFilterFunction(t -> t.getReadableName().equalsIgnoreCase(readableName));
    }

    /**
     * Maps a IABA name to a TLS-Version-Enum entry, or null if none can be found
     * @param technicalName IANA name of the Version to get the Enum entry of
     * @return Enum-Entry, or null if none matches
     */
    public static TlsVersion fromTechnicalName(String technicalName) {
        return fromFilterFunction(t -> t.getTechnicalName().equalsIgnoreCase(technicalName));
    }

    /**
     * utility-method: Returns the enum entry from a filter function if <b>exactly one</b> was found, null otherwise
     * @param predicate Filter function
     * @return Matched enum entry (if only one), else null
     */
    private static TlsVersion fromFilterFunction(Predicate<? super TlsVersion> predicate) {
        List<TlsVersion> matches = Arrays.stream(TlsVersion.values()).filter(predicate).collect(Collectors.toList());
        if (matches.size() == 1) {
            return matches.get(0);
        }

        return null;
    }
}
