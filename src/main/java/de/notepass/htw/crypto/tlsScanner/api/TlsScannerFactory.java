package de.notepass.htw.crypto.tlsScanner.api;

import de.notepass.htw.crypto.tlsScanner.autogen.schema.cipherCatalog.CipherCatalog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;

public class TlsScannerFactory {
    private static final Logger LOGGER = LogManager.getLogger(TlsScannerFactory.class);

    /**
     * Returns a list of existing internal config files which could be found
     * @return
     */
    public static InternalConfigFile[] getInternalConfigFiles() {
        //TODO: Automate
        return new InternalConfigFile[]{
                new InternalConfigFile(
                        "/cipher_catalogs/BSI_cipher_catalog.xml",
                        "Technische Richtlinie TR-02102-2 Kryptographische Verfahren: Empfehlungen und Schlüssellängen"
                )
        };
    }

    /**
     * Creates a instance of the TlsScanner using the provided configuration
     * @param config
     * @param allowCustomTlsImplementationHelloMessageForCipherTesting
     * @return
     */
    public static TlsScanner createInstance(CipherCatalog config, boolean allowCustomTlsImplementationHelloMessageForCipherTesting) {
        TlsScanner scanner = new TlsScanner();
        scanner.allowCustomTlsImplementationHelloMessageForCipherTesting = allowCustomTlsImplementationHelloMessageForCipherTesting;
        scanner.config = config;

        return scanner;
    }

    /**
     * Creates an instance of the TlsScanner by serializing the given configuration XML-File and configuring the TlsScanner with it
     * @param config
     * @param allowCustomTlsImplementationHelloMessageForCipherTesting
     * @return
     * @throws JAXBException If the given InputStream contains an invalid configuration file
     */
    public static TlsScanner createInstance(InputStream config, boolean allowCustomTlsImplementationHelloMessageForCipherTesting) throws JAXBException {
        return createInstance(parseConfig(config), allowCustomTlsImplementationHelloMessageForCipherTesting);
    }

    /**
     * Creates an instance of the TlsScanner by serializing the given configuration XML-File and configuring the TlsScanner with it
     * @param config
     * @param allowCustomTlsImplementationHelloMessageForCipherTesting
     * @return
     * @throws JAXBException If the given InputStream contains an invalid configuration file
     */
    public static TlsScanner createInstance(Path config, boolean allowCustomTlsImplementationHelloMessageForCipherTesting) throws IOException, JAXBException {
        return createInstance(Files.newInputStream(config), allowCustomTlsImplementationHelloMessageForCipherTesting);
    }

    /**
     * Utility-method: Serializes the configuration given by the input stream
     * @param config
     * @return
     * @throws JAXBException If the input stream contains an invalid configuration file
     */
    public static CipherCatalog parseConfig(InputStream config) throws JAXBException {
        LOGGER.info("Parsing cipher catalog");
        JAXBContext jaxbContext = JAXBContext.newInstance(CipherCatalog.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        CipherCatalog cipherCatalog = (CipherCatalog) jaxbUnmarshaller.unmarshal(config);
        LOGGER.info("Catalog parsed. Contains {} cipher sets", cipherCatalog.getCipherSets().getCipherSet().size());
        return cipherCatalog;
    }
}
