package de.notepass.htw.crypto.tlsScanner.definition;

import de.notepass.htw.crypto.tlsScanner.util.SSLCertificateInfo;

import javax.net.ssl.SSLSocket;

public abstract class SecureConnection implements  AutoCloseable {

    public abstract SSLSocket getSecureSocket();

    public abstract void setSecureSocket(SSLSocket secureSocket);

}
