package de.notepass.htw.crypto.tlsScanner.api;

import de.notepass.htw.crypto.tlsScanner.reporting.TlsScannerReport;

public interface TlsScannerFinishedCallback {
    /**
     * Called when the TlsScanner finished execution. Used for accessing the TlsScannerReport when using an asynchronous
     * implementation
     * @param report
     */
    public void onFinish(TlsScannerReport report);
}
