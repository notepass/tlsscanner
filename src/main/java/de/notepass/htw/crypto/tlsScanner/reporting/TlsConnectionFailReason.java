package de.notepass.htw.crypto.tlsScanner.reporting;

public enum TlsConnectionFailReason {
    NO_SERVER_SUPPORT, NO_CLIENT_SUPPORT, NONE, UNKNOWN
}
