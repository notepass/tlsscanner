package de.notepass.htw.crypto.tlsScanner.api;

import java.io.InputStream;

/**
 * Object to represent an embedded configuration file - Used for listing an easier access for interfaces
 */
public class InternalConfigFile {
    private String location;
    private String name;

    public InternalConfigFile(String location, String name) {
        this.location = location;
        this.name = name;
    }

    /**
     * Returns the absolute path to the file in the jar. Please still load this from a de.notepass.htw.crypto.tlsScanner.*
     * class context as it could elsewhise fail if the library is used as an external library jar-file (separate jar file on disk
     * instead of fat jar), or better yet use {@link #getInputStream()} to acquire an input stream for the file
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     * Returns the readable name of the configuration as presented in the Name-tag of the CipherCatalog
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns an input stream for the given file, or null if the configured file doesn't exists
     * @return
     */
    public InputStream getInputStream() {
        return this.getClass().getResourceAsStream(location);
    }
}
