package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

//See: https://tools.ietf.org/html/rfc7366#section-2

/**
 * See: <a href="https://tools.ietf.org/html/rfc7366#section-2">RFC-7366</a>
 */
public class TlsEncryptThenMacExtension extends TlsExtension {
    public TlsEncryptThenMacExtension() {
        type = 22;
    }

    @Override
    public byte[] serialize() {
        byte[] type = convertToByteArray(this.type);
        return new byte[]{type[0], type[1], 0, 0};
    }

    @Override
    public void deserialize(byte[] data) {
        type = deserializeShort(data[0], data[1]);
    }
}
