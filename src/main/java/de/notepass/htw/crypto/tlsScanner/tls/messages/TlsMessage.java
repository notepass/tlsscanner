package de.notepass.htw.crypto.tlsScanner.tls.messages;

/**
 * Base interface for all kinds of TLS messages
 */
public interface TlsMessage {
    /**
     * Returns the message in a manner in which it can be send over the network to the target server
     * @return
     */
    public byte[] serialize();
}
