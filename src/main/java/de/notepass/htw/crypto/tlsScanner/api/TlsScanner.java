package de.notepass.htw.crypto.tlsScanner.api;

import de.notepass.htw.crypto.tlsScanner.autogen.schema.cipherCatalog.Cipher;
import de.notepass.htw.crypto.tlsScanner.autogen.schema.cipherCatalog.CipherCatalog;
import de.notepass.htw.crypto.tlsScanner.autogen.schema.cipherCatalog.CipherSet;
import de.notepass.htw.crypto.tlsScanner.autogen.schema.cipherCatalog.Property;
import de.notepass.htw.crypto.tlsScanner.definition.IProtocolCommunicator;
import de.notepass.htw.crypto.tlsScanner.definition.ISocketFactory;
import de.notepass.htw.crypto.tlsScanner.reporting.*;
import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsAlertException;
import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsClientHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsServerHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.UnknownContentTypeException;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.*;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls2.Tls12ClientHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls3.Tls13ClientHelloMessage;
import de.notepass.htw.crypto.tlsScanner.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TlsScanner {
    protected CipherCatalog config;
    protected TlsScannerEventCallback callback;
    protected boolean allowCustomTlsImplementationHelloMessageForCipherTesting = false;
    protected static final AtomicInteger threadCounter = new AtomicInteger(0);
    private static final Logger LOGGER = LogManager.getLogger(TlsScanner.class);
    private TlsScannerReport report;
    private URL effectivePlainUrl;
    private URL effectiveSecuredUrl;
    private IProtocolCommunicator protocolCommunicator;
    private ISocketFactory socketFactory;
    private String targetHost;
    private Map<TlsVersion, List<String>> serverCompatibleCiphersByProtocol;
    private boolean abortExecution = false;

    protected TlsScanner() {
        //No-op as the factory methods create this object
    }

    /**
     * Returns the parsed CipherCatalog-XML file. Please node that this structure is automatically generated from a
     * XSD file and will change if the structure of the XSD file changes. So it is not particularry version-safe to access this
     *
     * @return
     */
    public CipherCatalog getConfig() {
        return config;
    }

    /**
     * Returns if the usage of the custom TLS-Handshake-Implementation is allowed if the client doesn't supports a configured cipher
     *
     * @return
     */
    public boolean isAllowCustomTlsImplementationHelloMessageForCipherTesting() {
        return allowCustomTlsImplementationHelloMessageForCipherTesting;
    }

    /**
     * Runs the TlsScanner run asynchronously and uses the given callback to transport information in real-time<br>
     * Deprecated, as multithreaded support is WIP
     *
     * @param callback   Callback to send progress information to
     * @param onFinish   Used to access the report object after an asynchronous run
     * @param targetHost The host to scan
     * @return The thread the TlsScanner runs on
     */
    @Deprecated
    public Thread executeAsync(TlsScannerEventCallback callback, TlsScannerFinishedCallback onFinish, String targetHost) {
        Thread executorThread = new Thread(() -> {
            onFinish.onFinish(execute(callback, targetHost));
        }, "tls-scanner-executor-" + threadCounter.incrementAndGet());
        executorThread.start();
        return executorThread;
    }

    /**
     * Runs the TlsScanner run asynchronously and configures it to use the default callback-implementation (Log4J logging)<br>
     * Deprecated, as multithreaded support is WIP
     *
     * @param onFinish   Used to access the report object after an asynchronous run
     * @param targetHost The host to scan
     * @return The thread the TlsScanner runs on
     */
    @Deprecated
    public Thread executeAsync(TlsScannerFinishedCallback onFinish, String targetHost) {
        return executeAsync(new LoggingTlsScannerEventCallback(), onFinish, targetHost);
    }

    /**
     * Runs the TlsScanner blocking on the current thread. Returns the TlsScannerReport-object to analyze the result of
     * the scan. Uses the given callback to transport information in real-time.
     *
     * @param callback   To access scan information in real-time
     * @param targetHost The host to scan
     * @return Report of the scan for further analyses
     */
    public TlsScannerReport execute(TlsScannerEventCallback callback, String targetHost) {
        this.callback = callback;
        report = new TlsScannerReport();
        this.targetHost = targetHost;
        serverCompatibleCiphersByProtocol = new HashMap<>();

        if (allowCustomTlsImplementationHelloMessageForCipherTesting) {
            LOGGER.warn("Testing client unsupported ciphers via mocked client hello is enabled. Client unsupported ciphers will only be tested by hello messages and not a full handshake!");
        }

        prepareJvm();
        initializeSocketFactory();
        if (abortExecution) {
            return report;
        }

        initializeProtocolCommunicator();
        if (abortExecution) {
            return report;
        }

        calculateEffectiveUrls();
        if (abortExecution) {
            return report;
        }

        doGenericProtocolChecks();
        if (abortExecution) {
            return report;
        }

        doGenericTlsSupportChecks();
        if (abortExecution) {
            return report;
        }

        doSpecificProtocolChecks();
        if (abortExecution) {
            return report;
        }

        doAttackVectorChecks();

        return report;
    }

    /**
     * Runs the TlsScanner blocking on the current thread. Returns the TlsScannerReport-object to analyze the result of
     * the scan. Uses the default callback-implementation (Log4J-Logging).
     *
     * @param targetHost The host to scan
     * @return Report of the scan for further analyses
     */
    public TlsScannerReport execute(String targetHost) {
        return execute(new LoggingTlsScannerEventCallback(), targetHost);
    }

    /**
     * Pre-run step to configure the JVM as defined in the XML-configuration-file (currently only setting system properties)
     */
    private void prepareJvm() {
        if (config.getJvmConfiguration() != null) {
            if (config.getJvmConfiguration().getSystemProperties() != null && config.getJvmConfiguration().getSystemProperties().getProperty() != null) {
                for (Property property : config.getJvmConfiguration().getSystemProperties().getProperty()) {
                    String oldValue = System.getProperty(property.getName());
                    callback.beforeSetJvmProperty(property.getName(), oldValue, property.getValue(), report);
                    System.setProperty(property.getName(), property.getValue());
                    callback.afterSetJvmProperty(property.getName(), oldValue, System.getProperty(property.getName()), report);
                }
            }
        }
    }

    /**
     * pre-run step to initialize an object of the SocketFactory-Implementation defined in the config XML
     */
    private void initializeSocketFactory() {
        String socketFactoryClassName = null;

        if (config.getRuntimeConfiguration() != null) {
            socketFactoryClassName = config.getRuntimeConfiguration().getSocketProducer();
        }

        if (socketFactoryClassName == null) {
            socketFactoryClassName = "de.notepass.htw.crypto.tlsScanner.impl.ConfigurableSocketFactory";
        }

        callback.beforeCreateSocketFactory(socketFactoryClassName, report);
        try {
            socketFactory = (ISocketFactory) Class.forName(socketFactoryClassName).getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            abortExecution(new Exception("Could not instantiate configured socketFactory (\"" + socketFactoryClassName + "\"): " + e.getMessage(), e));
        }
        callback.afterCreateSocketFactory(socketFactory, report);
    }

    /**
     * pre-run step to initialize an object of the ProtocolCommunicator-Implementation defined in the config XML
     */
    private void initializeProtocolCommunicator() {
        String protocolCommunicatorClassName = null;

        if (config.getRuntimeConfiguration() != null) {
            protocolCommunicatorClassName = config.getRuntimeConfiguration().getProtocolCommunicator();
        }

        if (protocolCommunicatorClassName == null) {
            protocolCommunicatorClassName = "de.notepass.htw.crypto.tlsScanner.impl.HttpProtocolCommunicator";
        }

        callback.beforeCreateProtocolCommunicator(protocolCommunicatorClassName, report);
        try {
            protocolCommunicator = (IProtocolCommunicator) Class.forName(protocolCommunicatorClassName).getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            abortExecution(new Exception("Could not instantiate configured protocolCommunicator (\"" + protocolCommunicatorClassName + "\"): " + e.getMessage(), e));
        }
        callback.afterCreateProtocolCommunicator(protocolCommunicator, report);
    }

    /**
     * If called, this method will log an fatal error and set the "abortExecution" flag to true. This doesn't mean that
     * the execution will be stopped instantaniously, but at the next "clean exit" possibility
     *
     * @param cause Cause of the fatal error
     */
    private void abortExecution(Exception cause) {
        callback.onFatalError(cause, report);
        abortExecution = true;
    }

    /**
     * Runs before step 1 execution - Resolves the protocol redirect chain (e.g. HTTP 3xx codes) until the host would switch
     * to a secured protocol or not redirect anymore. Same for the secured redirection chain.
     */
    private void calculateEffectiveUrls() {
        if (protocolCommunicator.allowPlainCommunication()) {
            try {
                callback.beforeCalculateEffectivePlainUrl(targetHost, report);
                effectivePlainUrl = protocolCommunicator.getEffectivePlainUrl(targetHost);
                callback.afterCalculateEffectivePlainUrl(effectivePlainUrl, report);
            } catch (Exception e) {
                abortExecution(new Exception("Could not calculate effective plain URL: " + e.getMessage(), e));
            }
        }

        try {
            callback.beforeCalculateEffectiveSecuredUrl(targetHost, effectivePlainUrl, report);
            effectiveSecuredUrl = protocolCommunicator.getEffectiveSecuredUrl(targetHost, effectivePlainUrl);
            callback.afterCalculateEffectiveSecuredUrl(effectiveSecuredUrl, report);
        } catch (Exception e) {
            abortExecution(new Exception("Could not calculate effective secured URL: " + e.getMessage(), e));
        }
    }

    /**
     * Step 1 - let the protocol communicator do its protocol specific checks
     */
    private void doGenericProtocolChecks() {
        callback.beforeGenericProtocolCheckStepExecution(report);

        if (protocolCommunicator.allowPlainCommunication() && effectivePlainUrl != null) {
            callback.beforeGenericUnsecuredProtocolChecks(report);
            try (Socket unsecuredSocket = socketFactory.createUnsecuredSocket(effectivePlainUrl)) {
                protocolCommunicator.checkOverPlainConnection(unsecuredSocket, effectivePlainUrl, report);
            } catch (Exception e) {
                callback.onGenericUnsecuredProtocolChecksError(e, report);
            }
            callback.afterGenericUnsecuredProtocolChecks(report);
        }

        callback.beforeGenericSecuredProtocolChecks(report);
        try (SSLSocket securedSocket = socketFactory.createSecuredSocket(effectiveSecuredUrl)) {
            protocolCommunicator.checkOverSecuredConnection(securedSocket, effectiveSecuredUrl, report);
        } catch (Exception e) {
            callback.onGenericSecuredProtocolChecksError(e, report);
        }
        callback.afterGenericSecuredProtocolChecks(report);

        callback.beforeGenericCertificateChecks(report);
        try (SSLConnection connection = (SSLConnection) socketFactory.createSecuredConnection(effectiveSecuredUrl)) {
            report.setCertificateInfo(connection.getCertificateInfo());
        } catch (Exception e) {
            callback.onGenericCertificateChecksError(e, report);
        }
        callback.afterGenericCertificateChecks(report);

        callback.afterGenericProtocolCheckStepExecution(report);
    }

    /**
     * Step 2 - Check for TLS 1.0, 1.1, 1.2 and 1.3 support with all client-supported ciphers
     */
    private void doGenericTlsSupportChecks() {
        callback.beforeGenericTlsSupportCheckStepExecution(report);

        TlsVersion[] tlsProtocols = TlsVersion.values();

        for (TlsVersion protocol : tlsProtocols) {
            TlsConnectionFailReason failReason = TlsConnectionFailReason.UNKNOWN;
            Exception ex = null;
            callback.beforeGenericTlsCheck(protocol, effectiveSecuredUrl, report);

            socketFactory.setAllowedProtocols(new TlsVersion[]{protocol});
            try (SSLSocket securedSocket = socketFactory.createSecuredSocket(effectiveSecuredUrl)) {
                failReason = TlsConnectionFailReason.NONE;
            } catch (Exception e) {
                if (e instanceof IllegalArgumentException) {
                    failReason = TlsConnectionFailReason.NO_CLIENT_SUPPORT;
                    ex = e;
                } else {
                    failReason = TlsConnectionFailReason.NO_SERVER_SUPPORT;
                    ex = e;
                }
            }
            TlsRequestSupportDataContainer dataContainer = new TlsRequestSupportDataContainer(failReason == TlsConnectionFailReason.NONE, failReason, ex);
            report.addGenericSupportEntry(protocol, dataContainer);
            callback.afterGenericTlsCheck(protocol, dataContainer, effectiveSecuredUrl, report);
        }

        callback.afterGenericTlsSupportCheckStepExecution(report);
    }

    /**
     * Utility-method: A list of server and client supported ciphers grouped by TLS-Version. This list only contains
     * ciphers which are supported by the native implementation of the client for compatibility reasons.<br>
     * The cipher list is used to fill in the "randomSupported" variable supported by some support nodes for attack vector
     * and extension checks
     *
     * @param tlsVersion
     * @param ianaCipherName
     */
    private void addServerSupportedCipher(TlsVersion tlsVersion, String ianaCipherName) {
        List<String> entry = serverCompatibleCiphersByProtocol.getOrDefault(tlsVersion, new LinkedList<>());
        entry.add(ianaCipherName);
        serverCompatibleCiphersByProtocol.put(tlsVersion, entry);
    }

    /**
     * Step 3 - executes configured checks with specific TLS-Versions and cipher combinations
     */
    private void doSpecificProtocolChecks() {
        callback.beforeSpecificTlsSupportCheckStepExecution(report);

        for (CipherSet cipherSet : config.getCipherSets().getCipherSet()) {
            String protocolName = "";

            // Read TLS protocol name. For some reason the serialisation doesn't work correctly in newer java versions
            // So a check is needed for the content type of the field
            if (cipherSet.getProtocol().getName() instanceof org.w3c.dom.Element) {
                protocolName = ((org.w3c.dom.Element) cipherSet.getProtocol().getName()).getTextContent();
            } else {
                protocolName = cipherSet.getProtocol().getName().toString();
            }
            TlsVersion tlsVersion = TlsVersion.fromTechnicalName(protocolName);

            callback.beforeSpecificTlsSupportGroupCheckStepExecution(tlsVersion, report);
            socketFactory.setAllowedProtocols(new TlsVersion[]{TlsVersion.fromTechnicalName(protocolName)});

            List<String> supportedCiphers = Arrays.asList(socketFactory.getSupportedCipherSuites());

            for (Cipher cipher : cipherSet.getCipher()) {
                //Reset variables which need to be reset for each check
                TlsVersionAndCipherContainer versionAndCipherContainer = new TlsVersionAndCipherContainer(cipher.getName(), tlsVersion);
                boolean testedUsingMockImplementation = false;
                TlsRequestSupportDataContainer supportDataContainer;

                callback.beforeSpecificTlsCheck(versionAndCipherContainer, effectiveSecuredUrl, report);

                if (supportedCiphers.contains(cipher.getName())) {
                    supportDataContainer = doSpecificProtocolCheck(tlsVersion, cipher.getName());
                } else {
                    if (allowCustomTlsImplementationHelloMessageForCipherTesting) {
                        testedUsingMockImplementation = true;
                        supportDataContainer = doSpecificProtocolCheckViaMock(tlsVersion, cipher.getName());
                    } else {
                        supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.NO_CLIENT_SUPPORT);
                    }
                }

                report.addSpecificSupportEntry(versionAndCipherContainer, supportDataContainer);
                callback.afterSpecificTlsCheck(versionAndCipherContainer, supportDataContainer, testedUsingMockImplementation, effectiveSecuredUrl, report);
            }
            callback.afterSpecificTlsSupportGroupCheckStepExecution(tlsVersion, report);
        }
        callback.afterSpecificTlsSupportCheckStepExecution(report);
    }

    /**
     * utility-method for step 3: Does the specific protocol check for one cipher and one TLS-Version with the
     * official TLS java-implementation
     *
     * @param tlsVersion
     * @param ianaCipherName
     * @return
     */
    private TlsRequestSupportDataContainer doSpecificProtocolCheck(TlsVersion tlsVersion, String ianaCipherName) {
        TlsRequestSupportDataContainer supportDataContainer;

        socketFactory.setAllowedCiphers(new String[]{ianaCipherName});
        try (SSLSocket securedSocket = socketFactory.createSecuredSocket(effectiveSecuredUrl)) {
            addServerSupportedCipher(tlsVersion, ianaCipherName);
            supportDataContainer = new TlsRequestSupportDataContainer(true);
        } catch (Exception e) {
            supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.NO_SERVER_SUPPORT);
        }

        return supportDataContainer;
    }

    /**
     * utility-method for step 3: Does the specific protocol check for one cipher and one TLS-Version with the
     * "mocking" custom handshake client
     *
     * @param tlsVersion
     * @param ianaCipherName
     * @return
     */
    private TlsRequestSupportDataContainer doSpecificProtocolCheckViaMock(TlsVersion tlsVersion, String ianaCipherName) {
        TlsRequestSupportDataContainer supportDataContainer = null;

        LOGGER.debug("Cipher {} is not supported by native implementation. Testing via custom Client Hello message", ianaCipherName);
        short cipherCode = TlsUtils.getCipherCode(ianaCipherName);
        if (cipherCode < 0) {
            // Could not find short-code for cipher, cannot test compatibility, return error
            supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.NO_CLIENT_SUPPORT);
            return supportDataContainer;
        }

        TlsClientHelloMessage clientHello = null;

        clientHello = generateClientHelloMessageFor(tlsVersion);

        if (clientHello == null) {
            //Unsupported protocol, return error data
            supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.NO_CLIENT_SUPPORT);
            return supportDataContainer;
        }

        clientHello.setCipherSuites(new short[]{(cipherCode)});
        TlsServerHelloMessage serverHello = null;

        //Send TLS handshake to server
        try (Socket s = new Socket(effectiveSecuredUrl.getHost(), ConnectionUtils.getPortOrDefault(effectiveSecuredUrl))) {
            serverHello = TlsServerHelloMessage.parse(s.getInputStream());
            s.getOutputStream().write(clientHello.serialize());
        } catch (TlsAlertException e) {
            //Server response was a TLS Alert message, so the handshake failed
            supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.NO_SERVER_SUPPORT, e);
        } catch (Exception ex) {
            //Some error occurred while communicating with the server. Fail test
            supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.UNKNOWN, ex);
        }

        if (serverHello == null) {
            // Error or invalid server response, return error info
            return supportDataContainer;
        }

        // Check if the server actually uses the cipher requested by the client
        // According to the RFC this should always be the case, but who knows what the internet can deliver
        if (serverHello.getCipherSuite() == cipherCode) {
            supportDataContainer = new TlsRequestSupportDataContainer(true);
            // Do not add to server supported cipher list as this should only contain ciphers that can be used by the "real" TLS implementation
            //addServerSupportedCipher(tlsVersion, ianaCipherName);
        } else {
            supportDataContainer = new TlsRequestSupportDataContainer(false, TlsConnectionFailReason.NO_SERVER_SUPPORT);
        }

        return supportDataContainer;
    }

    /**
     * Utility-method for step 4: Runs all known attack vector / extension support checks
     */
    private void doAttackVectorChecks() {
        callback.beforeAttackVectorCheckStepExecution(report);
        Arrays.stream(AttackVector.values()).forEach(this::doAttackVectorCheck);
        callback.afterAttackVectorCheckStepExecution(report);
    }

    /**
     * Runs a singe attack vector / extension support check by deligating the call to the correct check function
     *
     * @param attackVector
     */
    private void doAttackVectorCheck(AttackVector attackVector) {
        callback.beforeAttackVectorCheck(attackVector, report);
        Exception errorResult = null;

        try {
            switch (attackVector) {
                case HEARTBLEED:
                    doHearthbleedCheck();
                    break;
                case TRUNCATED_HMAC:
                    doTruncatedHmacCheck();
                    break;
                case ENCRYPT_THAN_HMAC:
                    doEncryptThanMacCheck();
                    break;
                case TLS_COMPRESSION:
                    doTlsCompressionCheck();
                    break;
                case SESSION_RENEGOTIATION:
                    doSessionRenegotiationCheck();
                    break;
                case EXTENDED_MASTER_SECRET:
                    doExtendedMasterSecretCheck();
                    break;
                //case PROTOCOL_COMPRESSION:
                //    doProtocolCompressionCheck();
                //    break;
            }
        } catch (Exception e) {
            report.addAttackVectorCheckError(attackVector, e);
            errorResult = e;
        }

        Boolean result = report.getIsSecureAgainst().get(attackVector);

        if (errorResult != null) {
            callback.onAttackVectorCheckError(attackVector, errorResult, report);
        } else if (result != null) {
            callback.afterAttackVectorCheck(attackVector, result, report);
        }

    }

    /**
     * Runs the hearthbleed check
     */
    private void doHearthbleedCheck() throws TlsAlertException, UnknownContentTypeException, IOException {
        if (checkIfServerSupportsExtension(new TlsHeartbeatExtension(TlsHeartbeatExtension.HEARTBEAT_MODE_ONLY_CLIENT_REQUEST), TlsVersion.TLS12)) {
            report.addAttackVectorCheckResult(AttackVector.HEARTBLEED, false);
        } else {
            report.addAttackVectorCheckResult(AttackVector.HEARTBLEED, true);
        }
    }

    /**
     * Checks if the server supports the truncated_hmac extension
     */
    private void doTruncatedHmacCheck() throws TlsAlertException, UnknownContentTypeException, IOException {
        if (checkIfServerSupportsExtension(new TlsTruncatedHmacExtension(), TlsVersion.TLS12)) {
            report.addAttackVectorCheckResult(AttackVector.TRUNCATED_HMAC, false);
        } else {
            report.addAttackVectorCheckResult(AttackVector.TRUNCATED_HMAC, true);
        }
    }

    /**
     * Checks if the server supports the encrypt_than_mac extension
     */
    private void doEncryptThanMacCheck() throws TlsAlertException, UnknownContentTypeException, IOException {
        if (checkIfServerSupportsExtension(new TlsEncryptThenMacExtension(), TlsVersion.TLS12)) {
            report.addAttackVectorCheckResult(AttackVector.ENCRYPT_THAN_HMAC, true);
        } else {
            report.addAttackVectorCheckResult(AttackVector.ENCRYPT_THAN_HMAC, false);
        }
    }

    /**
     * Checks if the server allows for the use of TLS compression
     */
    private void doTlsCompressionCheck() throws TlsAlertException, UnknownContentTypeException, IOException {
        TlsClientHelloMessage clientHelloMessage = generateClientHelloMessageFor(TlsVersion.TLS12);

        // Tell server that client wants to use compression more than no compression
        clientHelloMessage.setCompressionMethods(new byte[]{1, 0});

        TlsServerHelloMessage serverHelloMessage = sendClientHello(clientHelloMessage);

        report.addAttackVectorCheckResult(AttackVector.TLS_COMPRESSION, serverHelloMessage.getCompressionMethod() == 0);
    }

    /**
     * Checks if the server supports secure renegotiation
     */
    private void doSessionRenegotiationCheck() {
        TlsVersion oldProto = TlsVersion.fromTechnicalName(config.getAttackVectors().getSessionRenegotiation().getOldProtocol());
        TlsVersion newProto = TlsVersion.fromTechnicalName(config.getAttackVectors().getSessionRenegotiation().getNewProtocol());
        String oldCipher = config.getAttackVectors().getSessionRenegotiation().getOldCipher();
        String newCipher = config.getAttackVectors().getSessionRenegotiation().getNewCipher();

        if (oldCipher.equalsIgnoreCase("randomsupported")) {
            oldCipher = getRandomEntry(serverCompatibleCiphersByProtocol.get(oldProto));
        }

        if (newCipher.equalsIgnoreCase("randomsupported")) {
            do {
                newCipher = getRandomEntry(serverCompatibleCiphersByProtocol.get(newProto));
            } while (oldCipher.equals(newCipher));
        }

        try (SSLSocket socket = socketFactory.createSecuredSocket(effectiveSecuredUrl)) {
            socket.setEnabledProtocols(new String[]{oldProto.getTechnicalName()});
            socket.setEnabledCipherSuites(new String[]{oldCipher});
            socket.startHandshake();
            byte[] oldId = new byte[socket.getSession().getId().length];
            System.arraycopy(socket.getSession().getId(), 0, oldId, 0, oldId.length);
            long oldCreationTime = socket.getSession().getCreationTime();
            String oldSessionCipher = socket.getSession().getCipherSuite();
            socket.getOutputStream().write(new byte[]{25, 25, 25});
            socket.getSession().invalidate();
            //socket.getOutputStream().write("HTTP/1.1"+System.lineSeparator()+"GET /");
            //LOGGER.info("----");
            socket.setEnabledProtocols(new String[]{newProto.getTechnicalName()});
            socket.setEnabledCipherSuites(new String[]{newCipher});
            socket.startHandshake();
            socket.getOutputStream().write(new byte[]{25, 25, 25});
            //TODO: Is this type of session renegotiation really meant with 3.3.4.1? // See https://stackoverflow.com/questions/27832559/what-is-the-purpose-of-ssl-tls-renegotiation
            if (!oldSessionCipher.equalsIgnoreCase(socket.getSession().getCipherSuite()) && Arrays.equals(oldId, socket.getSession().getId()) /*&& oldCreationTime == socket.getSession().getCreationTime()*/) {
                report.addAttackVectorCheckResult(AttackVector.SESSION_RENEGOTIATION, true);
            } else {
                report.addAttackVectorCheckResult(AttackVector.SESSION_RENEGOTIATION, false);
            }
        } catch (Exception e) {
            // Error while checking - fail test
            if (e instanceof SSLHandshakeException && e.getMessage() != null && e.getMessage().contains("handshake_failure")) {
                //Handshake failure occurred. Server doesn't allow session renegotiation
                report.addAttackVectorCheckResult(AttackVector.SESSION_RENEGOTIATION, true);
            }
            report.addAttackVectorCheckError(AttackVector.SESSION_RENEGOTIATION, e);
        }
    }

    /**
     * Checks if the server supports the extended_master_secret extension
     */
    private void doExtendedMasterSecretCheck() throws TlsAlertException, UnknownContentTypeException, IOException {
        if (checkIfServerSupportsExtension(new TlsExtendedMasterSecretExtension(), TlsVersion.TLS12)) {
            report.addAttackVectorCheckResult(AttackVector.EXTENDED_MASTER_SECRET, true);
        } else {
            report.addAttackVectorCheckResult(AttackVector.EXTENDED_MASTER_SECRET, false);
        }
    }

    /**
     * Checks if the underlying protocol allows compression (WIP, no function currently)
     */
    private void doProtocolCompressionCheck() {
        //TODO
    }

    /**
     * Utility-method to send a TLS handshake with a specified extension. The server-response will be checked for support of
     * the extension
     *
     * @param tlsExtension The extension tpo check support for
     * @param tlsVersion   The Version of the TLS protocol to use
     * @return If the extension is supported by the server
     */
    private boolean checkIfServerSupportsExtension(final TlsExtension tlsExtension, TlsVersion tlsVersion) throws TlsAlertException, UnknownContentTypeException, IOException {
        return checkIfServerSupportsExtension(tlsExtension, generateClientHelloMessageFor(tlsVersion));
    }

    /**
     * Utility-method to send a TLS handshake with a specified extension. The extension will be added to the client hello
     * given to this method and removed afterwards - So if the client hello object is accessed asynchronously there might be
     * problematic behaviour
     *
     * @param tlsExtension         The extension tpo check support for
     * @param helloMessageTemplate Hello message to use for testing, will be temporary modified!
     * @return If the extension is supported by the server
     */
    private boolean checkIfServerSupportsExtension(final TlsExtension tlsExtension, TlsClientHelloMessage helloMessageTemplate) throws TlsAlertException, UnknownContentTypeException, IOException {
        TlsServerHelloMessage serverHello = null;

        helloMessageTemplate.addExtension(tlsExtension);

        try {
            serverHello = sendClientHello(helloMessageTemplate);
            return serverHello != null
                    && serverHello.getExtensions() != null
                    && Arrays.stream(serverHello.getExtensions()).anyMatch(e -> e.getClass().equals(tlsExtension.getClass()));
        } finally {
            helloMessageTemplate.removeExtension(tlsExtension);
        }
    }

    /**
     * Utility method - Generates a client hello message for a given TLS-Version
     *
     * @param tlsVersion
     * @return
     */
    private TlsClientHelloMessage generateClientHelloMessageFor(TlsVersion tlsVersion) {
        TlsClientHelloMessage clientHello = null;

        switch (tlsVersion) {
            default:
            case TLS10:
            case TLS11:
                //There is no mock implementation for TLS 1.0 and 1.1
                break;
            case TLS12:
                clientHello = new Tls12ClientHelloMessage();
                break;
            case TLS13:
                clientHello = new Tls13ClientHelloMessage();
                break;
        }

        return clientHello;
    }

    /**
     * Sends a given client hello to a server and parses the server response
     *
     * @param clientHello
     * @return
     * @throws TlsAlertException           If the server sends a TLS Alter message instead of a hello message (= handshake failed)
     * @throws IOException                 If transmission errors occour
     * @throws UnknownContentTypeException If the response of the server contains an unknown "content type" field value in the response (= No alert but also no server hello)
     */
    protected TlsServerHelloMessage sendClientHello(TlsClientHelloMessage clientHello) throws TlsAlertException, IOException, UnknownContentTypeException {
        try (Socket s = new Socket(effectiveSecuredUrl.getHost(), ConnectionUtils.getPortOrDefault(effectiveSecuredUrl))) {
            s.getOutputStream().write(clientHello.serialize());
            return TlsServerHelloMessage.parse(s.getInputStream());
        }
    }

    /**
     * Utility method - returns an random entry from a given list - used to resolve the "randomSupported" placeholder
     * supported by the configuration of some attack vector / extension checks
     *
     * @param list
     * @param <T>
     * @return
     */
    private static <T> T getRandomEntry(List<T> list) {
        return list.get(new Random().nextInt(list.size()));
    }
}
