package de.notepass.htw.crypto.tlsScanner.api;

import de.notepass.htw.crypto.tlsScanner.definition.IProtocolCommunicator;
import de.notepass.htw.crypto.tlsScanner.definition.ISocketFactory;
import de.notepass.htw.crypto.tlsScanner.reporting.*;
import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;

import java.net.URL;

public interface TlsScannerEventCallback {
    /**
     * This callback is executed before a JVM property is set to a new value. The properties an values are read from the
     * CipherCatalog/JvmConfiguration/SystemProperties node of the config file
     * @param name Name of the property to set
     * @param currentValue Current value of the property
     * @param newValue Target value as configured in the CipherCatalog
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeSetJvmProperty(String name, String currentValue, String newValue, TlsScannerReport report);

    /**
     *  This callback is executed after a JVM property is set to a new value. The properties an values are read from the
     *  CipherCatalog/JvmConfiguration/SystemProperties node of the config file
     * @param name Name of the property to set
     * @param oldValue Value before the property was set to a new value
     * @param currentValue Current value of the property
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterSetJvmProperty(String name, String oldValue, String currentValue, TlsScannerReport report);


    /**
     * This function is called after gathering information about which ciphers are supported by the native TLS implementation
     * @param supportedCiphers List of client supported ciphers
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void onListSupportedCiphers(String[] supportedCiphers, TlsScannerReport report);


    /**
     * This method is called before the configured SocketFactory-instance is created. The SocketFactory implementation class
     * is configured with the node "/CipherCatalog/RuntimeConfiguration/SocketProducer" inside the configuration-XML
     * @param className FQCN of the configured SocketFactory implementation class
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeCreateSocketFactory(String className, TlsScannerReport report);

    /**
     * This method is called after the configured SocketFactory-instance is created. The SocketFactory implementation class
     * is configured with the node "/CipherCatalog/RuntimeConfiguration/SocketProducer" inside the configuration-XML
     * @param socketFactory Instance of the socket factory created from the configuration file
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterCreateSocketFactory(ISocketFactory socketFactory, TlsScannerReport report);

    /**
     * This method is called before the configured ProtocolCommunicator-instance is created. The ProtocolCommunicator
     * implementation class is configured with the node "/CipherCatalog/RuntimeConfiguration/ProtocolCommunicator" inside
     * the configuration-XML
     * @param className FQCN of the configured ProtocolCommunicator implementation class
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeCreateProtocolCommunicator(String className, TlsScannerReport report);

    /**
     * This method is called after the configured ProtocolCommunicator-instance is created. The ProtocolCommunicator
     * implementation class is configured with the node "/CipherCatalog/RuntimeConfiguration/ProtocolCommunicator"
     * inside the configuration-XML
     * @param protocolCommunicator Instance of the ProtocolCommunicator created from the configuration file
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterCreateProtocolCommunicator(IProtocolCommunicator protocolCommunicator, TlsScannerReport report);

    /**
     * This function is called before the plain-URL redirection chain is resolved or the given host URL is parsed
     * @param targetHost Raw host address, as given by the client
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeCalculateEffectivePlainUrl(String targetHost, TlsScannerReport report);

    /**
     * This function is called after the plain-URL redirection chain is resolved
     * @param plainUrl Final URL-endpoint before the redirection chain stops or the server forces an encrypted connection
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterCalculateEffectivePlainUrl(URL plainUrl, TlsScannerReport report);

    /**
     * This function is called before the plain-URL redirection chain is resolved or the given host URL is parsed
     * @param plainUrl Effective plain URL. The last non-encrypted URL-endpoint the hosts supports. Or null if no plain
     *                 URL redirection chain was resolved (e.g. because the host doesn't support plain protocol connections)
     * @param targetHost Raw host address, as given by the client
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeCalculateEffectiveSecuredUrl(String targetHost, URL plainUrl, TlsScannerReport report);

    /**
     * This function is called after the plain-URL redirection chain is resolved
     * @param securedUrl Final URL-endpoint before the redirection chain stops
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterCalculateEffectiveSecuredUrl(URL securedUrl, TlsScannerReport report);

    //Pre/Post step 1

    /**
     * This function is called before check step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeGenericProtocolCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called after check step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterGenericProtocolCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called before the subtask "do protocol specific checks on an unsecured connection" of check
     * step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeGenericUnsecuredProtocolChecks(TlsScannerReport report);

    /**
     * This function is called after the subtask "do protocol specific checks on an unsecured connection" of check
     * step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterGenericUnsecuredProtocolChecks(TlsScannerReport report);

    /**
     * This function is called if an error occurs while running subtask
     * "do protocol specific checks on an unsecured connection" of check  step 1 (generic protocol checks for the
     * underlying protocol) is executed
     * @param error Exception generated while running checks
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void onGenericUnsecuredProtocolChecksError(Exception error, TlsScannerReport report);

    /**
     * This function is called before the subtask "do protocol specific checks on an secured connection" of check
     * step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeGenericSecuredProtocolChecks(TlsScannerReport report);

    /**
     * This function is called after the subtask "do protocol specific checks on an secured connection" of check
     * step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterGenericSecuredProtocolChecks(TlsScannerReport report);

    /**
     * This function is called if an error occurs while running subtask
     * "do protocol specific checks on an secured connection" of check  step 1 (generic protocol checks for the
     * underlying protocol) is executed
     * @param error Exception generated while running checks
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void onGenericSecuredProtocolChecksError(Exception error, TlsScannerReport report);

    /**
     * This function is called before the subtask "do certificate checks" of check
     * step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeGenericCertificateChecks(TlsScannerReport report);

    /**
     * This function is called after the subtask "do certificate checks" of check
     * step 1 (generic protocol checks for the underlying protocol) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterGenericCertificateChecks(TlsScannerReport report);

    /**
     * This function is called if an error occurs while running subtask
     * "do certificate checks" of check step 1 (generic protocol checks for the
     * underlying protocol) is executed
     * @param error Exception generated while running checks
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void onGenericCertificateChecksError(Exception error, TlsScannerReport report);

    //Pre/Post step 2
    /**
     * This function is called before check step 2 (generic TLS-Version support checks) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeGenericTlsSupportCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called after check step 2 (generic TLS-Version support checks) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterGenericTlsSupportCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called before the subtask "do generic TLS version support check" of check
     * step 2 (generic TLS-Version support checks) is executed
     * @param securedUrl URL used for connecting to the secured endpoint
     * @param tlsVersion TLS-Version for which the support was checked
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeGenericTlsCheck(TlsVersion tlsVersion, URL securedUrl, TlsScannerReport report);

    /**
     * This function is called after the subtask "do generic TLS version support check" of check
     * step 2 (generic TLS-Version support checks) is executed
     * @param securedUrl URL used for connecting to the secured endpoint
     * @param tlsVersion TLS-Version for which the support was checked
     * @param dataContainer Container for the result data of the check (This is also added to the report beforehand)
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterGenericTlsCheck(TlsVersion tlsVersion, TlsRequestSupportDataContainer dataContainer, URL securedUrl, TlsScannerReport report);

    //Pre/Post step 3
    /**
     * This function is called before check step 3 (specific TLS-Version and cipher support checks) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeSpecificTlsSupportCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called after check step 3 (specific TLS-Version and cipher support checks) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterSpecificTlsSupportCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called before the subtask "do specific TLS version with cipher support check" of check step 3
     * (specific TLS-Version and cipher support checks) is executed for a given TLS-Version (each TLS-Version can contain
     * multiple ciphers to check for)
     * @param version TLS-Version of the group which will be checked next
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeSpecificTlsSupportGroupCheckStepExecution(TlsVersion version, TlsScannerReport report);

    /**
     * This function is called after the subtask "do specific TLS version with cipher support check" of check step 3
     * (specific TLS-Version and cipher support checks) is executed for a given TLS-Version (each TLS-Version can contain
     * multiple ciphers to check for)
     * @param version TLS-Version of the group which was checked
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterSpecificTlsSupportGroupCheckStepExecution(TlsVersion version, TlsScannerReport report);

    /**
     * This function is called before the subtask "do specific TLS version with cipher support check" of check
     * step 3 (specific TLS-Version and cipher support checks) is executed
     * @param versionAndCipherContainer Contains information about the TLS version and cipher combination to check
     * @param securedUrl URL used for connecting to the secured endpoint
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeSpecificTlsCheck(TlsVersionAndCipherContainer versionAndCipherContainer, URL securedUrl, TlsScannerReport report);

    /**
     * This function is called after the subtask "do specific TLS version with cipher support check" of check
     * step 3 (specific TLS-Version and cipher support checks) is executed
     * @param versionAndCipherContainer Contains information about the checked TLS version and cipher combination
     * @param securedUrl URL used for connecting to the secured endpoint
     * @param dataContainer Data container for information about the test result
     * @param madeByUsingMockClient Indicates if the check was done via the native TLS implementation or the mock client
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterSpecificTlsCheck(TlsVersionAndCipherContainer versionAndCipherContainer, TlsRequestSupportDataContainer dataContainer, boolean madeByUsingMockClient, URL securedUrl, TlsScannerReport report);

    //Pre/Post step 4
    /**
     * This function is called before check step 4 (possible attack vectors / supported extensions checks) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeAttackVectorCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called after check step 4 (possible attack vectors / supported extensions checks) is executed
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterAttackVectorCheckStepExecution(TlsScannerReport report);

    /**
     * This function is called before the subtask "do attack vector / extension support test" of check
     * step 4 (possible attack vectors / supported extensions checks) is executed
     * @param attackVector The attack vector / extension which is tested
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void beforeAttackVectorCheck(AttackVector attackVector, TlsScannerReport report);

    /**
     * This function is called after the subtask "do attack vector / extension support test" of check
     * step 4 (possible attack vectors / supported extensions checks) is executed
     * @param attackVector The attack vector / extension which was tested
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void afterAttackVectorCheck(AttackVector attackVector, boolean isSecureAgainst, TlsScannerReport report);

    /**
     * This function is called when the subtask "do attack vector / extension support test" of check
     * step 4 (possible attack vectors / supported extensions checks) has encountered an error for a
     * specific attack vecotr / extension support check
     * @param attackVector The attack vector / extension which was tested
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void onAttackVectorCheckError(AttackVector attackVector, Exception error, TlsScannerReport report);

    /**
     * This function is called if an unrecoverable error occours in any of the steps and the execution needs to be aborted.
     * This doesn't mean that there will be no more callback functions will be called - The scanner will shut down on the
     * next "clean exit" possibility which is normally after a "after[...]StepExecution" function call
     * @param e Fatal exception which resulted in the scanner shutdown
     * @param report Current version of the TlsScannerReport for in-depth/context bound-operations
     */
    public void onFatalError(Exception e, TlsScannerReport report);
}
