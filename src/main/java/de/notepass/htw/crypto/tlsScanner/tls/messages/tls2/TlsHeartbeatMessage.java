package de.notepass.htw.crypto.tlsScanner.tls.messages.tls2;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

import java.security.SecureRandom;

//TODO: Untested, Unused
@Deprecated
//See https://tools.ietf.org/html/rfc6520#page-5 / https://blog.trendmicro.com/trendlabs-security-intelligence/files/2014/04/payloadlength.png
public class TlsHeartbeatMessage {
    // ======== START "Record Layer" ========
    /**
     * @see Tls12ClientHelloMessage#contentType
     */
    private byte contentType = 24; //24 = Heartbeat

    /**
     * @see Tls12ClientHelloMessage#tlsVersion
     */
    private short tlsVersion = 0x0303; //0x0303 = TLSv1.2

    /**
     * @see Tls12ClientHelloMessage#totalLength
     */
    private short totalLength = 0; //Size of the package to send
    // ======== END "Record Layer" ========

    // Start content
    private byte type = 1; //1 = Request, 2 = Response
    private short payloadLength;


    private byte[] payload;
    private byte[] padding = new byte[16];

    public TlsHeartbeatMessage(short payloadLength, byte[] payload) {
        this.payloadLength = payloadLength;
        this.payload = payload;
        new SecureRandom().nextBytes(padding);
    }

    public byte[] serialize() {
        totalLength = (short) (sizeof(type) + sizeof(payloadLength) + sizeof(payload) + sizeof(padding));
        if (payloadLength <= 0) { //Allow override for heartbleed
            payloadLength = (short) sizeof(payload);
        }

        byte[] out = new byte[totalLength + sizeof(contentType) + sizeof(tlsVersion) + sizeof(totalLength)];
        int pos = append(out, contentType, 0);
        pos = append(out, tlsVersion, pos);
        pos = append(out, totalLength, pos);
        pos = append(out, type, pos);
        pos = append(out, payloadLength, pos);
        pos = append(out, payload, pos);
        pos = append(out, padding, pos);

        return out;
    }
}
