package de.notepass.htw.crypto.tlsScanner.tls.messages;

/**
 * Exception encapsulating a TLS Alert message
 */
public class TlsAlertException extends Exception {
    public TlsAlertException() {
        super();
    }

    public TlsAlertException(String message) {
        super(message);
    }

    public TlsAlertException(String message, Throwable cause) {
        super(message, cause);
    }
}
