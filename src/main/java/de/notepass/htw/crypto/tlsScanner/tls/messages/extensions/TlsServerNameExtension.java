package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;

import java.nio.charset.StandardCharsets;
import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

//See rfc6066
/**
 * See: <a href="https://tools.ietf.org/html/rfc6066">RFC-6066</a>
 */
public class TlsServerNameExtension extends TlsExtension {
    private String hostname;
    private byte serverNameType;

    public TlsServerNameExtension(String hostname) {
        this.type = 0; // 0 = ServerName
        this.hostname = hostname;
        this.serverNameType = 0; //0 = host name
    }

    @Override
    public byte[] serialize() {
        //TODO: Check if this should be ASCII or UTF-8
        byte[] hostnameBytes = hostname.getBytes(StandardCharsets.UTF_8);
        //byte[] serverNameLengthBytes = convertToByteArray((short) hostnameBytes.length);
        //byte[] typeBytes = convertToByteArray(type);
        short serverNameListLength = (short) (sizeof(serverNameType) + sizeof((short) hostnameBytes.length) + sizeof(hostnameBytes));
        //byte[] serverNameListLengthBytes = convertToByteArray(serverNameListLength);
        length = (short) (serverNameListLength + sizeof(serverNameListLength));
        //byte[] totalLengthBytes = convertToByteArray(totalLength);

        byte[] output = new byte[length + sizeof(type) + sizeof(length)];
        int pos = append(output, type, 0);
        pos = append(output, length, pos);
        pos = append(output, serverNameListLength, pos);
        pos = append(output, serverNameType, pos);
        pos = append(output, (short) hostnameBytes.length, pos);
        pos = append(output, hostnameBytes, pos);

        return output;
    }

    @Override
    public void deserialize(byte[] data) {
        type = deserializeShort(data[0], data[1]);
    }
}
