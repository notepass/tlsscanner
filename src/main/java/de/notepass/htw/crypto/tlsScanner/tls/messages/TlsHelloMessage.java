package de.notepass.htw.crypto.tlsScanner.tls.messages;

/**
 * Base interface for server/client hello messages (handshake)
 */
public interface TlsHelloMessage extends TlsMessage {
}
