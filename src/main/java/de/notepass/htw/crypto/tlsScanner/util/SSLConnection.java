package de.notepass.htw.crypto.tlsScanner.util;

import de.notepass.htw.crypto.tlsScanner.definition.SecureConnection;

import javax.net.ssl.SSLSocket;

public class SSLConnection extends SecureConnection {

    private SSLSocket secureSocket;
    private SSLCertificateInfo certificateInfo;

    public SSLConnection(SSLSocket secureSocket, SSLCertificateInfo certificateInfo) {
        this.secureSocket = secureSocket;
        this.certificateInfo = certificateInfo;
    }

    public SSLSocket getSecureSocket() {
        return secureSocket;
    }

    public void setSecureSocket(SSLSocket secureSocket) {
        this.secureSocket = secureSocket;
    }

    public SSLCertificateInfo getCertificateInfo() {
        return certificateInfo;
    }

    public void setCertificateInfo(SSLCertificateInfo certificateInfo) {
        this.certificateInfo = certificateInfo;
    }

    @Override
    public void close() throws Exception {
        secureSocket.close();
    }
}
