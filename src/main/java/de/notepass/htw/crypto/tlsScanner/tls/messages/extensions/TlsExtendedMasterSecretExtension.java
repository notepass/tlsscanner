package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

//See: https://tools.ietf.org/html/rfc7627

/**
 * See: <a href="https://tools.ietf.org/html/rfc7627">RFC-7627</a>
 */
public class TlsExtendedMasterSecretExtension extends TlsExtension {
    @Override
    public byte[] serialize() {
        type = 0x17;
        return new byte[]{0, 0x17, 0, 0};
    }

    @Override
    public void deserialize(byte[] data) {
        type = deserializeShort(data[0], data[1]);
    }
}
