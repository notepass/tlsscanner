package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

/**
 * See: <a href="https://tools.ietf.org/html/rfc8446">RFC-8446</a>
 */
public class TlsSupportedVersionsExtension extends TlsExtension {
    private short[] supportedVersions = new short[0];

    public TlsSupportedVersionsExtension(short supportedVersion) {
        type = 43;
        this.supportedVersions = new short[]{supportedVersion};
    }

    public TlsSupportedVersionsExtension(short[] supportedVersions) {
        type = 43;
        this.supportedVersions = supportedVersions;
    }

    @Override
    public byte[] serialize() {
        byte supportedVersionsSize = (byte) sizeof(supportedVersions);
        length = (short) (supportedVersionsSize + sizeof(supportedVersionsSize));
        byte[] data = new byte[length + sizeof(length) + sizeof(type)];
        int pos = append(data, type, 0);
        pos = append(data, length, pos);
        pos = append(data, supportedVersionsSize, pos);
        pos = append(data, supportedVersions, pos);

        return data;
    }

    @Override
    public void deserialize(byte[] data) {

    }
}
