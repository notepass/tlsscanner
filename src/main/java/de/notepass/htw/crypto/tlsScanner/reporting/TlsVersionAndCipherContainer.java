package de.notepass.htw.crypto.tlsScanner.reporting;

import de.notepass.htw.crypto.tlsScanner.util.TlsVersion;

public class TlsVersionAndCipherContainer {
    private String ianaCipherName;
    private TlsVersion tlsVersion;

    public TlsVersionAndCipherContainer(String ianaCipherName, TlsVersion tlsVersion) {
        this.ianaCipherName = ianaCipherName;
        this.tlsVersion = tlsVersion;
    }

    public String getIanaCipherName() {
        return ianaCipherName;
    }

    public TlsVersion getTlsVersion() {
        return tlsVersion;
    }
}
