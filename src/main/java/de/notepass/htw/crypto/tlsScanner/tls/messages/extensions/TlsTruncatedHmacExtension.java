package de.notepass.htw.crypto.tlsScanner.tls.messages.extensions;

import static de.notepass.htw.crypto.tlsScanner.util.TlsMessageUtils.*;

//See: https://tools.ietf.org/html/rfc6066
/**
 * See: <a href="https://tools.ietf.org/html/rfc6066">RFC-6066</a>
 */
public class TlsTruncatedHmacExtension extends TlsExtension {
    public TlsTruncatedHmacExtension() {
        type = 4;
    }

    @Override
    public byte[] serialize() {
        byte[] type = convertToByteArray(this.type);
        return new byte[]{type[0], type[1], 0, 0};
    }

    @Override
    public void deserialize(byte[] data) {
        type = deserializeShort(data[0], data[1]);
    }
}
