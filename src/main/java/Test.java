import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsClientHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.TlsServerHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsExtension;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsHeartbeatExtension;
import de.notepass.htw.crypto.tlsScanner.tls.messages.extensions.TlsServerNameExtension;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls2.Tls12ClientHelloMessage;
import de.notepass.htw.crypto.tlsScanner.tls.messages.tls2.TlsHeartbeatMessage;

import java.net.Socket;
import java.net.URI;
import java.security.SecureRandom;

public class Test {
    public static void main(String[] args) throws Exception {
        URI uri = URI.create("https://bachgold.de");
        System.out.println(uri.getPort());

        Socket s = new Socket("bachgold.de", 443);
        TlsClientHelloMessage clientHello = new Tls12ClientHelloMessage();
        clientHello.setCompressionMethods(new byte[]{1, 0});
        clientHello.setExtensions(new TlsExtension[]{new TlsServerNameExtension("bachgold.de"), new TlsHeartbeatExtension((byte) 1)});
        s.getOutputStream().write(clientHello.serialize());

        TlsServerHelloMessage serverHello = TlsServerHelloMessage.parse(s.getInputStream());

        if (serverHello.getCompressionMethod() == 1) {
            System.out.println("Compression supported");
        } else {
            System.out.println("Compression not supported");
        }

        Thread.sleep(2000);
        byte[] payload = new byte[16];
        new SecureRandom().nextBytes(payload);
        s.getOutputStream().write(new TlsHeartbeatMessage((short) 16, payload).serialize());

        s.getInputStream().read(payload);

        Thread.sleep(1000);
        s.close();

        /*
        byte[] recordLayerData = new byte[5];
        s.getInputStream().read(recordLayerData);
        if (recordLayerData[0] == 21) {
            System.out.println("Got ALERT");
        } else if (recordLayerData[0] == 22) {
            System.out.println("Got HANDSHAKE");
        } else {
            System.out.println("Got UNKNOWN");
        }
        */
    }
}
